﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/DistanceAndLOS/AllyInAttackRangeAndVisible")]
    [Help("checks if an ally is within the minions attackRange and LOS")]
    public class AllyInAttackRangeAndVisible : GOCondition
    {
        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                PLY_AllyManager am = body.allyManager;
                GameObject nearestAlly = am.getNearestVisibleAlly(gameObject.transform.position, body.sightRange, true);
                if (nearestAlly == null) return false;
                float dist = Vector3.Distance(nearestAlly.transform.position, gameObject.transform.position);
                if (dist > body.attackRange) return false;
                if (!body.isFacingForAttack(nearestAlly.transform.position)) {
                    body.shouldRotate = true;
                    body.lookTowardsVec = nearestAlly.transform.position;
                    return false;
                }
                //body.shouldRotate = false;
                //if (!UTL_Dungeon.checkLOSWalls(UTL_Math.vec3ToVec2(gameObject.transform.position), UTL_Math.vec3ToVec2(nearestAlly.transform.position))) return false;
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}