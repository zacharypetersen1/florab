﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/DistanceAndLOS/EnemyVisibleInThreatRange")]
    [Help("checks if any enemies are visible and within the minions threatRange")]
    public class EnemyVisibleInThreatRange : GOCondition
    {
        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                PLY_AllyManager am = body.allyManager;
                GameObject nearestEnemy = am.getNearestVisibleEnemy(gameObject.transform.position, body.sightRange);
                if (nearestEnemy == null) return false;
                if (Vector3.Distance(nearestEnemy.transform.position, gameObject.transform.position) > body.threatRange) return false;
                if (!body.isFacing(nearestEnemy.transform.position)) return false;
                //if (!UTL_Dungeon.checkLOSWalls(UTL_Math.vec3ToVec2(gameObject.transform.position), UTL_Math.vec3ToVec2(nearestEnemy.transform.position))) return false;
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}