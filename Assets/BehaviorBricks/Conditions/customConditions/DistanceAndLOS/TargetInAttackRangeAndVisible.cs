﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/DistanceAndLOS/TargetInAttackRange")]
    [Help("checks if an enemy is within the minions attackRange")]
    public class TargetInAttackRangeAndVisible : GOCondition
    {
        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                PLY_AllyManager am = body.allyManager;
                GameObject nearestEnemy = am.getNearestVisibleEnemy(gameObject.transform.position, body.sightRange, false);
                if (nearestEnemy == null)
                {
                    //Debug.Log("Nearest enemy is null in targetInAttackRange");
                    return false;
                }
                if (Vector3.Distance(nearestEnemy.transform.position, gameObject.transform.position) > body.attackRange)
                {
                    //Debug.Log("nearest enemy not in range");
                    return false;
                }
                if (!body.isFacing(nearestEnemy.transform.position))
                {
                    //Debug.Log("isFacing is false");
                    return false;
                }
                //if (!UTL_Dungeon.checkLOSWalls(UTL_Math.vec3ToVec2(gameObject.transform.position), UTL_Math.vec3ToVec2(nearestEnemy.transform.position))) return false;
                //Debug.Log("got past attackRange check");
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}