﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;
using System.Collections.Generic;

namespace BBUnity.Conditions
{
    [Condition("Custom/DistanceAndLOS/ShouldUseSlow")]
    [Help("checks if this confusion enemy's ability is ready and if it is in range of at least 2 enemies")]
    public class ShouldUseSlow : GOCondition
    {
        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_SlowMinion body = gameObject.GetComponent<ENT_SlowMinion>();
                ENT_SlowAbility slowAbi = gameObject.GetComponent<ENT_SlowAbility>();
                if (slowAbi.EnemyCoolDownTimer > 0) return false;
                PLY_AllyManager am = body.allyManager;
                List<GameObject> inRange = am.getAllAlliesWithinRange(gameObject.transform.position, 20, true); //range of confusion ability
                return (inRange.Count >= 1);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}