﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;
using UnityEngine.AI;

namespace BBUnity.Conditions
{
    [Condition("Custom/AllySpecific/NoPathToDestination")]
    [Help("Checks if the minion does not have a path to its destination")]
    public class NoPathToDestination : GOCondition
    {

        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                NavMeshAgent agent = body.Agent;
                float remainingDist;
                float directDist;
                if (agent.pathStatus == NavMeshPathStatus.PathComplete)
                {
                    remainingDist = agent.remainingDistance;
                    directDist = Vector3.Distance(gameObject.transform.position, agent.destination);
                }
                else return true;
                //Debug.Log("remainingDist: " + remainingDist + " directDist: " + directDist);
                return ((remainingDist > directDist * 1.5 && remainingDist > 10) || remainingDist > directDist + 5 || (remainingDist == 0 && directDist > 3));
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}