﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/Distance/NotNearSpawnedBy")]
    [Help("Checks if the monster is within 5 distance units of the structure that spawned it")]
    public class NotNearSpawnedBy : GOCondition
    {

        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                return !body.amINearSpawner();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}