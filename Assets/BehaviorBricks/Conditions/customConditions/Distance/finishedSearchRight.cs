﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Conditions
{

    [Condition("Custom/Distance/finishedSearchRight")]
    [Help("checks if the minion has rotated enough to the right")]
    public class finishedSearchRight : GOCondition
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;

        public override bool Check()
        {
            const float sensitivity = 3;
            ENT_Body body = gameObject.GetComponent<ENT_Body>();
            return Mathf.Abs(Vector3.Distance(body.transform.position, body.searchRight)) < sensitivity;
        }
    }
}