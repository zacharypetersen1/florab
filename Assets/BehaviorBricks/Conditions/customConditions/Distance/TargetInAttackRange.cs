﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/Distance/TargetinAttackRange")]
    [Help("checks if an enemy is within the minions attackRange")]
    public class TargetInAttackRange : GOCondition
    {
        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                PLY_AllyManager am = body.allyManager;
                GameObject nearestEnemy = am.getNearestEnemy(gameObject.transform.position, false);
                ENT_DungeonEntity enemyENT = nearestEnemy.GetComponent<ENT_DungeonEntity>();
                if (enemyENT == null)
                {
                    return false;
                }
                bool result = Vector3.Distance(enemyENT.getNearestAttackablePoint(gameObject.transform.position), gameObject.transform.position) < body.attackRange;
                return result;

            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}