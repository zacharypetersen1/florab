﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/Distance/AllyInHalfAttackRange")]
    [Help("checks if an ally is within the minions attackRange * .5")]
    public class AllyInHalfAttackRange : GOCondition
    {
        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                PLY_AllyManager am = body.allyManager;
                GameObject nearestAlly = am.getNearestVisibleAlly(gameObject.transform.position, body.sightRange, true);
                if (nearestAlly == null) return false;
                float dist = Vector3.Distance(nearestAlly.transform.position, gameObject.transform.position);
                return (dist <= body.attackRange * 0.5F);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}