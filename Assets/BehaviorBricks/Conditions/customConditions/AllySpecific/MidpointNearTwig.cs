﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/AllySpecific/MidPointNearTwig")]
    [Help("Checks if the midpoint of all allies is within the AllyFollowRadius of Twig")]
    public class MidpointNearTwig : GOCondition
    {

        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                return body.isMidpointNearTwig();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}