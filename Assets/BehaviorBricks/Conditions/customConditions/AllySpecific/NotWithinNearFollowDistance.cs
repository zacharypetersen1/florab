﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/AllySpecific/NotWithinNearFollowDistance")]
    [Help("Checks if the ally is not within the nearFollowDistance of Twig")]
    public class NotWithinNearFollowDistance : GOCondition
    {

        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                //Debug.Log("notNearMidpointRes: " + body.isMidpointNearTwig());
                return Vector3.Distance(gameObject.transform.position, body.Player.transform.position) > body.allyManager.nearFollowDistance;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}