﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/AllySpecific/AllyAmIThreatened")]
    [Help("checks if an enemy is within this allys threatRange")]
    public class AllyAmIThreatened : GOCondition
    {

        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                PLY_AllyManager am = body.allyManager;
                GameObject nearestEnemy = am.getNearestEnemy(gameObject.transform.position, false);
                if (nearestEnemy == null)
                {
                    //Debug.Log("nearestEnemy is null in checkAlly");
                    return false;
                }
                //Debug.Log("am I threatened res: " + (Vector3.Distance(nearestEnemy.transform.position, gameObject.transform.position) < brain.threatRange));
                bool result = Vector3.Distance(nearestEnemy.transform.position, gameObject.transform.position) < body.threatRange;
                //Debug.Log("am I threatened = " + result + ", Distance= " + Vector3.Distance(nearestEnemy.transform.position, gameObject.transform.position) + "threatRange = " + body.threatRange);
                return result;

            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}