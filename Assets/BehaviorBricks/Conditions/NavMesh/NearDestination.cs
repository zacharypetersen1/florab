﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/NavMesh/NearDestination")]
    [Help("Checks if the monster is near destination")]
    public class NearDestination : GOCondition
    {

        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                bool result = body.isAtPosition(body.endPoint);
                Debug.Log("near Position: " + result);
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}