﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Other/SwitchTreeSearch")]
    [Help("switches to the monster search tree")]
    public class SwitchTreeSearch : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;

        public override void OnStart()
        {
            //Debug.Log("ChangeGoal");

            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                body.CurrentTree = ENT_Body.BehaviorTree.lookaroundTree; //actually sets it to work tree
            }
            catch (Exception e)
            {
                throw e;
            }
            //monster = gameObject.GetComponent<DBG_MonsterTest>();
            //monster.attack();
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
