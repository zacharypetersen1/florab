﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Other/EndEnemyAbility")]
    [Help("switches to the previous tree if ally is in allyActivateTree")]
    public class EndEnemyAbility : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;

        public override void OnStart()
        {
            //Debug.Log("ChangeGoal");

            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                ENT_MinionAbility abi = gameObject.GetComponent<ENT_MinionAbility>();
                if (abi != null)
                {
                    abi.endEnemyAbility();
                }
                body.CurrentTree = ENT_Body.BehaviorTree.AllyActivateTree;
            }
            catch (Exception e)
            {
                throw e;
            }
            //monster = gameObject.GetComponent<DBG_MonsterTest>();
            //monster.attack();
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
