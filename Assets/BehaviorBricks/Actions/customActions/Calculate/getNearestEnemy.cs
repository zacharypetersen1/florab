﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Calculate/getNearestEnemy")]
    [Help("gets the enemy nearest to the current gameObject and sets outparam nearestEnemy to equal it")]
    public class getNearestEnemy : GOAction
    {

        [OutParam("nearestEnemy")]
        [Help("closest enemy")]
        public GameObject nearestEnemy;

        public override void OnStart()
        {
            //Debug.Log("TemplateAction");

            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                nearestEnemy = body.getNearestEnemy();
                //Debug.Log("waitPosition: " + waitPosition);

                //monster.ActionFunctionName();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}