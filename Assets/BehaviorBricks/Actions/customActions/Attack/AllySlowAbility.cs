﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Attack/AllySlowAbility")]
    [Help("activates the slow ability on a slow minion that is an ally")]
    public class AllySlowAbility : GOAction
    {
        public override void OnStart()
        {
            //Debug.Log("attack");

            try
            {
                ENT_Body monsterBody = gameObject.GetComponent<ENT_Body>();
                //need this check as otherwise BB will sometimes run this node once more before switching trees
                if (!monsterBody.IsAbilityActive && monsterBody.CurrentTree == ENT_Body.BehaviorTree.AllyActivateTree)
                {
                    gameObject.GetComponent<ENT_SlowAbility>().activateAllyAbility();
                }
                monsterBody.CurrentTree = ENT_Body.BehaviorTree.AllyActivateTree;
            }
            catch (Exception e)
            {
                throw e;
            }
            //monster = gameObject.GetComponent<DBG_MonsterTest>();
            //monster.attack();
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}

