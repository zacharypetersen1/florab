﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Attack/AllyAttack")]
    [Help("attack action for allied monsters. attacks nearest enemy, assumes it is within attackRange")]
    public class AllyAttack : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;

        public override void OnStart()
        {
            //Debug.Log("attack");

            try
            {
                ENT_Body monsterBody = gameObject.GetComponent<ENT_Body>();
                //Debug.Log("attack Node");
                if (monsterBody.attackCooldown <= 0) monsterBody.shouldAttack = true;
                //GameObject nearestEnemy = monsterBody.getNearestEnemy();
                //monsterBody.attackTarget(nearestEnemy);
            }
            catch (Exception e)
            {
                throw e;
            }
            //monster = gameObject.GetComponent<DBG_MonsterTest>();
            //monster.attack();
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
