﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Attack/AllyAttackTarget")]
    [Help("this ally attacks whatever value is in their AttackTarget")]
    public class AllyAttackTarget : GOAction
    {
        public override void OnStart()
        {
            //Debug.Log("TemplateAction");

            try
            {
                ENT_Body monster = gameObject.GetComponent<ENT_Body>();
                //Debug.Log("attackTarget node");
                if (monster.AttackTarget != null && monster.attackCooldown <= 0) monster.shouldAttack = true;
                //monster.ActionFunctionName();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}