﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Attack/EnemyAttack")]
    [Help("attack action for enemy monsters. attacks nearest ally, assumes it is within attackRange")]
    public class EnemyAttack : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;

        public override void OnStart()
        {
            //Debug.Log("attack");

            try
            {
                ENT_Body monsterBody = gameObject.GetComponent<ENT_Body>();
                if (monsterBody.attackCooldown <= 0) monsterBody.shouldAttack = true;
            }
            catch (Exception e)
            {
                throw e;
            }
            //monster = gameObject.GetComponent<DBG_MonsterTest>();
            //monster.attack();
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
