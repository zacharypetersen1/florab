﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/TeleportToAttackTarget")]
    [Help("teleports the monster near their attackTarget")]
    public class TeleportToAttacktarget : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;
        ENT_Body monsterBody;
        public override void OnStart()
        {
            monsterBody = gameObject.GetComponent<ENT_Body>();
            Vector3 point = monsterBody.AttackTarget.transform.position;
            float distToSpawn = 20.0f;
            Vector2 offset = Random.insideUnitCircle * distToSpawn;
            Vector3 pointToSpawn = new Vector3((point.x + offset.x), point.y, (point.z + offset.y));
            float distToCheck = 2.5f;
            bool pointIsValid;
            do
            {
                pointToSpawn = monsterBody.ValidifyPoint(pointToSpawn, out pointIsValid, distToCheck);
                distToCheck *= 2;
            } while (!pointIsValid);
            monsterBody.teleport(pointToSpawn);
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}