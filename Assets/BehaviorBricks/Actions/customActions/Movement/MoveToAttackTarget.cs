﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Attack/MoveToAttackTarget")]
    [Help("this ally attacks whatever value is in their AttackTarget")]
    public class MoveToAttackTarget : GOAction
    {
        public override void OnStart()
        {
            //Debug.Log("TemplateAction");

            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                if (body.AttackTarget != null)
                {
                    Vector3 vec = gameObject.transform.position - body.AttackTarget.transform.position;
                    Vector3 dirFromTarget = vec.normalized;
                    Vector3 finalPos;
                    if (body.entity_type == ENT_DungeonEntity.monsterTypes.mushroomMinion)
                    {
                        finalPos = dirFromTarget * (body.attackRange * 0.5F);
                    }
                    else if (body.entity_type == ENT_DungeonEntity.monsterTypes.treeMinion || body.entity_type == ENT_DungeonEntity.monsterTypes.slowMinion)
                    {
                        finalPos = dirFromTarget * (body.attackRange * 0.25F);
                    }
                    else
                    {
                        finalPos = dirFromTarget * (body.attackRange * 0.8F);
                    }
                    finalPos = body.AttackTarget.transform.position + finalPos;
                    if (!body.isAtPosition(finalPos))
                        body.goToPosition(finalPos);
                }
                //monster.ActionFunctionName();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}