﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/Wait")]
    [Help("Tells minions to wait")]
    public class Wait : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;

        public override void OnStart()
        {
            //Debug.Log("attack");

            try
            {
                //ENT_Brain monsterBrain = gameObject.GetComponent<ENT_Brain>();
                ENT_Body monsterBody = gameObject.GetComponent<ENT_Body>();
                monsterBody.WaitPos = gameObject.transform.position;
                monsterBody.CurrentTree = ENT_Body.BehaviorTree.AllyWaitTree;
                //monsterBody.endPoint = gameObject.transform.position;
                //Debug.Log("waiting");
            }
            catch
            {
                throw new UnityException("Wait behavior is broken and sad");
            }
            //monster = gameObject.GetComponent<DBG_MonsterTest>();
            //monster.attack();
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
