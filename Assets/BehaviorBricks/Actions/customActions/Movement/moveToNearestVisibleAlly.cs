﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/MoveToNearestVisibleAlly")]
    [Help("Moves the monster towards the nearest allied position")]
    public class CustomMoveNearestVisibleAlly : GOAction
    {
        public override void OnStart()
        {
            //Debug.Log("TemplateAction");

            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                PLY_AllyManager am = body.allyManager;
                GameObject nearestAlly;
                nearestAlly = am.getNearestVisibleAlly(gameObject.transform.position, body.sightRange, true, false);
                if (nearestAlly != null)
                {
                    Vector3 vec = gameObject.transform.position - nearestAlly.transform.position;
                    Vector3 dirFromTarget = vec.normalized;
                    Vector3 finalPos;
                    if (body.entity_type == ENT_DungeonEntity.monsterTypes.treeMinion)
                    {
                        finalPos = dirFromTarget * (body.attackRange * 0.25F);
                    }
                    else if (body.entity_type == ENT_DungeonEntity.monsterTypes.mushroomMinion)
                    {
                        finalPos = dirFromTarget * (body.attackRange * 0.5F);
                    }
                    else
                    {
                        finalPos = dirFromTarget * (body.attackRange * 0.8F);
                    }

                    if(Vector3.Distance(gameObject.transform.position, nearestAlly.transform.position) < Vector3.Distance(gameObject.transform.position, nearestAlly.transform.position + finalPos))
                    {
                        finalPos = gameObject.transform.position;
                    }else
                    {
                        finalPos = nearestAlly.transform.position + finalPos;
                    }             
                    if (!body.isAtPosition(finalPos))
                        body.goToPosition(finalPos);
                }
                    
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}