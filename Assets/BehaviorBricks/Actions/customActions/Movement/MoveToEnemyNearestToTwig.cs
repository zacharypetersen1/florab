﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/MoveToEnemyNearestToTwig")]
    [Help("Moves the monster towards the most pressing threat to twig")]
    public class MoveToEnemyNearestToTwig : GOAction
    {
        public override void OnStart()
        {
            //Debug.Log("TemplateAction");

            try
            {
                //ENT_Brain monsterBrain = gameObject.GetComponent<ENT_Brain>();
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                body.goToPosition(body.allyManager.getNearestEnemyToTwig().transform.position);
                //monster.ActionFunctionName();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}