﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/MoveToPlayer")]
    [Help("Gets the component of a given type if the game object has one attached, null if it doesn't")]
    public class MoveToPlayer : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;

        public override void OnStart()
        {
            //Debug.Log("move");

            try
            {
                //ENT_Brain monsterBrain = gameObject.GetComponent<ENT_Brain>();
                ENT_Body monsterBody = gameObject.GetComponent<ENT_Body>();
                monsterBody.goToPlayer();
            }
            catch (Exception e)
            {
                throw e;
            }
            //monster = gameObject.GetComponent<DBG_MonsterTest>();
            //monster.goToPlayer();
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}