﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/rotateTowardsNearestAlly")]
    [Help("Tells the character to rotate towards the nearest enemy")]
    public class rotateTowardsNearestAlly : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;

        public override void OnStart()
        {
            ENT_Body body = gameObject.GetComponent<ENT_Body>();
            body.shouldRotate = true;
            body.lookTowardsVec = body.allyManager.getNearestAlly(gameObject.transform.position).transform.position;
            //body.lookTowardsVec = body.allyManager.getNearestVisibleAlly(gameObject.transform.position, body.sightRange, true).transform.position;
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
