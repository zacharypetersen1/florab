﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/PatrolTargetPath")]
    [Help("Patrols along the target route.")]
    public class PatrolTargetPath : GOAction
    {


        public override void OnStart()
        {
            //Debug.Log("TemplateAction");

            try
            {
                ENT_Body monster = gameObject.GetComponent<ENT_Body>();
                monster.patrol();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
