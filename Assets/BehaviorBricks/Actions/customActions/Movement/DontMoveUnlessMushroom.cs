﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using UnityEngine.AI;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/DontMoveUnlessMushroom")]
    [Help("Tells the character not to move until DoMove node is reached.")]
    public class DontMoveUnlessMushroom : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;

        public override void OnStart()
        {
            //Debug.Log("DontMove");

            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                if (body.entity_type != ENT_DungeonEntity.monsterTypes.mushroomMinion)
                {
                    NavMeshAgent agent = gameObject.GetComponent<NavMeshAgent>();
                    body.endPoint = gameObject.transform.position;
                }
                gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
                gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            }
            catch
            {
                throw new UnityException("DontMove behavior is broken and sad");
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
