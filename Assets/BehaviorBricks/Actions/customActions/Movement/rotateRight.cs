﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/rotateRight")]
    [Help("Tells the character to rotate right")]
    public class rotateRight : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;

        public override void OnStart()
        {
            ENT_Body body = gameObject.GetComponent<ENT_Body>();
            body.rotate(false, 5F);
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
