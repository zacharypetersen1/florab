﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressurePlate : MonoBehaviour {

    public int max_count;
    private int count;
    Vector3 plate;
    Vector3 new_pos_on;
    public ENV_RotateToTarget rot1;
    public ENV_RotateToTarget rot2;
    public SpriteRenderer rend;

    // Use this for initialization
    void Start () {
        plate = transform.position;
        new_pos_on = new Vector3(plate.x, plate.y - .5f, plate.z);
        rend.color = Color.gray;
    }
	
	// Update is called once per frame
	void Update () {

        if (count >= max_count)//Will move the plate down if activated
        {
            transform.position = Vector3.Slerp(new_pos_on, plate, .15f);
            rot1.targetVal = 0;
            rot2.targetVal = 0;
            rend.color = Color.green;
            
        } else if(count < 1)//Will move plate to original pos if not activated
        {
            transform.position = Vector3.Slerp(plate, new_pos_on, .15f);
            rot1.targetVal = 1;
            rot2.targetVal = 1;
            rend.color = Color.gray;
        }
	}

    void FixedUpdate()
    {
        count = 0;
    }

    void OnTriggerStay(Collider collider)
    {
        if (collider.tag == "Minion")
        {
            if(collider.GetComponent<ENT_Body>().isAlly)
            {
                count += 1;
            }
            
        }

        if(collider.tag == "Player")
        {
            count += 1;
        }
    }

    void OnTriggerExit(Collider collider)
    {
        /*if (collider.tag == "Minion")
        {
            if (collider.GetComponent<ENT_Body>().isAlly)
            {
                count -= 1;
            }

        }

        if (collider.tag == "Player")
        {
            count -= 1;
        }*/
    }
}
