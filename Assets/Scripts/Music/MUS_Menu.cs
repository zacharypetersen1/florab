﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MUS_Menu : MonoBehaviour {

    [FMODUnity.EventRef]
    public string music_menu = "event:/music_menu";
    FMOD.Studio.EventInstance musMenuEvent;

    FMOD.Studio.PLAYBACK_STATE musMenuState;

    public bool playMusic = true;
    float inMenu = 1f;
    float inMenuInterp = 0.005f;

    /*
    void Awake()
    {
        DontDestroyOnLoad(this);
        if(FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
        }
    }
    */

    // Use this for initialization
    void Start () {
        musMenuEvent = FMODUnity.RuntimeManager.CreateInstance(music_menu);
        musMenuEvent.getPlaybackState(out musMenuState);
        musMenuEvent.start();
    }
	
	// Update is called once per frame
	void Update () {
        inMenu += playMusic ? -inMenuInterp : inMenuInterp;
        inMenu = Mathf.Clamp(inMenu, 0, 1);
        musMenuEvent.setParameterValue("outOfMenu", inMenu);
        
    }

    void OnDestroy()
    {
        musMenuEvent.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }

    public void stopMusic()
    {
        Debug.Log("stopping music");
        musMenuEvent.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        playMusic = false;
    }

    public void resetMusic()
    {
        if (musMenuEvent != null)
        {
            if (musMenuState == FMOD.Studio.PLAYBACK_STATE.PLAYING)
            {
                musMenuEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            }
        }
        else
        {
            musMenuEvent = FMODUnity.RuntimeManager.CreateInstance(music_menu);
            musMenuEvent.getPlaybackState(out musMenuState);
            musMenuEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            musMenuEvent.start();
        }
    }
}
