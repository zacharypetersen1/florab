﻿//Devon

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//
// Camera state when camera is focused on a location, player has no control
//
public class CAM_Locked : CAM_State {
    private float slideSpeed = 0.00f;


    //
    // runs once per frame
    // Slides camera into position, "f" value controls slide speed
    //
    public override void FixedUpdate()
	{
        slideSpeed += (float)(.5 / distance) * Time.deltaTime;
		cameraScript.transform.position = Vector3.Lerp(cameraScript.transform.position, targetPosition, slideSpeed);
		cameraScript.transform.rotation = Quaternion.Lerp(cameraScript.transform.rotation, targetRotation, slideSpeed);
	}
}
