﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//
// Camera state when camera is targeting an enemy or ability location
//
public class CAM_LockOn : CAM_State
{
    private GameObject player = GameObject.Find("Player");
    private Vector3 NormalToTarget;
    private Vector3 AngleToTarget;
    private Vector3 LookAtPosition;
    private float camOffset = 7f;
    private float camLift =5f;
    private float camStartingDist = 0f;
    private float camDistRatio = 0f;
    private Vector3 camForward;
    private float prevAngle = 180f;
    private int adjustAngle = 1;
    private float slideSpeed = 0.00f;

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	public override void FixedUpdate () {
        //Finds a Target
        /*if (!UTL_Targeting.target)
        {
            UTL_Targeting.getTarget();
        }*/
        if (!UTL_Targeting.target)
        {
            CAM_Camera.setState(CAM_State.type.returning);
        }
        else if (Vector3.Distance(player.transform.position, UTL_Targeting.target.transform.position) > 25f)
        {
            CAM_Camera.setState(CAM_State.type.returning);
            UTL_Targeting.target = null;
        }
        else
        {

            //Accounts For Variable Time Between Frames
            float Delta = Time.deltaTime;
            if (slideSpeed < 10f)
            {
                slideSpeed += (float)(100 / 4) * Delta;
            }
            //Finds New Camera Position
            NormalToTarget = player.transform.position - UTL_Targeting.target.transform.position;
            NormalToTarget = (NormalToTarget.normalized * camOffset);
            NormalToTarget = new Vector3(NormalToTarget.x, camLift, NormalToTarget.z);
            cameraScript.transform.position = Vector3.Lerp(cameraScript.transform.position, player.transform.position + NormalToTarget, slideSpeed * Delta);
            //Adjusts Camera Angle
            AngleToTarget = UTL_Targeting.target.transform.position - dolly.transform.position;
            float newAngle = Vector3.Angle(AngleToTarget, dolly.transform.forward);
            if (newAngle > prevAngle)
            {
                adjustAngle *= -1;
            }
            prevAngle = newAngle;
            dolly.horizontalRotation += newAngle * adjustAngle * Delta * slideSpeed * (1 - (180 - newAngle) / 180);
            //Set Viewing Angle
            if (camStartingDist == 0f)
            {
                camStartingDist = Vector3.Distance(player.transform.position + NormalToTarget, cameraScript.transform.position);
                camForward = cameraScript.transform.forward;
            }
            camDistRatio = Vector3.Distance(player.transform.position + NormalToTarget, cameraScript.transform.position) / camStartingDist;
            LookAtPosition = UTL_Targeting.target.transform.position * (1 - camDistRatio) + (cameraScript.transform.position + 5 * camForward) * camDistRatio;
            cameraScript.transform.LookAt(LookAtPosition);
        }
    }
}
