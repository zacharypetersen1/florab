﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CAM_Rotate : MonoBehaviour {

    private Vector3 rotation;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //transform.Rotate(0, 0.1f, 0);
        transform.Rotate(Vector3.up * Time.deltaTime, Space.World);
    }
}
