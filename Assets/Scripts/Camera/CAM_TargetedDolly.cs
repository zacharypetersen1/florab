﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CAM_TargetedDolly : MonoBehaviour {

    //Transform newPositionTransform;
    Transform targetTransform;
    private Vector3 directionVector = new Vector3(0F, 0F, -1F).normalized; //from CAM_Dolly
    public float directionMag = 8;                                 // magnitude, or distance of camera away
    public float magnitudeUpperClamp = 20;
    public float rotationLimit = 10;
    public float magnitudeLowerClamp = 2;

    // Use this for initialization
    void Start () {
        targetTransform = new GameObject("TargetedCameraDollyTarget").transform;
    }

    void FixedUpdate()
    {
        //transform.position = Vector3.Slerp(transform.position, newPositionTransform.position, 0.15f);
    }

    /*
    public Vector3 setDirectionVector(Vector3 pointA, Vector3 pointB)
    {
        Vector3 normalVector = Vector3.Cross(pointA, pointB).normalized;
        directionVector = normalVector;
        return directionVector;
    }*/
	

    public void setDirectionMag(float magnitude) { directionMag = magnitude; }
    public float getDirectionMag() { return directionMag; }

    public Transform getTargetCamTransform()
    {
        //gets scalar in range [0, 1] to smooth rotation
        //copied almost exactly from CAM_Dolly.getCamTransform() by Zach 
        /*float rotationScalar = Mathf.Min(1f, (directionMag - magnitudeLowerClamp) / (rotationLimit - magnitudeLowerClamp));
        rotationScalar = Mathf.Sin((Mathf.PI / 2) * rotationScalar);
        rotationScalar = 1 - rotationScalar;
        rotationScalar = Mathf.Pow(rotationScalar, 2);*/

        //set target transform to correct value and return
        targetTransform.position = transform.position;
        targetTransform.rotation = transform.rotation;
        //print("directionMag: " + directionMag + "directionVector: " + directionVector);
        targetTransform.Translate(directionVector * directionMag, Space.World);
        //targetTransform.Rotate(new Vector3(0, 0, 180), Space.World);
        //targetTransform.Rotate(new Vector3(30 + 40 * rotationScalar, 0, 180), Space.Self);

        return targetTransform;
    }
}
