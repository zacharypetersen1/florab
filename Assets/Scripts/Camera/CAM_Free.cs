﻿//Zach
//Devon

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//
// Camera state when camera is centered around player and player can control it
//
public class CAM_Free : CAM_State
{
    private Vector3 groundPos;
    private Vector3 adjustedPos;
    private float adjustRatio;
    private int steps;
    public static bool frozen = false;

    //
    // runs once per frame
    // Smoothly follows camera dolly position, "f" value controls slide speed
    //
    public override void FixedUpdate()
    {
        dolly.adjust();

		Transform target = dolly.getCamTransform();
        cameraScript.transform.position = target.position;
        cameraScript.transform.rotation = target.rotation;

        adjustedPos = cameraScript.transform.position;
        groundPos = UTL_Dungeon.snapPositionToTerrain(adjustedPos);
        if (groundPos.y + 1.0f > adjustedPos.y)
        {
            adjustRatio = groundPos.y + 1.0f - adjustedPos.y;
            steps = 0;
            while (groundPos.y + 1.0f > adjustedPos.y && steps < 1000)
            {
                adjustedPos.x += 0.1f * adjustRatio * cameraScript.transform.forward.x;
                adjustedPos.y += 0.1f * adjustRatio * cameraScript.transform.forward.y;
                adjustedPos.z += 0.1f * adjustRatio * cameraScript.transform.forward.z;
                groundPos = UTL_Dungeon.snapPositionToTerrain(adjustedPos);
                steps++;
            }
            if (steps < 1000)
            {
                cameraScript.transform.position = adjustedPos;
            }
        }
    }
}
