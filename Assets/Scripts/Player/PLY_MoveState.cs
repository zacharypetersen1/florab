﻿// Zach

using UnityEngine;
using System.Collections;

public abstract class PLY_MoveState : MonoBehaviour {



    protected CHA_Motor motor;
    protected PLY_PlayerDriver driver;



    //
    // different states of the player's finite state machine
    //
    public enum type
    {
        normal, surf
    }



    //
    // Constructor
    //
    public PLY_MoveState()
    {
        // cannot call StateEnter here, must call after constructing
    }



    //
    // Overridable functions
    //
    virtual public void StateEnter()
    {
        motor = gameObject.GetComponent<CHA_Motor>();
        driver = gameObject.GetComponent<PLY_PlayerDriver>();
    }
    abstract public void StateUpdate();         // must be overiden
    virtual public void StateFixedUpdate() { }  // can be overiden
    
}
