﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class PLY_CorruptionStatus : MonoBehaviour {

    Camera cam;
    CHA_Motor motor;
    ENT_PlayerBody player;

    float corruptionTimer = 0f;
    public float maxCorruptionTimer = 5f;

    BlurOptimized blurComponent;
    float blurSize = 0f;
    float maxBlurSize = 2.5F;

    MotionBlur motionBlurComponent;
    float motionBlur = 0f;
    float maxMotionBlur = .5f;

    VignetteAndChromaticAberration vignetteComponent;
    float vignetteIntestity = 0f;
    float maxVignetteIntensity = .4f;


    private float damageTimer;
    public float MaxDamageTimer = 2;

    public float inAirTimer = 0f;
    public float maxInAirTimer = 1.5f;

    public float waveSpeed = 1;
    private float wavePeriod = 0;

    public float waveSize = 40;

    private bool messageDisplayed = false;

    corruptionStatus playerStatus;
    TrigWave trigWave = null;
    bool didDamage = false;

    // Use this for initialization
    void Start () {
        cam = Camera.main;
        blurComponent = cam.GetComponent<BlurOptimized>();
        motionBlurComponent = cam.GetComponent<MotionBlur>();
        vignetteComponent = cam.GetComponent<VignetteAndChromaticAberration>();

        motor = gameObject.GetComponent<CHA_Motor>();
        player = gameObject.GetComponent<ENT_PlayerBody>();

        damageTimer = 0;
        playerStatus = corruptionStatus.safe;

        vignetteComponent.chromaticAberration = .2f;

        trigWave = new TrigWave(TrigWave.trigType.SINE, waveSpeed, 1, 0);
    }

    enum corruptionStatus
    {
        safe, bufferState, damageState
    }

    // Update is called once per frame
    void Update()
    {

        applyChromaticEffect();

        //set inital buffer zone
        if (playerStatus == corruptionStatus.safe)
        {
            decreaseCorruption();
            if(motor.isTouchingCorruption())
            {
                playerStatus = corruptionStatus.bufferState;
            }
        }

        else if(playerStatus == corruptionStatus.bufferState)
        {
            vignetteComponent.enabled = true;
            motionBlurComponent.enabled = true;

            resetComponent();

            TransitionToSafeState();

            increaseCorruption();
        }
        else if(playerStatus == corruptionStatus.damageState)
        {

            TransitionToSafeState();

            increaseCorruption();

            damagePlayer();
        }
    }

    void damagePlayer()
    {
        if (damageTimer <= 0)
        {
            player.receive_combat_message(new CBI_CombatMessenger(1, 0, 0, null, player.transform.position));
            damageTimer = MaxDamageTimer;
            didDamage = true;
            if (!messageDisplayed)
            {
                TUT_TutorialManager.queueMessage(TUT_Messages.Messages.Corruption);
                messageDisplayed = true;
            }
        }
        else
        {
            damageTimer -= TME_Manager.getDeltaTime(TME_Time.type.environment);
        }
    }

    void applyChromaticEffect()
    {
        float amplitude = (corruptionTimer / maxCorruptionTimer) * waveSize;
        float sinevalue = trigWave.update(TME_Manager.getDeltaTime(TME_Time.type.environment));
        sinevalue = sinevalue * sinevalue;
        sinevalue *= amplitude;

        vignetteComponent.chromaticAberration = .2f + sinevalue;
        
    }

    void TransitionToSafeState()
    {
        if (MAP_NatureMap.natureTypeAtPos(transform.position) != MAP_NatureData.type.corrupt)
        {
            playerStatus = corruptionStatus.safe;
        }
        else if (motor.onGround() == CHA_Motor.groundType.air)
        {
            inAirTimer += TME_Manager.getDeltaTime(TME_Time.type.environment);
            if (inAirTimer > maxInAirTimer)
            {
                playerStatus = corruptionStatus.safe;
                inAirTimer = 0;
            }
        }else
        {
            inAirTimer = 0;
        }
    }

    void increaseCorruption()
    {
        corruptionTimer += TME_Manager.getDeltaTime(TME_Time.type.environment);

        if (corruptionTimer >= maxCorruptionTimer)
        {
            corruptionTimer = maxCorruptionTimer;
        }

        setMotionIntensity(UTL_Math.mapToNewRange(corruptionTimer, 0, maxCorruptionTimer, 0, maxMotionBlur));
        setVignetteIntensity(UTL_Math.mapToNewRange(corruptionTimer, 0, maxCorruptionTimer, 0, maxVignetteIntensity));

        if (corruptionTimer >= maxCorruptionTimer && playerStatus == corruptionStatus.bufferState)
        {
            playerStatus = corruptionStatus.damageState;
        }
    }

    void decreaseCorruption()
    {
        corruptionTimer -= (TME_Manager.getDeltaTime(TME_Time.type.environment));
        if (corruptionTimer <= 0)
        {
            corruptionTimer = 0;
            vignetteComponent.enabled = false;
            motionBlurComponent.enabled = false;
        }

        setMotionIntensity(UTL_Math.mapToNewRange(corruptionTimer, 0, maxCorruptionTimer, 0, maxMotionBlur));
        setVignetteIntensity(UTL_Math.mapToNewRange(corruptionTimer, 0, maxCorruptionTimer, 0, maxVignetteIntensity));

    }

    void setVignetteIntensity(float intensityValue)
    {
        vignetteComponent.intensity = intensityValue;
    }

    void setMotionIntensity(float motionValue)
    {
        motionBlurComponent.blurAmount = motionValue;
    }

    void setBlurSize(float blurValue)
    {
        blurComponent.blurSize = blurValue;
    }

    void resetComponent()
    {
        blurComponent.downsample = 1;
        blurComponent.blurIterations = 2;
    }
}
