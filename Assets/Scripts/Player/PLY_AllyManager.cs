﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PLY_AllyManager : MonoBehaviour {
    public float farFollowDistance = 25;
    public float nearFollowDistance = 20F;
    public float threatenedRange = 8; //controls range at which monsters will attack enemies
    private allyState state;
    public int allyCount = 0;
    public const float FOLLOWDISTANCESCALE = 1.3F; //0.5
    public float MAX_FOLLOW_DIST = 150;
    float recallTimer = 0f;
    public float RECALL_TIMER = 1F;
    Vector3 infinty = new Vector3(Mathf.Infinity, Mathf.Infinity, Mathf.Infinity);
    public const float DISTANCE_TO_STRAY_FROM_GOTO = 15;
    private AIC_AllyGroup _lastCommandIssued;
    public AIC_AllyGroup lastCommandIssued
    {
        get {
            if (_lastCommandIssued != null)
            {
                return _lastCommandIssued;
            }
            else
            {
                return followGroup;
            }
        }
        set
        {
            _lastCommandIssued = value;
        }
    }
    //[HideInInspector]
    //public Vector3 compareToPos;

    //public List<float> allyFollowDistances; 

    public enum allyState
    {
        follow, wait, goTo, activate, attack
    }

    public List<AIC_AllyGroup> commandGroups = new List<AIC_AllyGroup>();
    public AIC_AllyGroup followGroup;
    public COM_AllyCommands commandModule;
    PLY_PlayerDriver playerDriver;

    List<AIC_AllyGroup> removeList = new List<AIC_AllyGroup>();

    //private Vector3 waitPosition = Vector3.zero;

    private Transform twigTransform;
   
    private List<GameObject> ally_group_1 = new List<GameObject>();
    private List<GameObject> ally_group_2 = new List<GameObject>();
    private List<GameObject> ally_group_3 = new List<GameObject>();
    private List<GameObject> ally_group_4 = new List<GameObject>();
    private List<GameObject> ally_group_5 = new List<GameObject>();
    private List<GameObject> ally_group_6 = new List<GameObject>();
    private List<GameObject> ally_group_7 = new List<GameObject>();
    private List<List<GameObject>> allies = new List<List<GameObject>>();
    private List<GameObject> allAllies = new List<GameObject>();
    private List<GameObject> allStructures = new List<GameObject>();

    private List<GameObject> enemies = new List<GameObject>();

    public List<List<GameObject>> getAllies() { return allies; }
    public List<GameObject> getEnemies() { return enemies; }

    public List<GameObject> getAllEnemies
    {
        get { return enemies; }
    }

    public List<GameObject> getAllAllies
    {
        get { return allAllies; }
    }

    public List<GameObject> getAllStructures
    {
        get { return allStructures; }
    }

    private void Awake()
    {
        allies.Add(ally_group_1);
        allies.Add(ally_group_2);
        allies.Add(ally_group_3);
        allies.Add(ally_group_4);
        allies.Add(ally_group_5);
        allies.Add(ally_group_6);
        allies.Add(ally_group_7);

        followGroup = new AIC_AllyGroup(allyState.follow, new List<GameObject>(), this, transform.position);
        commandGroups.Add(followGroup);
        commandModule = gameObject.GetComponent<COM_AllyCommands>();
        playerDriver = gameObject.GetComponent<PLY_PlayerDriver>();
    }

    #region setStateFunctions
    public void setState(GameObject ally, allyState newState)
    {
        ENT_Body body = ally.GetComponent<ENT_Body>();
        if (body == null)
        {
            Debug.LogWarning("Error: cannot setState of " + ally.name + "because it does not have an ENT_Body component");
            return;
        }
        if (!body.Purity)
        {
            Debug.LogWarning("Error: cannot setState of " + ally.name + "because it is an enemy");
            return;
        }
        switch (newState)
        {
            case allyState.follow:
                body.CurrentTree = ENT_Body.BehaviorTree.AllyFollowTree;
                break;
            case allyState.wait:
                body.WaitPos = body.transform.position;
                body.CurrentTree = ENT_Body.BehaviorTree.AllyWaitTree;
                break;
            case allyState.goTo:
                Debug.LogWarning("Error: must pass in a gotoPosition in order to change state to go to. Try passing in a Vector3 position to go to.");
                //body.CurrentTree = ENT_Body.BehaviorTree.AllyGoToTree;
                return;
            case allyState.activate:
                if (ally.GetComponent<ENT_MinionAbility>() == null)
                {
                    return;
                }
                body.CurrentTree = ENT_Body.BehaviorTree.AllyActivateTree;
                break;
            case allyState.attack:
                Debug.LogWarning("Error: must pass in a target to attack when changing state to Attack. Try passing in the enemy to attack.");
                return;
        }
    }

    public void setState(GameObject ally, allyState newState, Vector3 goToPos)
    {
        ENT_Body body = ally.GetComponent<ENT_Body>();
        if (body == null)
        {
            Debug.LogWarning("Error: cannot setState of " + ally.name + "because it does not have an ENT_Body component");
            return;
        }
        if (!body.Purity)
        {
            Debug.LogWarning("Error: cannot setState of " + ally.name + "because it is an enemy");
            return;
        }
        switch (newState)
        {
            case allyState.follow:
                body.CurrentTree = ENT_Body.BehaviorTree.AllyFollowTree;
                break;
            case allyState.wait:
                body.WaitPos = body.transform.position;
                body.CurrentTree = ENT_Body.BehaviorTree.AllyWaitTree;
                break;
            case allyState.goTo:
                //need to set gotoposition
                body.WaitPos = goToPos;
                body.CurrentTree = ENT_Body.BehaviorTree.AllyGoToTree;
                break;
            case allyState.activate:
                if (ally.GetComponent<ENT_MinionAbility>() == null)
                {
                    return;
                }
                body.CurrentTree = ENT_Body.BehaviorTree.AllyActivateTree;
                break;
            case allyState.attack:
                Debug.LogWarning("Error: must pass in a target to attack when changing state to Attack. Try passing in the enemy to attack.");
                return;
        }
    }

    public void setState(GameObject ally, allyState newState, GameObject attackTarget)
    {
        ENT_Body body = ally.GetComponent<ENT_Body>();
        if (body == null)
        {
            Debug.LogWarning("Error: cannot setState of " + ally.name + "because it does not have an ENT_Body component");
            return;
        }
        if (!body.Purity)
        {
            Debug.LogWarning("Error: cannot setState of " + ally.name + "because it is an enemy");
            return;
        }
        switch (newState)
        {
            case allyState.follow:
                body.CurrentTree = ENT_Body.BehaviorTree.AllyFollowTree;
                break;
            case allyState.wait:
                body.WaitPos = body.transform.position;
                body.CurrentTree = ENT_Body.BehaviorTree.AllyWaitTree;
                break;
            case allyState.goTo:
                Debug.LogWarning("Error: must pass in a gotoPosition in order to change state to go to. Try passing in a Vector3 position to go to.");
                //body.CurrentTree = ENT_Body.BehaviorTree.AllyGoToTree;
                return;
            case allyState.activate:
                if (ally.GetComponent<ENT_MinionAbility>() == null)
                {
                    return;
                }
                body.CurrentTree = ENT_Body.BehaviorTree.AllyActivateTree;
                break;
            case allyState.attack:
                body.AttackTarget = attackTarget;
                body.CurrentTree = ENT_Body.BehaviorTree.AllyAttackTree;
                break;
        }
    }

    public void setAllAlliesState(allyState newState)
    {
        //if (state == newState) return;
        state = newState;
        foreach (List<GameObject> group in allies)
        {
            foreach (GameObject ally in group)
            {
                setState(ally, newState);
            }
        }
    }

    public void setAllAlliesState(allyState newState, Vector3 goToPos)
    {
        //if (state == newState) return;
        state = newState;
        foreach (List<GameObject> group in allies)
        {
            foreach (GameObject ally in group)
            {
                setState(ally, newState, goToPos);
            }
        }
    }

    public void setAllAlliesState(allyState newState, GameObject target)
    {
        //if (state == newState) return;
        state = newState;
        foreach (List<GameObject> group in allies)
        {
            foreach (GameObject ally in group)
            {
                setState(ally, newState, target);
            }
        }
    }
    #endregion

    public allyState getState()
    {
        return state;
    }


    public void addStructure(GameObject newStructure)
    {
        allStructures.Add(newStructure);
    }

    public void removeStructure(GameObject exStructure)
    {
        allStructures.Remove(exStructure);
    }

    public AIC_AllyGroup addAlly(GameObject newAlly)
    {
        ENT_Body body = newAlly.GetComponent<ENT_Body>();
        if (body == null) return null;
        if (!body.Purity)
        {
            print("Error: Tried to add an evil monster " + newAlly.name + "to ally list");
            return null;
        }
        allAllies.Add(newAlly);
        allies[(int)body.getMyType()].Add(newAlly);
        //allyFollowDistances.Add(allyCount);
        allyCount++;
        //setAllAlliesState(getState());
        //print("Added " + newAlly + " to allAllies");
        //return setCorrectGroup(newAlly);
        commandModule.FollowCommand(new List<GameObject>() { newAlly });
        //print("added " + newAlly.name + " to follow group");
        return followGroup;
        //print("addAlly called with object: " + newAlly.name);
    }

    public AIC_AllyGroup setCorrectGroup(GameObject newAlly)
    {
        switch(lastCommandIssued.State)
        {
            case allyState.follow:
                setState(newAlly, allyState.follow);
                followGroup.addToList(newAlly);
                return followGroup;
            case allyState.goTo:
                setState(newAlly, allyState.goTo, lastCommandIssued.Destination);
                lastCommandIssued.addToList(newAlly);
                return lastCommandIssued;
            case allyState.attack:
                setState(newAlly, allyState.attack, lastCommandIssued.Target);
                lastCommandIssued.addToList(newAlly);
                return lastCommandIssued;
            case allyState.wait:
                setState(newAlly, allyState.wait, lastCommandIssued.Destination);
                lastCommandIssued.addToList(newAlly);
                return lastCommandIssued;
            default:
                setState(newAlly, allyState.follow);
                followGroup.addToList(newAlly);
                return followGroup;
        }
    }

    public void addEnemy(GameObject newEnemy)
    {
        ENT_Body body = newEnemy.GetComponent<ENT_Body>();
        if (body == null) return;
        if (body.Purity)
        {
            print("Error: Tried to add a good creature " + newEnemy.name + "to enemy list");
            return;
        }
        enemies.Add(newEnemy);
        body.CurrentTree = ENT_Body.BehaviorTree.WorkTree;
    }

    public void removeAlly(GameObject exAlly)
    {
        foreach(List<GameObject> group in allies)
        {
            if (group.Contains(exAlly))
            {
                
                group.Remove(exAlly);
                //allyFollowDistances.RemoveAt(allyCount); //remove element at end of array
                allyCount--;
            }

        }
        allAllies.Remove(exAlly);
        //else print("Warning: Tried to remove " + exAlly.name + " From Ally list but it is not in Ally list");
    }

    public void removeEnemy(GameObject exEnemy)
    {
        if (enemies.Contains(exEnemy)) enemies.Remove(exEnemy);
        //else print("Warning: Tried to remove " + exEnemy.name+ " From enemy list but it is not in enemy list");
    }

    //return null if no enemies in enemy list
    public GameObject getNearestEnemy(Vector3 pos, bool includeStructures = true)
    {
        GameObject nearest = null;
        float leastDist = 1000000;
        foreach (GameObject enemy in enemies)
        {
            float dist = Vector3.Distance(pos, enemy.transform.position);
            if (dist < leastDist)
            {
                nearest = enemy;
                leastDist = dist;
            }
        }
        if (includeStructures)
        {
            foreach (GameObject structure in allStructures)
            {
                float dist = Vector3.Distance(pos, structure.transform.position);
                if (dist < leastDist)
                {
                    nearest = structure;
                    leastDist = dist;
                }
            }
        }
        //Debug.Log(nearest.name);
        return nearest;
    }

    public GameObject getNearestEnemyToTwig()
    {
        return getNearestEnemy(transform.position);
    }

    public GameObject getNearestVisibleEnemy(Vector3 pos, float maxRange, bool includeStructures = true)
    {
        GameObject nearest = null;
        float leastDist = maxRange;
        foreach (GameObject enemy in enemies)
        {
            if (!UTL_Dungeon.checkLOSWalls(pos, enemy.transform.position)) continue;
            float dist = Vector3.Distance(pos, enemy.transform.position);
            if (dist < leastDist)
            {
                nearest = enemy;
                leastDist = dist;
            }
        }
        if (includeStructures)
        {
            foreach (GameObject structure in allStructures)
            {
                if (!UTL_Dungeon.checkLOSWalls(pos, structure.transform.position)) continue;
                float dist = Vector3.Distance(pos, structure.transform.position);
                if (dist < leastDist)
                {
                    nearest = structure;
                    leastDist = dist;
                }
            }
        }
        return nearest;
    }

    public GameObject getNearestVisibleEnemyWithinSphere(Vector3 pos, float maxRange, Vector3 center, float radius, bool includeStructures = true)
    {
        GameObject nearest = null;
        float leastDist = maxRange;
        foreach (GameObject enemy in enemies)
        {
            if (!UTL_Dungeon.checkLOSWalls(pos, enemy.transform.position)) continue;
            float dist = Vector3.Distance(pos, enemy.transform.position);
            if (dist < leastDist)
            {
                if (Vector3.Distance(enemy.transform.position, center) <= radius)
                {
                    nearest = enemy;
                    leastDist = dist;
                }
            }
        }
        if (includeStructures)
        {
            foreach (GameObject structure in allStructures)
            {
                if (!UTL_Dungeon.checkLOSWalls(pos, structure.transform.position)) continue;
                float dist = Vector3.Distance(pos, structure.transform.position);
                if (Vector3.Distance(structure.transform.position, center) <= radius)
                {
                    nearest = structure;
                    leastDist = dist;
                }
            }
        }
        return nearest;
    }

    public GameObject getNearestAlly(Vector3 pos, bool includeTwig = true)
    {
        GameObject nearest = null;
        float leastDist = 1000000;
        foreach (List<GameObject> group in allies)
        {
            foreach (GameObject ally in group)
            {
                float dist = Vector3.Distance(pos, ally.transform.position);
                if (dist < leastDist)
                {
                    nearest = ally;
                    leastDist = dist;
                }
            }
        }
        if (includeTwig)
        {
            float dist = Vector3.Distance(pos, gameObject.transform.position);
            if (dist < leastDist)
            {
                nearest = gameObject;
                leastDist = dist;
            }
        }
        return nearest;
    }

    public GameObject getNearestVisibleAlly(Vector3 pos, float maxRange, bool includeTwig = true, bool prioritizeTwig = false)
    {

        GameObject nearest = null;
        float leastDist = maxRange;
        if (includeTwig && prioritizeTwig)
        {
            if (UTL_Dungeon.checkLOSWalls(pos, gameObject.transform.position))
            {
                float dist = Vector3.Distance(pos, gameObject.transform.position);
                if (dist < leastDist)
                {
                    nearest = gameObject;
                    leastDist = dist;
                }
            }
            if (prioritizeTwig && nearest != null) return nearest;
        }
        foreach (List<GameObject> group in allies)
        {
            foreach (GameObject ally in group)
            {
                if (!UTL_Dungeon.checkLOSWalls(pos, ally.transform.position)) continue;
                float dist = Vector3.Distance(pos, ally.transform.position);
                if (dist < leastDist)
                {
                    nearest = ally;
                    leastDist = dist;
                }
            }
            if (nearest != null && !includeTwig) return nearest;
        }
        if (includeTwig)
        {
            if (UTL_Dungeon.checkLOSWalls(pos, gameObject.transform.position))
            {
                float dist = Vector3.Distance(pos, gameObject.transform.position);
                if (dist < leastDist)
                {
                    nearest = gameObject;
                    leastDist = dist;
                }
            }
        }
        
        return nearest;
    }

    public Vector3 getMidpoint()
    {
        Vector3 tempVec = Vector3.zero;
        int ally_count = 0;
        foreach (List<GameObject> group in allies)
        {
            foreach(GameObject ally in group)
            {
                ++ally_count;
                tempVec += ally.transform.position;
            }
            
        }
        tempVec /= ally_count;
        return tempVec;
    }

    public bool isMidpointNearTwig()
    {
        Vector3 midpoint = getMidpoint();
        return Vector3.Distance(twigTransform.position, midpoint) < farFollowDistance;
    }

    public bool amINearTwig(Vector3 myPos)
    {
        return Vector3.Distance(twigTransform.position, myPos) < (nearFollowDistance);
    }

    /*public Vector3 get
     * ()
    {
        //print("waitPosition: " + waitPosition);
        return waitPosition;
    }*/

    public void setWaitPosition()
    {
        foreach (List<GameObject> group in allies)
        {
            foreach (GameObject ally in group)
            {
                ally.GetComponent<ENT_Body>().WaitPos = twigTransform.position;
            }
        }
    }

    public void setWaitPosition(Vector2 newWaitPos)
    {
        foreach (List<GameObject> group in allies)
        {
            foreach (GameObject ally in group)
            {
                ally.GetComponent<ENT_Body>().WaitPos = newWaitPos;
            }
        }
    }

    public List<GameObject> getAllAlliesWithinRange(Vector3 pos, float range, bool includeTwig = false)
    {
        List<GameObject> inRange = new List<GameObject>();
        foreach (GameObject ally in allAllies)
        {
            if (Vector3.Distance(pos, ally.transform.position) <= range)
            {
                inRange.Add(ally);
            }
        }
        if (includeTwig)
        {
            if (Vector3.Distance(pos, transform.position) <= range)
            {
                inRange.Add(gameObject);
            }
        }
        return inRange;
    }

    public List<GameObject> getAllEnemiesWithinRange(Vector3 pos, float range)
    {
        List<GameObject> inRange = new List<GameObject>();
        foreach (GameObject enemy in enemies)
        {
            if (Vector3.Distance(pos, enemy.transform.position) <= range)
            {
                inRange.Add(enemy);
            }
        }
        return inRange;
    }
    /*

    public bool amINearWaitPos(Vector3 myPos)
    {
        return Vector3.Distance(waitPosition, myPos) < individualFollowDistance;
    }

    public bool isMidpointNearWaitPos()
    {
        Vector3 midpoint = getMidpoint();
        return Vector3.Distance(waitPosition, midpoint) < midpointFollowDistance;
    }*/


    // Use this for initialization
    void Start () {
        twigTransform = gameObject.transform;
        setWaitPosition();
	}
	
	// Update is called once per frame
	void Update () {
        //setFollowDistances();
        removeList.Clear();
        foreach (AIC_AllyGroup group in commandGroups)
        {
            if (group.State == allyState.attack && (group.Target == null || group.Target.GetComponent<ENT_DungeonEntity>().isAlly || group.Target.GetComponent<ENT_DungeonEntity>().State == "cleansed"))
            {
                //commandModule.FollowCommand(group.AllyList);
                //followGroup.setFollowDistances();
                removeList.Add(group);
            }
            /*else if (group.State == allyState.goTo || group.State == allyState.wait)
            {
                group.assignGoToPoints();
            }*/
            else group.setFollowDistances();
        }
        foreach (AIC_AllyGroup removeGroup in removeList)
        {
            commandModule.FollowCommand(new List<GameObject>(removeGroup.AllyList));
            followGroup.setFollowDistances();
        }
        recallTimer += TME_Manager.getDeltaTime(TME_Time.type.master);
        if (recallTimer >= RECALL_TIMER && !(playerDriver.activeState == PLY_MoveState.type.surf))
        {
            recallMinions();
            recallTimer = 0;
        }
        
    }

    public List<GameObject> getAlliesOfType(ENT_DungeonEntity.monsterTypes type)
    {
        return allies[(int)type];
    }

    public void recallMinions(List<GameObject> minionsToRecall = null)
    {
        if (minionsToRecall == null)
        {
            minionsToRecall = new List<GameObject>(followGroup.AllyList);
        }   
        float startAngle = Random.Range(0F, 360F);
        //List<Vector3> goToPoints = new List<Vector3>();
        float dist = 5F; //1.5 times navmesh diameter
        bool offset = false;
        List<GameObject> recallList = new List<GameObject>();
        foreach (GameObject minion in minionsToRecall)
        {
            NavMeshAgent agent = minion.GetComponent<NavMeshAgent>();
            Animator anim = minion.GetComponentInChildren<Animator>();
            bool appearing = (anim.GetCurrentAnimatorStateInfo(0).IsName("Appear"));
            if (Vector3.Distance(minion.transform.position, transform.position) > MAX_FOLLOW_DIST || (agent.pathStatus != NavMeshPathStatus.PathComplete && !appearing)) // 
            {
                if (!minion.GetComponent<ENT_Body>().reappear)
                {
                    recallList.Add(minion);
                }
            }
        }
       for (int pointsAssigned = 0; pointsAssigned < recallList.Count; offset = !offset)
        {
            for (float angle = 0F; angle <= 360F; angle += 60F)
            {
                float tempAngle = angle + (offset ? 30 : 0) + startAngle + Random.Range(-10F, 10F);
                Vector3 unitAngleVector = UTL_Math.angleRadToUnitVec((Mathf.Deg2Rad *tempAngle), 0);
                float dot = Vector3.Dot(transform.forward.normalized, unitAngleVector.normalized);
                if (dot > 0)
                {
                    continue;
                }
                Vector3 goToPoint = makeRecallPoint(tempAngle, dist);
                if (goToPoint != infinty)
                {
                    //goToPoints.Add(goToPoint);
                    GameObject minion = recallList[pointsAssigned];
                    minion.GetComponent<ENT_Body>().teleport(goToPoint);
                    pointsAssigned++;
                    if (pointsAssigned >= recallList.Count) break;
                }
            }
            dist += 4f; // 1.5
        }
        //commandModule.FollowCommand(minionsToRecall);
    }

    Vector3 makeRecallPoint(float angle, float dist)
    {
        Vector3 goToPoint = transform.position + UTL_Math.vectorfromAngleAndMagnitude(angle, dist);
        //goToPoint = UTL_Dungeon.snapPositionToTerrain(goToPoint);
        //check if point is valid
        NavMeshHit hit;
        int navmeshMask = 1 << NavMesh.GetAreaFromName("Walkable");
        if (NavMesh.SamplePosition(goToPoint, out hit, 10.0F, navmeshMask))
        {
            return hit.position;
        }
        else return new Vector3(Mathf.Infinity, Mathf.Infinity, Mathf.Infinity);
    }

    //public 
}
