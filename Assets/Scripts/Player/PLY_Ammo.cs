﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLY_Ammo : MonoBehaviour
{
    UIX_AmmoBar ammoBar;
    GameObject twig;
    TerrainData terrainData;

    public float healthRecoverRate = .1f;

    public static PLY_Ammo singleton;

    int[,] bluMap;
    int[,] redMap;

    public enum ammoTypes { blu, red };

    Dictionary<ammoTypes, int> totalAmmoAmount = new Dictionary<ammoTypes, int>
    {
        {ammoTypes.red, 300 }
    };

    Dictionary<ammoTypes, int> curentAmmoAmount = new Dictionary <ammoTypes, int>
    {
        {ammoTypes.red, 0 }
    };

    // Use this for initialization
    void Start()
    {
        twig = GameObject.Find("Player");
        ammoBar = GameObject.Find("Scripts").GetComponent<UIX_AmmoBar>();
        terrainData = Terrain.activeTerrain.terrainData;
        bluMap = terrainData.GetDetailLayer(0, 0, terrainData.detailWidth, terrainData.detailHeight, 1);
        redMap = terrainData.GetDetailLayer(0, 0, terrainData.detailWidth, terrainData.detailHeight, 2);
        singleton = this;

        for (int i = 0; i < bluMap.GetLength(0); i++)
        {
            for (int j = 0; j < bluMap.GetLength(1); j++)
            {
                //if (bluMap[i, j] > 0) Debug.Log("I: " + i + " J: " + j);
            }
        }
        //Debug.Log("Terrain dimensions: " + bluMap.GetLength(0) + ", " + bluMap.GetLength(1));

    }

    // Update is called once per frame
    void Update()
    {
        int bluDetails = bluMap[(int) (twig.transform.position.z + 1024) , (int) (twig.transform.position.x + 1024) ];
        int redDetails = redMap[(int) (twig.transform.position.z + 1024) , (int) (twig.transform.position.x + 1024) ];

        if (bluDetails > 0)
        {
            //Debug.Log("healing");
            twig.GetComponent<ENT_PlayerBody>().inflict_healing(healthRecoverRate);
        }
        if (redDetails > 0)
        {
            addAmmo(ammoTypes.red, 4);
        }

        //Debug.Log("Twig: X: " + ((int) (twig.transform.position.z + 1024) ) + " Y: " + ((int) (twig.transform.position.x + 1024) ));
    }



    //
    // Adds an amount to the current ammo value
    //
    public static void addAmmo(ammoTypes type, int amount)
    {
        singleton.curentAmmoAmount[type] =
            Mathf.Clamp(singleton.curentAmmoAmount[type] + amount, 0, singleton.totalAmmoAmount[type]);
        UIX_AmmoBar.setAmmoValue((float)singleton.curentAmmoAmount[type] / (float)singleton.totalAmmoAmount[type]);
    }

    

    //
    // Checks if there is any ammo left for given type
    //
    public static bool hasAmmo(ammoTypes type)
    {
        return singleton.curentAmmoAmount[type] > 0;
    }



    //
    // Reduces ammo amount by one shot
    //
    public static void doShot(ammoTypes type)
    {
        singleton.curentAmmoAmount[type]--;
    }



    //
    // Returns size of total amount of ammo given ammo type
    //
    public static int getTotalAmmoAmount(ammoTypes type)
    {
        return singleton.totalAmmoAmount[type];
    }
}
