﻿// Zach

using UnityEngine;
using System.Collections;

public class PLY_PlayerDriver : MonoBehaviour {

    public bool spiderTwig = false;
    public string obj_name = "player";          // for loading JSON

    public float normalSpeed = 1;               // speed at which player moves with no altering effects
    public float normalRotVelocity = 2;

    public float treeRootSpeed = 4;             // speed when using treeRoot ability

    public float surfingSpeed = 1.8f;           // stats for Surfing
    public float surfingRotVelMax = 0.3f;
    public float surfingRotVelMin = 0.8f;

    [HideInInspector]
    public float corruptSurfingSpeed = 1.5f;

    [HideInInspector]
    public bool sensingDirtWall = false;        // records if currently looking at a dirt wall
    [HideInInspector]
    public PLY_MoveState.type activeState;   // curent state of player
    [HideInInspector]
    public PLY_MoveState activeStateReference;          
    [HideInInspector]
    public CHA_Motor motor;

    private ABI_Listener listener;
    Animator anim;
    ABI_Moving surfScript;

    int doubleJumped = 3;
    bool _boosted = false;
    public bool boosted
    {
        set { _boosted = value; }
    }

    public float firstJumpSize = 1f;
    public float doubleJumpSize = .75f;

    public static PLY_PlayerDriver singleton;

    // FMOD parameters
    bool isIdle = true;
    float idleVal = 1;
    float idleValInterp = 0.005f;


    //
    // Use this for initialization
    //
    void Start () {
        //Test code: initializing values using the JSN_Manager
        //JSN_Manager.getStats(this, obj_name, "1");
        //Debug.Log(normalSpeed);
        //Debug.Log(treeRootSpeed);
        //Debug.Log(mossSpeed);

        // get the motor and set the speed
        motor = gameObject.GetComponent<CHA_Motor>();
        motor.setSpeed(normalSpeed);
        motor.rotationVelocity = normalRotVelocity;

        // get ability listener/executer
        listener = gameObject.GetComponent<ABI_Listener>();

        // set initial state
        setState(PLY_MoveState.type.normal);

        // Get animator for twig
        anim = GameObject.Find("TwigMesh").GetComponent<Animator>();
        surfScript = (ABI_Moving)ABI_Manager.manager.abilities[ABI_Manager.abilityType.Moving];

        singleton = this;
    }
	
    //
    // perform jump according to conditions
    //
    void jumpHandler()
    {
        // if not using spider twig, don't let player jump when hindering surf
        if (!spiderTwig && motor.shouldHinderSurf() && ABI_Moving.singleton.isSurfing) return;

        if (surfScript.isSurfing)
        {
            if (motor.onGround() != CHA_Motor.groundType.air)
            {
                doSurfJump();
            }
        }
        else
        {
            if (!motor.isDuringJumpResetBuffer())
            {
                if (motor.onGround() == CHA_Motor.groundType.terrainGround)
                {
                    if (MAP_NatureMap.natureTypeAtPos(gameObject.transform.position) == MAP_NatureData.type.grass)
                    {
                        doBoostedJump();
                    }
                    else
                    {
                        doJump();
                    }
                }
                else if (_boosted)
                {
                    doBoostedJump();
                    _boosted = false;
                }
                else if (motor.onGround() == CHA_Motor.groundType.meshGround)
                {
                    doJump();
                }
                else
                {
                    if (doubleJumped < 2)
                    {
                        doDoubleJump();
                    }

                }

            }
        }
    }



    //
    // Executes single jump without extra boost from log
    //
    void doJump()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/twig_jump", transform.position);
        doubleJumped = 1;
        motor.jump(firstJumpSize);
    }



    //
    // Executes single jump with extra boost from log
    //
    void doBoostedJump()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/twig_jump_boosted", transform.position);
        StumpAnim.activateStumpAnim(transform.position);
        motor.jump(firstJumpSize);
        doubleJumped = 1;
    }



    //
    // Executes double jump
    //
    void doDoubleJump()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/twig_jump_air", transform.position);
        motor.jump(doubleJumpSize);
        doubleJumped = 2;
    }



    //
    // Executes jump while surfing
    //
    void doSurfJump()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/twig_jump_boosted", transform.position);
        motor.jump(1.5f);
        anim.SetTrigger("doJump");
        surfScript.onJumpWhileSurfing();
    }




    //
    // Runs once per frame
    //
    void Update()
    {
        if (motor.onGround() != CHA_Motor.groundType.air)
        {
            doubleJumped = 0;
        }

        if (INP_PlayerInput.getButtonDown("Jump") && !motor.isSliding)
        {
            jumpHandler();
        }

        if (!motor.isSliding)
        {
            // check all abilities for input
            listener.checkAllAbilities();

            // run current state's update function
            activeStateReference.StateUpdate();
        }

        // Fade music track for idling
        isIdle = (motor.getVelocity2D() != Vector2.zero) ? false : true;
        idleVal += isIdle ? idleValInterp : -idleValInterp;
        idleVal = Mathf.Clamp(idleVal, 0, 1);
        if (MUS_System.singleton != null) { 
            MUS_System.singleton.MusicIdle(idleVal);
        }

        // Check if player landed while surfing
        if(motor.landed && surfScript.isSurfing)
        {
            surfScript.onLandWhileSurfing();
        }
    }



    //
    // factory function, creates and sets the current state of the player
    //
    public void setState(PLY_MoveState.type state)
    {
        // destroy currently active state and replace it
        Destroy(activeStateReference);
        switch (state)
        {
            case PLY_MoveState.type.normal:    activeStateReference = gameObject.AddComponent<PLY_State_Normal>(); break;
            case PLY_MoveState.type.surf: activeStateReference = gameObject.AddComponent<PLY_StateSurf>(); break;
        }

        // record active state
        activeState = state;

        // must call "StateEnter" function here instead of within constructor
        activeStateReference.StateEnter();
    }





    //
    // Called once per frame at fixed time
    //
    void FixedUpdate () {

        // call update on current active state
        activeStateReference.StateFixedUpdate();
	}
}
