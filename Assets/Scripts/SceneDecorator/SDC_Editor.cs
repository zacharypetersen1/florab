﻿#if (UNITY_EDITOR) 

using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(SDC_Manager))]
public class SDC_Editor : Editor
{
    public override void OnInspectorGUI()
    {

        SDC_Manager script = (SDC_Manager)target;

        //
        //Buttons to set path to certain resource
        //
        if (GUILayout.Button("Update Colors"))
        {
            script.updateColors();
        }

        DrawDefaultInspector();

        //
        //Buttons to set path to certain resource
        //
        if (GUILayout.Button("Acacia Trees"))
        {
            script.assetPath = "Environment/Trees/Acacia_Trees";
        }

        if (GUILayout.Button("Apple Trees"))
        {
            script.assetPath = "Environment/Trees/Apple_Trees";
        }

        if (GUILayout.Button("Birch Trees"))
        {
            script.assetPath = "Environment/Trees/Birch_Tree";
        }

        if (GUILayout.Button("Dead Trees"))
        {
            script.assetPath = "Environment/Trees/Dead Trees";
        }

        if (GUILayout.Button("Fir Trees"))
        {
            script.assetPath = "Environment/Trees/Fir_Trees/OneSided";
        }

        if (GUILayout.Button("Oak Trees"))
        {
            script.assetPath = "Environment/Trees/Oak_Trees";
        }

        if (GUILayout.Button("Palm Trees"))
        {
            script.assetPath = "Environment/Trees/Palm_Trees/OneSided";
        }

        if (GUILayout.Button("Pine Trees"))
        {
            script.assetPath = "Environment/Trees/Pine_Trees/OneSided";
        }

        if (GUILayout.Button("Random Trees"))
        {
            script.assetPath = "Environment/Trees/Random_Trees";
        }

        if (GUILayout.Button("Simple Trees"))
        {
            script.assetPath = "Environment/Trees/Simple_Trees";
        }

        if (GUILayout.Button("Thuja Trees"))
        {
            script.assetPath = "Environment/Trees/Thuja_Trees";
        }

        //
        // Generates all types of decorators, plants, rocks, etc
        //
        if (GUILayout.Button("Generate All Decorators in Scene"))
        {
            script.populateScene();
        }



        //
        // Destroys all types of decorators
        //
        if (GUILayout.Button("Clear All Decorators in Scene"))
        {
            if(EditorUtility.DisplayDialog("Title", "Are you sure you want to destroy all Decorators in the scene? This cannot be undone.", "Yes", "No"))
            {
                script.cleanUpScene();
            }
        }
    }
}
#endif