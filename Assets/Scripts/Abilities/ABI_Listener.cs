﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_Listener : MonoBehaviour {


    
    private ABI_Manager manager;



    //
    // maps ability type to timer for how long button has been held
    //
    public Dictionary<ABI_Manager.abilityType, float> heldTimer = new Dictionary<ABI_Manager.abilityType, float>
    {
        {ABI_Manager.abilityType.Cleansing, 0 },
        {ABI_Manager.abilityType.Moving,    0 },
        {ABI_Manager.abilityType.CommandActivate,  0 },
        {ABI_Manager.abilityType.CommandGoTo,  0 },
        {ABI_Manager.abilityType.CommandFollow,  0 }
    };



    //
    // maps ability type to bool that traks if the ability is being held
    //
    Dictionary<ABI_Manager.abilityType, bool> beingHeld = new Dictionary<ABI_Manager.abilityType, bool>
    {
        {ABI_Manager.abilityType.Cleansing, false },
        {ABI_Manager.abilityType.Moving,    false },
        {ABI_Manager.abilityType.CommandActivate,  false },
        {ABI_Manager.abilityType.CommandGoTo,  false },
        {ABI_Manager.abilityType.CommandFollow,  false }
    };



    // Use this for initialization
    void Start () {
        manager = gameObject.GetComponent<ABI_Manager>();
    }
	


	// Update is called once per frame
	void Update () {
        foreach (var ability in manager.abilities.Values)
        {
            ability.update();
        }
    }



    //
    // checks if player pressed any ability buttons on this frame- then activates corresponding ability
    //
    public void checkAllAbilities()
    {
        // loop through all ability numbers
        for (int i = 0; i <= 1; i++)
        {
            // get ability type
            ABI_Manager.abilityType thisAbility = manager.abilityNumToName[i];

            // check if this ability was pressed this frame
            if (INP_PlayerInput.getButtonDown("Ability" + i))
            {

                // activate callback for button down
                manager.abilities[thisAbility].buttonDown();
            }
            
            // check this ability number button was pressed
            if (INP_PlayerInput.getButtonUp("Ability" + i))
            {
                // reset held timer and held state
                heldTimer[thisAbility] = 0;
                beingHeld[thisAbility] = false;              

                // activate callback for button up
                manager.abilities[thisAbility].buttonUp();    
            }

            // logic for ability button being held
            if (INP_PlayerInput.getButton("Ability" + i))
            {
                manager.abilities[thisAbility].buttonHeldDown();
                /*// update how long button has been down
                heldTimer[thisAbility] += TME_Manager.getDeltaTime(TME_Time.type.game);

                // runs during period of time before ability is registered as being "held"
                if (!beingHeld[thisAbility])
                {

                    // check if we have reached the threshold and this button is now being "held"
                    if(heldTimer[thisAbility] >= manager.abilities[thisAbility].getHeldThreshold())
                    {
                        // repurpose the timer to keep track of time since ability was first registered as held
                        heldTimer[thisAbility] -= manager.abilities[thisAbility].getHeldThreshold();

                        // make note that this ability is being held
                        beingHeld[thisAbility] = true;

                        // activate callback for when ability is first held
                        manager.abilities[thisAbility].buttonHeld();
                    }
                }

                // runs when button is being registered as "held"
                if (beingHeld[thisAbility])
                {
                    // activate callback for button being "held"
                    manager.abilities[thisAbility].buttonHeldDown(heldTimer[thisAbility]);
                }

                //update ability wasHeld bool
                manager.abilities[thisAbility].wasHeld = beingHeld[thisAbility];*/
            }
        }
    }



    //
    // activates ability by calling correct ability function
    //
    void activateAbility(ABI_Manager.abilityType a)
    {
        manager.abilities[a].buttonDown();
        manager.abilities[a].active = true;
    }


    //
    //return held timer for this ability
    //
    public float getHeldTimer(ABI_Manager.abilityType a)
    {
        return heldTimer[a];
    }
}
