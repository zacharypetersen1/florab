﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_CommandGoTo : ABI_Ability {

    CHA_Motor cursor_motor;
    float offset = 5f;

    public override float getRechargeTime()
    {
        return 0.0f;
    }

    //
    // constructor
    //
    public ABI_CommandGoTo(PLY_PlayerDriver setDriver, GameObject twig, ABI_Manager setManager)
    {
        base.storeReferences(setDriver, twig, setManager);
    }



    //
    // activate GoTo command
    //
    public override void buttonDown()
    {
        //ADD IF STATEMENT SO ONLY WORKS WHEN ONLY THIS BUTTON IS DOWN
        cursor = UTL_Resources.cloneAtLocation("Abilities/Commands/Cursor", driver.transform.position + direction * offset);
        cursor_motor = cursor.GetComponent<CHA_Motor>();

        //DISABLE CHACACTER MOTOR
        motor.enabled = false;

        //set ally's state to follow cursor
        player.GetComponent<PLY_AllyManager>().setAllAlliesState(PLY_AllyManager.allyState.goTo);
    }



    //
    //creates cursor for ally minions to follow
    //
    public override void buttonUp()
    {
        if (cursor)
        {
            player.GetComponent<PLY_AllyManager>().setWaitPosition(UTL_Math.vec3ToVec2(cursor.transform.position));
            MonoBehaviour.Destroy(cursor);
            //cursor_motor = null;

            //ENABLE CHACACTER MOTOR
            motor.enabled = true;
        }
        player.GetComponent<PLY_AllyManager>().setAllAlliesState(PLY_AllyManager.allyState.wait);
    }



    //
    //updates position of cursor
    //
    public override void update()
    {
        base.update();
        try
        {
            if (cursor)
            {
                cursor_motor.move(CAM_Camera.applyDollyRotZ(INP_PlayerInput.get2DAxis("Cursor", false)));
                //MUST UPDATE THE PATHFINDING PATH 
                player.GetComponent<PLY_AllyManager>().setWaitPosition(UTL_Math.vec3ToVec2(cursor.transform.position));
            }
        }
        catch (Exception e)
        {
            throw (e);
        }
    }
}
