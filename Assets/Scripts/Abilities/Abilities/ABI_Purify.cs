﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


//
// Handles purification "weapon"
//
public class ABI_Purify : ABI_Ability {



    // The different types of purification "attacks"
    public enum purifyTypes {
        launchShot, quickShot, multiShot
    }

    public purifyTypes currentType = purifyTypes.quickShot;

    // Stores instances of all purify types
    Dictionary<purifyTypes, ABI_PurifyType> typeList = new Dictionary<purifyTypes, ABI_PurifyType>{
        {purifyTypes.launchShot, new ABI_LaunchShot() },
        {purifyTypes.quickShot, new ABI_QuickShot() },
        {purifyTypes.multiShot, new ABI_MultiShot() }
    };


    //
    // constructor
    //
    public ABI_Purify(PLY_PlayerDriver setDriver, GameObject twig, ABI_Manager setManager)
    {
        base.storeReferences(setDriver, twig, setManager);
    }



    public override void start()
    {
        foreach(var ability in typeList.Values)
        {
            ability.start();
        }
        UIX_ShotIndicator.setShotIndicator(currentType);
    }



    // DEBUG Listen for player to change selected purify type
    public override void update()
    {
        if (!SelectionManager.isInSelectMode && !TUT_TutorialManager.isMessagePlaying)
        {
            // This code updated purify shot type based on scroll wheel
            /*float scrollDelta = Input.GetAxis("Mouse ScrollWheel")*10;
            //Debug.Log(scrollDelta);
            if(scrollDelta != 0)
            {
                currentType = (purifyTypes) (((int)currentType+(int)scrollDelta+3)%3);
                //Debug.Log(currentType);
                // Trigger UI change here
                UIX_ShotIndicator.setShotIndicator(currentType);
            }*/

            typeList[currentType].update();
        }
    }



    //
    // Runs once when ability is first activated;
    //
    public override void buttonDown()
    {
        if(!SelectionManager.isInSelectMode && !TUT_TutorialManager.isMessagePlaying)
        {
            typeList[currentType].groundShotDown();
        }
    }



    //
    // Runs once when ability is first released
    //
    public override void buttonUp()
    {
        if (!SelectionManager.isInSelectMode && !TUT_TutorialManager.isMessagePlaying)
        {
            typeList[currentType].shotUp();
        }
    }



    //
    // Runs once per frame while ability is being held down
    //
    public override void buttonHeldDown()
    {
        if (!SelectionManager.isInSelectMode && !TUT_TutorialManager.isMessagePlaying)
        {
            typeList[currentType].shotHeld();
        }
    }
}
