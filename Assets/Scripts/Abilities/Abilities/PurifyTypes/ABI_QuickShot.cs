﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_QuickShot : ABI_ShotTypeStandard {

    public override void start()
    {
        initShot(75, .12f, 1.5f, true, PLY_Ammo.ammoTypes.red, 1);
    }

    protected override void doShot()
    {
        ABI_Nozzle.shootParticle(ABI_Nozzle.nozzle.quickShot, 1);
        FMODUnity.RuntimeManager.PlayOneShot("event:/twig_purify_single", player.transform.position);
    }
}
