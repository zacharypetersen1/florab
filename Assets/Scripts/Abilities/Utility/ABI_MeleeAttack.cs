﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_MeleeAttack : MonoBehaviour
{
    public TME_Time.type timeType = TME_Time.type.game;
    public string[] collidesWithTags;
    public string[] affectsTags;
    public float DC = 10;
    public float heal = 0;
    public bool purity; //should be equal to the purity of the creator of this projectile
    float lifeTime = .15f;
    public ENT_DungeonEntity.monsterTypes entityType;
    
    bool hit = false;

    private IEnumerator WaitCoroutine(float waitTime, ENT_DungeonEntity other_alarm, Vector3 pos)
    {
        yield return new WaitForSeconds(waitTime);

        if (!hit)
        {
            //Debug.Log("Projectile triggered alarm");
            other_alarm.receive_combat_message(new CBI_CombatMessenger(0, 0, 0, null, pos));
            hit = false;
        }
    }


    void OnTriggerEnter(Collider collider)
    {
        foreach (string tag in affectsTags)
        {
            if (collider.tag == tag)
            {
                Vector3 projectileDir = collider.transform.position - transform.position;
                if (collider.tag == "alarmSphere")
                {
                    ENT_DungeonEntity other_alarm = collider.gameObject.GetComponentInParent<ENT_DungeonEntity>();
                    if (purity != other_alarm.Purity)
                    {
                        //wait .5 seconds before sending message to minion
                        StartCoroutine(WaitCoroutine(.5F, other_alarm, transform.position));
                        
                    }
                    continue;
                }
                
                ENT_DungeonEntity other = collider.gameObject.GetComponent<ENT_DungeonEntity>();
                
                if (other != null && purity != other.Purity)
                {
                    Vector3 hitForward;
                    if (collider.tag == "Player")
                    {
                        hitForward = collider.transform.right;
                    }
                    else
                    {
                        hitForward = collider.transform.forward;
                    }                
                    float dot = Vector3.Dot(hitForward, projectileDir);
                    if (dot >= 0) //attack came from behind
                    {
                        //print(collider.gameObject.name + " got hit from behind!");
                        if(entityType == ENT_DungeonEntity.monsterTypes.treeMinion)
                        {
                            other.receive_combat_message(new CBI_CombatMessenger(DC * 2, heal, 0, null, transform.position));
                        }
                        /*
                        else if(entityType == ENT_DungeonEntity.monsterTypes.slowMinion)
                        {
                            other.receive_combat_message(new CBI_CombatMessenger(DC * 2, heal, 0, new List<CBI_CombatMessenger.status>() { CBI_CombatMessenger.status.meleeSlow }, transform.position));
                        }
                        */
                        if(collider.tag == "Minion")
                        {
                            //signals alarmsphere not to send combat message
                            hit = true;
                            other.GetComponent<Rigidbody>().AddForce(projectileDir.normalized * 15, ForceMode.Impulse);
                         
                        }
                        UTL_Resources.cloneAtLocation("Environment Effects/Hit", transform.position + new Vector3(0, 2f, 0), false);
                    }
                    else if (other != null)
                    {
                        if (entityType == ENT_DungeonEntity.monsterTypes.treeMinion)
                        {
                            other.receive_combat_message(new CBI_CombatMessenger(DC, heal, 0, null, transform.position));
                        }
                        /*
                        else if (entityType == ENT_DungeonEntity.monsterTypes.slowMinion)
                        {
                            other.receive_combat_message(new CBI_CombatMessenger(DC, heal, 0, new List<CBI_CombatMessenger.status>() { CBI_CombatMessenger.status.meleeSlow }, transform.position));
                        }*/
                        Rigidbody rb = other.GetComponent<Rigidbody>();
                        if (rb != null)
                        {
                            rb.AddForce(projectileDir.normalized * 5, ForceMode.Impulse);
                        }
                    }
                }
            }
        }
    }

    public void Update()
    {
        lifeTime -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
        if(lifeTime <= 0)
        {
            Destroy(gameObject);
        }
    }
}
