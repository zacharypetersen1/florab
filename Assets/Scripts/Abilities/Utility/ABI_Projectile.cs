﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_Projectile : MonoBehaviour
{
    public TME_Time.type timeType = TME_Time.type.game;
    public float Velocity = 0.5f;
    public float maxDistance = 20f;
    public string[] collidesWithTags;
    public string[] affectsTags;
    public float DC = 1;
    public float heal = 0;
    public bool purity; //should be equal to the purity of the creator of this projectile
    bool alive = true;
    float distanceTravelled = 0;
    Vector3 direction;
    Vector3 lastPosition;

    Quaternion newRotation;

    Quaternion startVertical = Quaternion.Euler(90,0,-90); 
    Quaternion startHorizontal = Quaternion.Euler(0,0,0);
    Quaternion startDiagonal = Quaternion.Euler(0,45,45);

    Quaternion rotateAngle = Quaternion.Euler(0, 100, 0);

    float smoothing;

    bool hit = false;

    public void Start()
    {
        smoothing = Random.Range(5, 10);
        lastPosition = transform.position;
        int num = Random.Range(0,2);
        if(num == 0)
        {
            transform.rotation = startVertical;
        }
        else if(num == 1)
        {
            transform.rotation = startHorizontal;
        }
        else
        {
            transform.rotation = startDiagonal;
        }

    }

    public void setDirection(Vector3 dir)
    {
        direction = dir;
    }


    private IEnumerator WaitCoroutine(float waitTime, ENT_DungeonEntity other_alarm, Vector3 pos)
    {
        yield return new WaitForSeconds(waitTime);

        if (!hit)
        {
            //Debug.Log("Projectile triggered alarm");
            other_alarm.receive_combat_message(new CBI_CombatMessenger(0, 0, 0, null, pos));
            hit = false;
        }
    }

    //
    // Runs on collision
    //
    void OnTriggerEnter(Collider collider)
    {

        // loop through the collidable tags
        foreach(var tag in collidesWithTags)
        {
            // check if need to destroy particle
            if (collider.tag == tag)
            {
                alive = false;
            }
        }

        foreach(string tag in affectsTags)
        {
            if (collider.tag == tag)
            {
                Vector3 projectileDir = collider.transform.position - transform.position;
                if (collider.tag == "alarmSphere")
                {
                    ENT_DungeonEntity other_alarm = collider.gameObject.GetComponentInParent<ENT_DungeonEntity>();
                    if (purity != other_alarm.Purity)
                    {
                        //wait .5 seconds before sending message to minion
                        StartCoroutine(WaitCoroutine(.5F, other_alarm, transform.position));
                    }
                    continue;
                }

                ENT_DungeonEntity other = collider.gameObject.GetComponent<ENT_DungeonEntity>();
                if (other != null)
                {
                    if (purity != other.Purity)
                    {
                        Vector3 hitForward;
                        if (collider.tag == "Player")
                        {
                            hitForward = collider.transform.right;
                        }
                        else
                        {
                            hitForward = collider.transform.forward;
                        }
                        float dot = Vector3.Dot(hitForward, projectileDir);
                        if (dot >= 0) //attack came from behind
                        {
                            //print(collider.gameObject.name + " got hit from behind!");
                            if (other.tag == "Minion")
                            {
                                other.GetComponent<Rigidbody>().AddForce(projectileDir.normalized * 15, ForceMode.Impulse);
                            }
                            UTL_Resources.cloneAtLocation("Environment Effects/Hit", transform.position + new Vector3(0, 2f, 0), false);
                            other.receive_combat_message(new CBI_CombatMessenger(DC * 2, heal, 0, null, transform.position));
                        }
                        else
                        {
                            Rigidbody rb = other.GetComponent<Rigidbody>();
                            if (rb != null)
                            {
                                rb.AddForce(projectileDir.normalized * 5, ForceMode.Impulse);
                            }
                            other.receive_combat_message(new CBI_CombatMessenger(DC, heal, 0, null, transform.position));
                        }
                        alive = false;
                    }
                }
            }
        }
    }



    //
    // check if max distance reached
    //
    void checkDistance()
    {
       
        distanceTravelled += Vector3.Distance(transform.position, lastPosition);
        if (distanceTravelled >= maxDistance)
        {
            alive = false;
        }
        lastPosition = transform.position;
    }



    //
    // update position
    //
    void updatePosition()
    {
        transform.position += direction * TME_Manager.getDeltaTime(timeType) * Velocity;
    }

    void rotate()
    {
        newRotation = transform.rotation * rotateAngle;
        transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, TME_Manager.getDeltaTime(TME_Time.type.enemies) * smoothing);

    }



    public void FixedUpdate()
    {
        updatePosition();
        rotate();
        checkDistance();

        if (!alive)
        {
            if (gameObject.name.Contains("Basic"))
            {
                destroyLeaveParticles();
            }else
            {
                Destroy(gameObject);
            }
           
        }
       
    }

    public void destroyLeaveParticles()
    {
        Transform effect = transform.GetChild(0).GetChild(0);
        effect.parent = null;
        Destroy(gameObject);
        Destroy(effect.gameObject, .5F);
    }
}
