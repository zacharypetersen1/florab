﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_RemnantDetector : MonoBehaviour {

    private List<GameObject> remnantList = new List<GameObject>();
    public List<GameObject> Remnants
    {
        get { return remnantList; }
    }

	public void OnTriggerEnter(Collider col)
    {
        if (col.tag == "remnant")
        {
            col.gameObject.GetComponent<ENT_Remnant>().addToList(remnantList);
        }
    }
    public void OnTriggerExit(Collider col)
    {
        if (col.tag == "remnant")
        {
            col.gameObject.GetComponent<ENT_Remnant>().removeFromList(remnantList);
        }
    }

    public GameObject getNearestRemnant()
    {
        if (remnantList.Count == 0) return null;
        float dist = 100000;
        GameObject nearest = null;
        foreach (GameObject remnant in remnantList)
        {
            if (remnant == null) continue;
            float newDist = Vector3.Distance(remnant.transform.position, transform.position);
            if (newDist < dist)
            {
                dist = newDist;
                nearest = remnant;
            }
        }
        return nearest;
    }
}
