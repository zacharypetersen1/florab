﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_TurretLOSAbility : MonoBehaviour
{
    ENT_TurretMinion body;
    // Use this for initialization
    void Start()
    {
        body = GetComponentInParent<ENT_TurretMinion>();
    }

    // Update is called once per frame
    void Update()
    {

        if (body == null || !body.IsAbilityActive)
        {
            Destroy(gameObject);
        }
    }
}

