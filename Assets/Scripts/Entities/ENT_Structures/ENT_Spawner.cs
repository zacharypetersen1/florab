﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_Spawner : ENT_Structure {

    //public MeshRenderer rend;
    public Material corrupted;
    public Material purified;
   

    /// <summary>
    /// Could also be float. Used to determine how close the player must get before the spawner will spawn anything
    /// </summary>
    public int sightRange = 6;

    /// <summary>
    /// Determines the maximum number of entities that the spawner can spawn throughout its lifetime.
    /// Could have seperate limit for each type if spawner can spawn multiple monster types.
    /// </summary>
    public int spawnLimit = 7;
    /// <summary>
    /// How many have spawned
    /// </summary>
    public int spawned;

    /// <summary>
    /// A variable to be used in determining how fast to spawn monsters.
    /// </summary>
    public float spawnRate;

    /// <summary>
    /// The minimum distance away from its parent spawner a monster can spawn
    /// </summary>
    public float min_spawn_distance;

    /// <summary>
    /// The furthest away from its parent spawner a monster can spawn
    /// </summary>
    public float max_spawn_distance;

    /// <summary>
    /// Timer counts down until the next spawn, based on spawnRate.
    /// </summary>
    public float timer;

    public bool onlySpawnNearPlayer = true;

    //an array of minions that can be spawned by this structure.
    //Place a minion in this list multiple times for it to be spawned with a higher chance.
    public string[] minionsToSpawn;

    public GameObject[] crytals;
    public GameObject[] crytalCleanseEffects; 

    // Use this for initialization
    public override void Start () {
        //gameObject.GetComponent<MeshRenderer>().material = corrupted;
        base.Start();
        priority = 2; //Low priority is higher priority
        State = "active";
        Purity = false;
        //sightRange = 6;
        //spawnLimit = 7;
        spawned = 0;
        timer = spawnRate; //Start the timer. This line will be moved to the activate function eventually
        //twig = GameObject.Find("Player");
	}

    public float distanceToPlayer()
    {
        Vector2 spawnerLocation = new Vector2(transform.position.x, transform.position.z);
        Vector2 twigPosition = new Vector2(twig.GetComponent<Transform>().position.x, twig.GetComponent<Transform>().position.z);
        return Vector2.Distance(spawnerLocation, twigPosition);
    }

    // Update is called once per frame
    public override void Update () {
        base.Update();
        //Update the timer with the time since the last frame. Uses game time
        timer -= TME_Manager.getDeltaTime(TME_Time.type.game);
        //When timer reaches 0, check if the spawn limit has been reached. If not, spawn a dude
        if (minionsToSpawn.Length > 0 && (!onlySpawnNearPlayer || distanceToPlayer() <= sightRange))
        {
            if (State == "active" && timer <= 0 && spawned < spawnLimit)
            {
                float distance_offset = Random.Range(min_spawn_distance, max_spawn_distance); //Random Polar coords to spawn
                float radial_offset = Random.Range(0f, 360f);
                float x_offset = Mathf.Cos(radial_offset) * distance_offset;
                float z_offset = Mathf.Sin(radial_offset) * distance_offset;
                Vector3 spawn_offset = new Vector3(transform.position.x + x_offset, transform.position.y, transform.position.z + z_offset);
                //Debug.Log("Spawn location: " + spawn_offset);
                int toSpawn = Random.Range(0, minionsToSpawn.Length);
                //Code for generating the monster object in game
                GameObject newEnemy = UTL_Resources.cloneAtLocation(minionsToSpawn[toSpawn], spawn_offset);
                newEnemy.GetComponent<ENT_Body>().spawnedBy = gameObject;
                spawned++; // Keep track of how many have spawned
                timer = spawnRate; // Reset the timer
                //print("spawned " + newEnemy.name);
            }
        }

    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (State == "cleansed")
        {
            
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 0.08f, gameObject.transform.position.z);
            timer += 0.08f;
            if (timer >= 20)
            {
                Destroy(gameObject);
            }
        }
    }

    public override void cleanse_self()
    {
        base.cleanse_self();
        //Debug.Log("Spawner cleansed");
        State = "cleansed";
        //rend.material = purified;
        var children = transform.GetComponentsInChildren<Transform>();
        foreach (var child in children)
        {
            if (child.name.Contains("Crystal"))
            {
                child.GetComponent<MeshRenderer>().material = ghostMat;
            }
        }
        foreach(var crystal in crytals)
        {
            crystal.GetComponent<MeshRenderer>().enabled = false;
        }
        foreach(var ParticleObj in crytalCleanseEffects)
        {
            ParticleSystem[] particleSystems = ParticleObj.GetComponentsInChildren<ParticleSystem>();
            foreach(var ps in particleSystems)
            {
                ps.Play();
            }
        }
    }

}
