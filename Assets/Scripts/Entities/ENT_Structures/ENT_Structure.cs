﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_Structure : ENT_DungeonEntity {

    /// <summary>
    /// Reference to zone that contains the structure
    /// </summary>
    public Zone ParentZone;
    public int priority; // How important it is for the player to target the structure
    public string JSON_path; // Where to find JSON data for the structure object
    public GameObject twig; // Reference to player object
    public int ID; //ID for each structure so that minions can know where they spawned from
                   //public UIX_CameraFacingBillboard_Minion healthBar;

    protected ParticleSystem[] particleEffects;
    protected GameObject childBackgroundBar;
    public Collider attackableArea = null;

    Collider myCollider;
    // Use this for initialization
    public override void Start () {
        base.Start();
        //New health bars
        childBackgroundBar = UTL_Resources.cloneAtLocation("FloatingUI/BackgroundBar", transform.position);
        childBackgroundBar.transform.parent = transform;
        Collider col = GetComponent<Collider>();
        if (col != null)
        {
            childBackgroundBar.GetComponent<FUI_PurityBar>().offset = col.bounds.size.y + .5f;
        }
        childBackgroundBar.transform.localScale *= 2.0f;
        allyManager.addStructure(gameObject);
        CL = CL_MAX;
        twig = player;
        particleEffects = GetComponentsInChildren<ParticleSystem>();
        SetPSEnabled(false);
        if (attackableArea == null)
        {
            Transform temp = transform.Find("AttackableArea");
            if (temp != null)
            {
                attackableArea = temp.GetComponent<Collider>();
            }
            else
            {
                Debug.LogWarning(gameObject.name + " is missing it's AttackableArea child, please attach one for melee attacks to work properly");
            }
        }
    }

    void subStart()
    {
        // Implementation in derived classes
    }
	
	// Update is called once per frame
	public override void Update () {
        base.Update();
        if (State == "active" && CL <= 0)
        {
            cleanse_self();
        }
        subUpdate();
	}

    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    void subUpdate()
    {
        // Implementation in derived classes
    }


    public void SpawnEnemy()
    {
        // Implementation in derived classes
    }

    //May not be necessary in this intermediate level class; implemented below this class in the hierarchy, interfaced above it.
    public override void activate()
    {

    }

    public override void on_delta_clarity()
    {
        //Old code
        //if (healthBar != null) {
        //    healthBar.updateHealthBar(CL_MAX, CL);
        //}
    }

    public override void inflict_damage(float DC)
    {
        base.inflict_damage(DC);
    }

    public override void cleanse_self()
    {
        base.cleanse_self();
        var children = transform.GetComponentsInChildren<Transform>();
        foreach (var child in children)
        {
            //Remove crystals
            if (child.name.StartsWith("Crystal"))
            {
                UnityEngine.Object.Destroy(child.gameObject);
            }
        }
        GameObject healthBar = gameObject.transform.Find("BackgroundBar(Clone)").gameObject;
        if(healthBar != null) Destroy(gameObject.transform.Find("BackgroundBar(Clone)").gameObject);
        allyManager.removeStructure(gameObject);
    }
    
    public virtual void SetPSEnabled(bool enable)
    {
        foreach (ParticleSystem ps in particleEffects)
        {
            if (enable)
            {
                ps.Play();
                //Debug.Log("Start playing paticle effects on building" + gameObject.name);
            }
            else
            {
                ps.Stop();
                //Debug.Log("Stopped playing paticle effects on building" + gameObject.name);
            }
        }
    }

    public override Vector3 getNearestAttackablePoint(Vector3 originalPos)
    {
        if (attackableArea != null)
        {
            //print("using attackable area");
            return attackableArea.ClosestPointOnBounds(originalPos);
        }
        else return transform.position;
    }

    

    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    try
    //    {
    //        CBI_CombatMessenger message = collision.gameObject.GetComponent<CBI_CombatMessenger>();
    //        receive_combat_message(message);
    //    }
    //    catch
    //    {
    //        Debug.Log("no combat message to recieve");
    //    }
    //}
}
