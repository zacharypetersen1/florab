﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_Spawner_advanced : ENT_Structure
{
    //OBSOLETE, DO NOT USE
    public MeshRenderer meshRenderer;
    public Material corrupted;
    public Material purified;
    int waves_spawned;
    //public List<ENT_spawn_wave> wave_points;

    /// <summary>
    /// Could also be float. Used to determine how close the player must get before the spawner will spawn anything
    /// </summary>
    public int sightRange;

    /// <summary>
    /// Determines the maximum number of entities that the spawner can spawn throughout its lifetime.
    /// Could have seperate limit for each type if spawner can spawn multiple monster types.
    /// </summary>
    public int spawnLimit;
    /// <summary>
    /// How many have spawned
    /// </summary>
    public int spawned = 0;

    /// <summary>
    /// A variable to be used in determining how fast to spawn monsters.
    /// </summary>
    public float spawnRate;

    /// <summary>
    /// The minimum distance away from its parent spawner a monster can spawn
    /// </summary>
    public float min_spawn_distance;

    /// <summary>
    /// The furthest away from its parent spawner a monster can spawn
    /// </summary>
    public float max_spawn_distance;

    /// <summary>
    /// Timer counts down until the next spawn, based on spawnRate.
    /// </summary>
    public float timer;

    // Use this for initialization
    public override void Start()
    {
        //gameObject.GetComponent<MeshRenderer>().material = corrupted;
        base.Start();
        priority = 2; //Low priority is higher priority
        State = "active";
        sightRange = 6;
        spawnLimit = 7;
        waves_spawned = 0;
        timer = spawnRate; //Start the timer. This line will be moved to the activate function eventually
                           //twig = GameObject.Find("Player");
    }

    public float distanceToPlayer()
    {
        Vector2 spawnerLocation = new Vector2(transform.position.x, transform.position.y);
        Vector2 twigPosition = new Vector2(twig.GetComponent<Transform>().position.x, twig.GetComponent<Transform>().position.y);
        return Vector2.Distance(spawnerLocation, twigPosition);
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        //Update the timer with the time since the last frame. Uses game time
        timer -= TME_Manager.getDeltaTime(TME_Time.type.game);
        //When timer reaches 0, check if the spawn limit has been reached. If not, spawn a dude
        if (State == "active" && timer <= 0 && spawned < spawnLimit && distanceToPlayer() <= sightRange)
        {
            float distance_offset = Random.Range(min_spawn_distance, max_spawn_distance); //Random Polar coords to spawn
            float radial_offset = Random.Range(0f, 360f);
            float x_offset = Mathf.Cos(radial_offset) * distance_offset;
            float y_offset = Mathf.Sin(radial_offset) * distance_offset;
            Vector2 spawn_offset = new Vector2(transform.position.x + x_offset, transform.position.y + y_offset);
            //Code for generating the monster object in game
            UTL_Resources.cloneAtLocation("Minions/BasicEnemy", spawn_offset);
            spawned++; // Keep track of how many have spawned
            timer = spawnRate; // Reset the timer
        }
        //Spawn waves at equally distributed points along the spawner's health
        if (State == "active" && waves_spawned < wave_points.Count && (((float)CL / (float)CL_MAX) < ((((float)wave_points.Count)-(float)waves_spawned)/((float)wave_points.Count+1))))
        {
            Debug.Log("CL: " +CL +" CL_MAXC: "+CL_MAX+" waves left: "+(wave_points.Count - waves_spawned)+" wave_points.count: "+(wave_points.Count));
            wave_points[waves_spawned].spawn(1);
            waves_spawned++;
            Debug.Log("Spawning wave from wavepoint");
        }
    }


    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (State == "cleansed")
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 0.02f, gameObject.transform.position.z);
            timer += 0.02f;
            if (timer > 6)
            {
                Destroy(gameObject);
            }
        }
    }


    public override void cleanse_self()
    {
        //Debug.Log("Spawner cleansed");
        State = "cleansed";
        //meshRenderer.material = purified;
    }

}
