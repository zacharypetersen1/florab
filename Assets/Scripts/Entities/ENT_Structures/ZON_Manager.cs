﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using System;

public class ZON_Manager : MonoBehaviour {
    public static ZON_Manager zone_manager;
    /// <summary>
    /// List that keeps track of all zones in a dungeon.
    /// </summary>
    public List<Zone> zones = new List<Zone>();

    /// <summary>
    /// Keeps track of he ID of the most recently created zone.
    /// </summary>
    static int recent_ID = 1;

	// Use this for initialization
	void Start () {
        zone_manager = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    /// <summary>
    /// Constructs a zone object, assigns it a unique ID, and adds it to the "zones" list.
    /// </summary>
    public void CreateZone()
    {
        Zone new_zone = new Zone(); // Construct new Zone object
        new_zone.ID = recent_ID++; // Assign the ID
        zones.Add(new_zone); // Add it to the Zone Managers zone list
    }

    /// <summary>
    /// Takes an ID for the zone to activate, and calls ActivateChildren() for that zone.
    /// </summary>
    public void ActivateZone(int zoneID)
    {
        foreach( Zone z in zones ){
            if (z.ID == zoneID)
            {
                z.ActivateChildren();
            }
            break;
        }
    }
}

/// <summary>
/// Zones are defined by the structures they contain.
/// Only entities inside the zone should belong to it, but this is up to the level designer to enforce.
/// </summary>
public class Zone : MonoBehaviour
{
    /// <summary>
    /// The zone ID for each zone
    /// </summary>
    public int ID;

    /// <summary>
    /// List of structres and monsters that belong to the zone
    /// </summary>
    List<ENT_DungeonEntity> children = new List<ENT_DungeonEntity>();

    /// <summary>
    /// Iterates through all children, monsters and structures alike, and calls their activate function
    /// </summary>
    public void ActivateChildren()
    {
        foreach(ENT_DungeonEntity child in children){
            child.activate();
        }
    } 
}