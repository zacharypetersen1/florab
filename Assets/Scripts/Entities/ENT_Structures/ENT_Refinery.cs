﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_Refinery : ENT_Structure {

    private int crystalCount = 0;

    public int CrystalCount
    {
        get { return crystalCount; }
    }

    public void depositCrystal()
    {
        crystalCount += 1;
        print("crystalCount =" + crystalCount);
    }
}
