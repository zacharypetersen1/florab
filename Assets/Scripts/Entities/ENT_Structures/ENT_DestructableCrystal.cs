﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_DestructableCrystal : ENT_Structure
{
    public ParticleSystem lightning;
    public GameObject cleanse_effect;
    ParticleSystem[] cleanse_PS;

    public override void Start()
    {
        base.Start();
        Purity = false;
        childBackgroundBar.GetComponent<FUI_PurityBar>().offset = GetComponent<Collider>().bounds.size.y + 2f;
        cleanse_PS = cleanse_effect.GetComponentsInChildren<ParticleSystem>();
        particleEffects = new ParticleSystem[] { lightning };
        State = "active";
    }

    public override void cleanse_self()
    {
        base.cleanse_self();
        State = "cleansed";
        foreach (var ps in cleanse_PS)
        {
            ps.Play();
        }
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        gameObject.GetComponent<Collider>().enabled = false;
        Destroy(gameObject, 1);
    }
}
