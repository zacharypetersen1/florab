﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Harold
//this script is used to make the spawners use the new recipe system

public class ENT_RecipeSpawner : ENT_Structure
{

    public MeshRenderer meshRenderer;
    public Material corrupted;
    public Material purified;


    //Used to determine how close the player must get before the spawner will spawn anything
    public float sightRange = 30;

    // how frequently monsters spawn (seconds)
    public float spawnRate;

    // The minimum distance away from its parent spawner a monster can spawn
    public float min_spawn_distance;

    // The furthest away from its parent spawner a monster can spawn
    public float max_spawn_distance;

    // Timer counts down until the next spawn, based on spawnRate.
    private float timer;

    //whether or not the spawner only functions near the player
    public bool onlySpawnNearPlayer = false;

    //an array of minions that can be spawned by this structure.
    //Place a minion in this list multiple times for it to be spawned with a higher chance.
    //public string[] minionsToSpawn;

    //the Resources path to minion to be spawned on the next spawnCycle
    private string minionToSpawn = "";

    //the list of roles to make for the current recipe
    private List<AIG_Recipes.Roles> currentRecipeRoles;

    private AIG_Recipes.ChainGangRecipes currentCGRecipe = AIG_Recipes.ChainGangRecipes.Undef;

    //the list of CGRecipes to make as a queue
    private List<AIG_Recipes.ChainGangRecipes> CGRecipesToMake = new List<AIG_Recipes.ChainGangRecipes>(); //should be Priority Queue
    //the list of WBRecipes to make as a queue
    public List<AIG_Recipes.WarbandRecipes> WBRecipesToMake; //should be Priority Queue
    //the list of Chain Gang recipes already created, when recipe is complete move it to this list
    private List<AIG_Recipes.ChainGangRecipes> CGRecipesCompleted = new List<AIG_Recipes.ChainGangRecipes>();

    // Use this for initialization
    public override void Start()
    {
        //gameObject.GetComponent<MeshRenderer>().material = corrupted;
        base.Start();
        priority = 2; //Low priority is higher priority
        State = "active";
        timer = spawnRate; //Start the timer. This line will be moved to the activate function eventually
                           //twig = GameObject.Find("Player");
    }

    public float distanceToPlayer()
    {
        Vector2 spawnerLocation = new Vector2(transform.position.x, transform.position.y);
        Vector2 twigPosition = new Vector2(twig.GetComponent<Transform>().position.x, twig.GetComponent<Transform>().position.y);
        return Vector2.Distance(spawnerLocation, twigPosition);
    }

    public override void Update()
    {
        base.Update();
        timer -= TME_Manager.getDeltaTime(TME_Time.type.game);
        setMinionToSpawn();
        if (shouldSpawnMinion()) spawnMinion();
        
    }

    public override void cleanse_self()
    {
        Debug.Log("Spawner cleansed");
        State = "cleansed";
        meshRenderer.material = purified;
    }

    private void setMinionToSpawn()
    {
        if (minionToSpawn != "") return;
        AIG_Recipes.Roles newRole;
        if (currentRecipeRoles.Count > 0)
        {
            newRole = currentRecipeRoles[0];
            currentRecipeRoles.RemoveAt(0);
            minionToSpawn = AIG_Recipes.getFilePathFromRole(newRole);
        } 
        if (CGRecipesToMake.Count>0)
        {
            if (currentCGRecipe != AIG_Recipes.ChainGangRecipes.Undef) CGRecipesCompleted.Add(currentCGRecipe);
            currentCGRecipe = CGRecipesToMake[0];
            currentRecipeRoles = AIG_Recipes.getRolesFromCGRecipe(CGRecipesToMake[0]);
            CGRecipesToMake.RemoveAt(0);
            newRole = currentRecipeRoles[0];
            currentRecipeRoles.RemoveAt(0);
            minionToSpawn = AIG_Recipes.getFilePathFromRole(newRole);
        }
        else if (WBRecipesToMake.Count>0)
        {
            if (currentCGRecipe != AIG_Recipes.ChainGangRecipes.Undef) CGRecipesCompleted.Add(currentCGRecipe);
            currentCGRecipe = AIG_Recipes.ChainGangRecipes.Undef;
            currentRecipeRoles = AIG_Recipes.getRolesFromWBRecipe(WBRecipesToMake[0]);
            WBRecipesToMake.RemoveAt(0);
            newRole = currentRecipeRoles[0];
            currentRecipeRoles.RemoveAt(0);
            minionToSpawn = AIG_Recipes.getFilePathFromRole(newRole);
        }
        
    }

    private void spawnMinion()
    {
        float distance_offset = Random.Range(min_spawn_distance, max_spawn_distance); //Random Polar coords to spawn
        float radial_offset = Random.Range(0f, 360f);
        float x_offset = Mathf.Cos(radial_offset) * distance_offset;
        float y_offset = Mathf.Sin(radial_offset) * distance_offset;
        Vector2 spawn_offset = new Vector2(transform.position.x + x_offset, transform.position.y + y_offset);
        //int toSpawn = Random.Range(0, minionsToSpawn.Length);
        //Code for generating the monster object in game
        GameObject newEnemy = UTL_Resources.cloneAtLocation(minionToSpawn, spawn_offset);
        minionToSpawn = "";
        //set group to current group that is being produced
        newEnemy.GetComponent<ENT_Body>().spawnedBy = gameObject;
        timer = spawnRate; // Reset the timer
    }

    //fix this
    private bool shouldSpawnMinion()
    {
        //timer -= TME_Manager.getDeltaTime(TME_Time.type.game);
        //When timer reaches 0, check if the spawn limit has been reached. If not, spawn a dude
        return ((!onlySpawnNearPlayer || distanceToPlayer() <= sightRange) && State == "active" && timer <= 0 && minionToSpawn != "");
    }
}

