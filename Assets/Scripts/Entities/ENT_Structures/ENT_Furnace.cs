﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_Furnace : ENT_Structure {

    List<GameObject> fireWalls;
    List<GameObject> faceCrystals;
    List<GameObject> backCrystals;
    //Vector3 startPos;
    float timer = 0;



    //int ZoneIDtoActivate; // Could instead be direct reference to actual Zone object

    // Use this for initialization
    public override void Start () {
        base.Start();
        priority = 70; //Low priority is higher priority
        CL = CL_MAX;
        State = "active";
        //startPos = gameObject.transform.position;
        fireWalls = new List<GameObject>();
        faceCrystals = new List<GameObject>();
        backCrystals = new List<GameObject>();
        var children = gameObject.GetComponentsInChildren<Transform>();
        foreach (var child in children)
        {
            //Remove crystals
            if (child.tag == "fireWall")
            {
                fireWalls.Add(child.gameObject);
            }
            else if (child.name.StartsWith("FaceCrystal"))
            {
                faceCrystals.Add(child.gameObject);
            }
            else if (child.name.StartsWith("BackCrystal"))
            {
                backCrystals.Add(child.gameObject);
            }
        }

        
        /*for (int i = 0; i < gameObject.transform.childCount; ++i)
        {
            Transform child = gameObject.transform.GetChild(i);
            if (child.tag == "fireWall")
            {
                fireWalls.Add(child.gameObject);
            }
        }*/
    }

    // Update is called once per frame
    public override void Update () {
        base.Update();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (State == "cleansed" && fireWalls != null && fireWalls.Count > 0)
        {
            
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 0.04f, gameObject.transform.position.z);
            timer += 0.04f;
            if (timer >= 20)
            {
                Destroy(gameObject);
            }
            int num_walls = fireWalls.Count;
            for (int i = 0; i < num_walls; i++)
            {
                GameObject temp = fireWalls[i];
                temp.transform.position = new Vector3(temp.transform.position.x, temp.transform.position.y - 0.04f, temp.transform.position.z);
                if (timer >= 20)
                {
                    //fireWalls.Remove(temp);
                    Destroy(temp);
                }
                else
                {
                    temp.GetComponent<Collider>().enabled = false;
                    //wall.GetComponent<ParticleSystem>().Stop();
                }
            }
            if(timer >= 20)
            {
                fireWalls = null;
            }
        }
    }

    public override void cleanse_self()
    {
        //Debug.Log("Furnace cleansed");
        base.cleanse_self();
        State = "cleansed";
        foreach (GameObject wall in fireWalls)
        {
            var children = wall.GetComponentsInChildren<Transform>();
            foreach (var child in children)
            {
                if (child.name.StartsWith("WallCrystal"))
                {
                    child.GetComponent<MeshRenderer>().material = ghostMat;
                }
            }
        }
        foreach (GameObject crystal in backCrystals)
        {
            crystal.GetComponent<MeshRenderer>().material = ghostMat;
        }
        foreach (GameObject crystal in faceCrystals)
        {
            foreach (Renderer child in crystal.transform.Find("Flames").GetComponentsInChildren<Renderer>())
                {
                    child.enabled = false;
                }
            crystal.GetComponent<MeshRenderer>().material = ghostMat;
        }
        //rend.material = ghostMat;
        //wallCollider.enabled = false;
        //Must set zonmanager to avoid error
        //ZON_Manager.zone_manager.zones[ZoneIDtoActivate].ActivateChildren();
    }

    
}
