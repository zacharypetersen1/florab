﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ENT_PlayerBody : ENT_DungeonEntity
{

    /*
    [FMODUnity.EventRef]
    public string TwigHit;
    */

    //
    //
    //
    public bool invulnerable = false;

    public override void activate()
    {
        throw new NotImplementedException();
    }


    public override void Awake()
    {
        base.Awake();
        AIR_Respawn.SpawnPoint = transform.position;
    }

    //
    // Use this for initialization
    //
    public override void Start ()
    {
        CL = CL_MAX;
        Purity = true;
        //healthBar = gameObject.GetComponentInChildren<UIX_CameraFacingBillboard_Minion>();
    }


    public override void receive_combat_message(CBI_CombatMessenger combat_message)
    {
        if (combat_message.position == transform.position)
        {
            // Set direction to none
            UIX_DamageIndicator.setFlashDirection();
        }
        else
        {
            Vector3 vecOfImpact = combat_message.position - transform.position;
            Vector3 VecCam = Camera.main.transform.forward;
            VecCam.y = 0;
            vecOfImpact.y = 0;
            vecOfImpact = vecOfImpact.normalized;
            VecCam = VecCam.normalized;
            float angle = Mathf.Acos(Vector3.Dot(vecOfImpact, VecCam));
            Vector3 cross = Vector3.Cross(vecOfImpact, VecCam);
            if (cross.y > 0)
            {
                angle = angle * -1;
            }

            UIX_DamageIndicator.setFlashDirection(angle);
        }
        
        base.receive_combat_message(combat_message);
    }
    //
    // Runs when player takes damage
    //
    public override void inflict_damage(float DC)
    {
        if (invulnerable) return;
        CL -= DC;
        UIX_DamageIndicator.flash(DC);
        if(CL <= 0)
        {
            UIX_DeathFade.triggerFade = true;
            CL = CL_MAX;
            invulnerable = true;
        }
    }

    public override void inflict_healing(float DC)
    {
        CL += DC;
    }



    //
    // Runs any time the players CL changes
    //
    public override void on_delta_clarity()
    {
        GameObject.Find("HealthBarFill").GetComponent<Image>().fillAmount = CL / CL_MAX;
    }
}
