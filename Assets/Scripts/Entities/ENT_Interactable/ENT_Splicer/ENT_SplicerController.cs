﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_SplicerController : ENT_Interact
{
    public GameObject splicerObjective;

    public ENT_DungeonEntity.monsterTypes ingredient0;
    public ENT_DungeonEntity.monsterTypes ingredient1;
    public ENT_DungeonEntity.monsterTypes result;

    private SelectionManager selectionManager;

    

    private PLY_AllyManager allyManager;

    private List<splicerTicket> ticketList;

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        allyManager = player.GetComponent<PLY_AllyManager>();
        ticketList = transform.parent.GetComponentInChildren<ENT_SplicerZone>().tickets;
        selectionManager = GameObject.Find("SelectionManager").GetComponent<SelectionManager>();
    }

    public class splicerTicket
    {
        public GameObject minion0 = null;
        public GameObject minion1 = null;

        public bool minion0InPos = false;
        public bool minion1InPos = false;

        public bool readyToSplice = false;
    }

    public override void activate()
    {
        base.activate();
        splicerTicket newTicket = new splicerTicket();
        allyManager.getAllAllies.Sort(compareByDist);
        //allyManager.followGroup.AllyList.Sort(compareByDist);
        //ALSO LOOK THROUGH GROUP THAT IS CURRENTLY SELECTED
        foreach (GameObject ally in allyManager.getAllAllies/*allyManager.followGroup.AllyList*/)
        {
            if (ally.GetComponent<ENT_Body>().entity_type == ingredient0 && newTicket.minion0 == null)
            {
                newTicket.minion0 = ally;
                if (newTicket.minion1 != null) break;
                else continue;
            }
            if (ally.GetComponent<ENT_Body>().entity_type == ingredient1 && newTicket.minion1 == null)
            {
                newTicket.minion1 = ally;
                if (newTicket.minion0 != null) break;
                else continue;
            }
        }
        /*
        if(newTicket.minion0 == null || newTicket.minion1 == null)
        {
            foreach (GameObject ally in selectionManager.GetSelectedObjectsAsList())
            {
                if (ally.GetComponent<ENT_Body>().entity_type == ingredient0 && newTicket.minion0 == null)
                {
                    newTicket.minion0 = ally;
                    if (newTicket.minion1 != null) break;
                    else continue;
                }
                if (ally.GetComponent<ENT_Body>().entity_type == ingredient1 && newTicket.minion1 == null)
                {
                    newTicket.minion1 = ally;
                    if (newTicket.minion0 != null) break;
                    else continue;
                }
            }
        }
        */
        if(newTicket.minion0 != null && newTicket.minion1 != null)
        {
            newTicket.minion0.AddComponent<ENT_Ingredient>().activate(splicerObjective);
            newTicket.minion1.AddComponent<ENT_Ingredient>().activate(splicerObjective);
            newTicket.minion0.GetComponent<ENT_Body>().teleport(splicerObjective.transform.position);
            newTicket.minion1.GetComponent<ENT_Body>().teleport(splicerObjective.transform.position);
            //newTicket.minion0.GetComponent<ENT_Body>().allyManager.removeAlly(newTicket.minion0);
            //newTicket.minion1.GetComponent<ENT_Body>().allyManager.removeAlly(newTicket.minion1);
            selectionManager.disableObject(newTicket.minion0);
            selectionManager.disableObject(newTicket.minion1);
            newTicket.readyToSplice = true;
            ticketList.Add(newTicket);
        }

    }

    public int compareByDist(GameObject a, GameObject b)
    {
        if (a == null || b == null) return 0;
        float distA = Vector3.Distance(a.transform.position, transform.position);
        float distB = Vector3.Distance(b.transform.position, transform.position);
        if (distA < distB) return -1;
        if (distA > distB) return 1;
        return 0;
    }

    public void abortSplice(GameObject minion)
    {
        selectionManager.enableObject(minion);
    }
}
