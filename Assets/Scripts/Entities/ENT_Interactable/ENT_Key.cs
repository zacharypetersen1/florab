﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_Key : ENT_Interact
{


    private PLY_InteractableManager interactableManager;
    private FUI_HasItem theKey;
    private FUI_Default interactButton;

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        interactableManager = player.GetComponent<PLY_InteractableManager>();
        theKey = gameObject.GetComponent<FUI_HasItem>();
        interactButton = gameObject.GetComponent<FUI_Default>();
        destroy_after_use = true;
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
    }

    public override void activate()
    {
        base.activate();
        interactableManager.Keys += 1;
        theKey.hide();
        interactButton.hide();
        UTL_Resources.cloneAtLocation("Environment Effects/Hit", transform.position);
    }
}
