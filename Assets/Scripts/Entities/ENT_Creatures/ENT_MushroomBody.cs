﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_MushroomBody : ENT_Body
{
    private bool _mushroomAttacking = false;
    public bool mushroomAttacking
    {
        set { _mushroomAttacking = value; }
        get { return _mushroomAttacking; }
    }

    public override void Awake()
    {
        base.Awake();
    }

    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
        base.Update();
    }

    public override void lookTowards(Vector3 lookAt)
    {
        if (mushroomAttacking) return;
        if (Vector3.Angle(transform.forward, lookAt - transform.position) <= 1f)
        {
            shouldRotate = false;
            return;
        }

        Vector3 direction = (lookAt - transform.position).normalized;
        if (direction != Vector3.zero)
        {
            Quaternion lookRotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, TME_Manager.getDeltaTime(TME_Time.type.enemies) * 2);
        }
    }

    public override void attackTarget(GameObject target)
    {
        Vector3 vec = target.transform.position - transform.position;
        Vector3 dirToTarget = vec.normalized;

        lookTowards(target.transform.position);

        spawnMushroomAttack(dirToTarget);
    }

    public override void attackForwards()
    {
        Vector3 dirToTarget = transform.forward;

        spawnMushroomAttack(dirToTarget);

    }

    public override bool goToPosition(Vector3 position)
    {
        //Debug.Log("postion: "+position);
        if (mushroomAttacking) return false;
        if (agent.destination == position) return true;
        bool result = agent.SetDestination(position);

        return result;
    }

    public void spawnMushroomAttack(Vector3 dirToAttack)
    {
        Vector3 inAir = new Vector3(-.25F, 1.7F, .33F);
        GameObject spores;
        if (purity)
        {
            spores = UTL_Resources.cloneAtLocation("Abilities/MinionAttacks/AllyMushroomAttack", gameObject.transform.position + inAir, false);
            spores.transform.rotation = Quaternion.Euler(dirToAttack) * transform.rotation;
            spores.GetComponent<ABI_MushroomAttack>().body = this;
        }
        else
        {
            spores = UTL_Resources.cloneAtLocation("Abilities/MinionAttacks/EnemyMushroomAttack", gameObject.transform.position + inAir, false);
            spores.transform.rotation = Quaternion.Euler(dirToAttack) * transform.rotation;
            spores.GetComponent<ABI_MushroomAttack>().body = this;
        }
    }
}
