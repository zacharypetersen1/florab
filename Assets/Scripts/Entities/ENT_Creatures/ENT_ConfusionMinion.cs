﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_ConfusionMinion : ENT_Body {
    ENT_ConfusionAbility abi;

    float confusionAbilityTimer = 3f;
    public float CONFUSION_DURATION = 3f;

    public override void Awake()
    {
        base.Awake();
        abi = GetComponent<ENT_ConfusionAbility>();
    }

    public override void Update()
    {
        base.Update();
        if (CurrentTree == BehaviorTree.AllyActivateTree)
        {
            confusionAbilityTimer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
            if (confusionAbilityTimer <= 0)
            {
                abi.endAllyAbility();
                CurrentTree = BehaviorTree.AllyActivateTree;
            }
        }
        else if (confusionAbilityTimer >= 0)
        {
            confusionAbilityTimer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
            if (confusionAbilityTimer <= 0)
            {
                if (purity)
                {
                    abi.endAllyAbility();
                }
                else
                {
                    abi.endEnemyAbility();
                }
            }
        }
        
    }

    public override void purify_self()
    {
        ENT_ConfusionAbility ability = GetComponent<ENT_ConfusionAbility>();
        if (isAlly)
        {
            ability.endAllyAbility();
        }
        else
        {
            ability.endEnemyAbility();
        }
        abi.readyAbility();
        base.purify_self();
    }

    public void Confusion()
    {
        confusionAbilityTimer = CONFUSION_DURATION;
        if (purity)
        {
            abi.activateAllyAbility();
        }
        else
        {
            abi.activateEnemyAbility();
        }
    }

    


}
