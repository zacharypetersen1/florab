﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_ResurrectParticleEffect : MonoBehaviour {

    protected float resurrectTimer = 0f;
    [HideInInspector]
    public ENT_DungeonEntity.monsterTypes entity_type;
    [HideInInspector]
    public bool asAlly;
    public GameObject myNecromancer;
    public GameObject myRemnant;

    public bool finished;

    // Use this for initialization
    public virtual void Start () {
        resurrectTimer = 0f;
    }

    // Update is called once per frame
    public virtual void Update () {
		if (resurrectTimer <= 0)
        {
            finishResurrect();
        }
        else
        {
            resurrectTimer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
        }
	}

    public virtual void cancelResurrect()
    {
        finished = false;
        resurrectTimer = 100f;
        StartCoroutine(waitForSeconds());
        if (myRemnant != null)
        {
            Collider col = myRemnant.GetComponent<Collider>();
            col.enabled = false;
            col.enabled = true;
        }
    }

    public virtual void finishResurrect()
    {
        finished = true;
        GameObject newMinion = UTL_Dungeon.spawnMinion(entity_type, transform.position, asAlly);
        //print("Before: " + myNecromancer.GetComponent<ENT_Body>().allyManager.followGroup.getCount());
        myNecromancer.GetComponent<ENT_Body>().allyManager.followGroup.addToList(newMinion);
        //print("After: " + myNecromancer.GetComponent<ENT_Body>().allyManager.followGroup.getCount());
        myNecromancer.GetComponent<ENT_NecroMinion>().myResurrectedMinions.Add(newMinion);
        newMinion.GetComponent<ENT_Body>().isGhost = true;
        Destroy(myRemnant);
        resurrectTimer = 100f;
        StartCoroutine(waitForSeconds());
        
        
    }

    IEnumerator waitForSeconds()
    {
        yield return new WaitForSeconds(1.0f);
        foreach (ParticleSystem ps in GetComponentsInChildren<ParticleSystem>())
        {
            if (ps != null) ps.Stop();
        }
        
        Destroy(gameObject,2);
    }
}
