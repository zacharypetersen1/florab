﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_SlowAbility : ENT_MinionAbility {

    private GameObject slowAbility;
    private ABI_SlowAbility abilityScript;

    ENT_SlowMinion body;

    public override void Awake()
    {
        base.Awake();
        body = GetComponent<ENT_SlowMinion>();
    }

    protected override void _allyAbility()
    {
        base._allyAbility();
        slowAbility = UTL_Resources.cloneAsChild("Abilities/MinionAttacks/SlowAbility/SlowAbility", gameObject);
        abilityScript = slowAbility.GetComponent<ABI_SlowAbility>();
        abilityScript.Purity = true;
    }

    protected override void _enemyAbility()
    {
        base._enemyAbility();
        slowAbility = UTL_Resources.cloneAsChild("Abilities/MinionAttacks/SlowAbility/SlowAbility", gameObject);
        abilityScript = slowAbility.GetComponent<ABI_SlowAbility>();
        abilityScript.Purity = false;
    }

    public override void endAllyAbility()
    {
        base.endAllyAbility();

        ENT_Body body = gameObject.GetComponent<ENT_Body>();
        //body.CurrentTree = body.LastTree;
    }

    public override void endEnemyAbility()
    {
        base.endEnemyAbility();
    }

    public void OnDestroy()
    {
        if (GetComponent<ENT_Body>().isGhost)
        {
            foreach (GameObject slowObj in body.slowFlowers)
            {
                if (slowObj == null) continue;
                else
                {
                    slowObj.GetComponent<ENV_SlowObj>().killYourself();
                }
            }
        }
        if (slowAbility != null) Destroy(slowAbility, 1);

    }

    public void destroyMyAbility()
    {
        foreach (GameObject slowObj in body.slowFlowers)
        {
            if (slowObj == null) continue;
            else
            {
                slowObj.GetComponent<ENV_SlowObj>().killYourself();
            }
        }
        if (slowAbility != null) Destroy(slowAbility,.5f);
    }
}
