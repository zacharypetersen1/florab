﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_AbilityChargeIndicator : MonoBehaviour {
    ParticleSystem[] purePS;
    ParticleSystem[] corruptPS;
    ENT_MinionAbility abi;
    ENT_Body body;
    ParticleSystem.EmissionModule emitter;
    //public Material goodMat;
    //public Color goodColor;
    //public Material evilMat;
    //public Color evilColor;
    public GameObject corruptedIndicator;
    public GameObject pureIndicator;
    float cooldown;
    bool purity;

	// Use this for initialization
	void Start () {
        abi = GetComponentInParent<ENT_MinionAbility>();
        body = GetComponentInParent<ENT_Body>();
        if (pureIndicator == null)
        {
            Debug.LogError(gameObject.transform.parent.name + ": Pure ability charged effect indicator is null");
            return;
        }
        if (corruptedIndicator == null)
        {
            Debug.LogError(gameObject.transform.parent.name + ": corrupt ability charged effect indicator is null");
            return;
        }
        purePS = pureIndicator.GetComponentsInChildren<ParticleSystem>();
        corruptPS = corruptedIndicator.GetComponentsInChildren<ParticleSystem>();
        //ps = GetComponentInChildren<ParticleSystem>();
        //rend = GetComponentInChildren<ParticleSystemRenderer>();
        //emitter = ps.emission;
        //psMain = ps.main;
    }
	
	// Update is called once per frame
	void Update () {
        purity = body.Purity;
        if (purity) cooldown = abi.AllyCoolDownTimer;
        else cooldown = abi.EnemyCoolDownTimer;
        if (cooldown <= 0)
        {
            if (purity)
            {
                if (purePS == null)
                {
                    Debug.Log(gameObject.transform.parent.name + ": can't play pure PS");
                    return;
                }
                foreach (var ps in purePS)
                {
                    emitter = ps.emission;
                    emitter.enabled = true;
                }
                if (corruptPS == null) return;
                foreach (var ps in corruptPS)
                {
                    emitter = ps.emission;
                    emitter.enabled = false;
                }
            }
            else
            {
                if (corruptPS == null)
                {
                    Debug.Log(gameObject.transform.parent.name + ": can't play corrupt PS");
                    return;
                }
                foreach (var ps in corruptPS)
                {
                    emitter = ps.emission;
                    emitter.enabled = true;
                }
                if (purePS == null) return;
                foreach (var ps in purePS)
                {
                    emitter = ps.emission;
                    emitter.enabled = false;
                }
            }
        }
        else
        {
            if (corruptPS == null) return;
            foreach (var ps in corruptPS)
            {
                emitter = ps.emission;
                emitter.enabled = false;
            }
            if(purePS == null) return;
            foreach (var ps in purePS)
            {
                emitter = ps.emission;
                emitter.enabled = false;
            }
        }
	}
}
