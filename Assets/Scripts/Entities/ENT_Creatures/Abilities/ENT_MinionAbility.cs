﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//base class for minion abilities. Extend this for each minion ability
public class ENT_MinionAbility : MonoBehaviour {

    public float enemyMaxCoolDown = 10;
    public float allyMaxCoolDown = 10;

    protected bool abilitySuccessful = true;

    protected float enemyCoolDownTimer;
    public float EnemyCoolDownTimer
    {
        get { return enemyCoolDownTimer; }
    }

    protected float allyCoolDownTimer;
    public float AllyCoolDownTimer
    {
        get { return allyCoolDownTimer; }
    }

    private ENT_Body myBody;
    // Use this for initialization
    virtual public void Awake () {
        enemyCoolDownTimer = 0;
        allyCoolDownTimer = 0;
        myBody = gameObject.GetComponent<ENT_Body>();

    }
	
	// Update is called once per frame
	virtual public void Update () {
        enemyCoolDownTimer = Mathf.Max(0F, enemyCoolDownTimer - TME_Manager.getDeltaTime(TME_Time.type.enemies));
        allyCoolDownTimer = Mathf.Max(0F, allyCoolDownTimer - TME_Manager.getDeltaTime(TME_Time.type.enemies));
    }

    public void resetTimers()
    {
        if (abilitySuccessful)
        {
            enemyCoolDownTimer = enemyMaxCoolDown;
            allyCoolDownTimer = allyMaxCoolDown;
        }else
        {
            enemyCoolDownTimer = 1;
            allyCoolDownTimer = 1;
        }
     
    }

    public void readyAbility()
    {
        enemyCoolDownTimer = 0;
        allyCoolDownTimer = 0;
    }

    public bool activateEnemyAbility()
    {
        if (myBody.Purity) {
            print("cannot activate enemy ability from an ally minion");
            return false;
        }
        if (enemyCoolDownTimer > 0)
        {
            return false;
        }
        else
        {
            _enemyAbility();
            resetTimers();
            return true;
        }
    }

    public bool activateAllyAbility()
    {
        if (!myBody.Purity)
        {
            print("cannot activate ally ability from an enemy minion");
            return false;
        }
        if (allyCoolDownTimer > 0)
        {
            //print(gameObject.name + "'s ability still on cooldown");
            return false;
        }else
        {
            //print("activated slow ability");
            _allyAbility();
            resetTimers();
            return true;
        }  
    }

    //override these
    virtual protected void _enemyAbility()
    {
        myBody.IsAbilityActive = true;
    }

    //override these
    virtual protected void _allyAbility()
    {
        myBody.IsAbilityActive = true;
    }

    virtual public void endEnemyAbility()
    {
        myBody.IsAbilityActive = false;
        
    }

    virtual public void endAllyAbility()
    {
        myBody.IsAbilityActive = false;
    }
}
