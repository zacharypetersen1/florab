﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_SlowMinion : ENT_Body {

    ENT_SlowAbility abi;

    float slowAbilityTimer = 3f;
    public float SLOW_DURATION = 3f;

    public ArrayList slowFlowers;

    public override void Awake()
    {
        base.Awake();
        abi = GetComponent<ENT_SlowAbility>();
    }

    public override void Update()
    {
        base.Update();
        slowAbilityTimer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
        if (slowAbilityTimer <= 0)
        {
            if (isAbilityActive) {
                EndSlow();
            }
        }
    }

    public override void purify_self()
    {
        if (isAlly)
        {
            abi.endAllyAbility();
        }
        else
        {
            abi.endEnemyAbility();
        }
        abi.readyAbility();
        base.purify_self();
        if(IsAbilityActive) abi.destroyMyAbility();
        destroySlowFlowers();
    }

    public void destroySlowFlowers()
    {
        if (slowFlowers == null) return;
        foreach (GameObject slowFlower in slowFlowers)
        {
            if (slowFlower != null)
            {
                ENV_SlowObj slowScript = slowFlower.GetComponent<ENV_SlowObj>();
                if (slowScript != null)
                {
                    slowScript.killYourself();
                }
            }
        }
    }

    public void Slow()
    {
        slowAbilityTimer = SLOW_DURATION;
        if (purity)
        {
            abi.activateAllyAbility();
        }
        else
        {
            abi.activateEnemyAbility();
        }
    }

    public void EndSlow()
    {
        if (purity)
        {
            abi.endAllyAbility();
        }
        else
        {
            abi.endEnemyAbility();
        }
    }
}
