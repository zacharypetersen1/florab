﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class ENT_Body : ENT_DungeonEntity
{
    public bool startsAsAlly = false;
    public bool purifiable = true;
    public Material evilMaterial;
    public Material goodMaterial;
    public Color allyHealthBarColor = Color.green;
    public Color enemyHealthBarColor = new Color(0x57, 0x00, 0x9E);
    [HideInInspector]
    public float attackRechargeTime = 1f;
    public monsterTypes entity_type;
    public ParticleSystem purifySparkle;

    //-- start behavior trees
    public BrickAsset AttackTree;
    public BrickAsset WorkTree;
    public BrickAsset LookaroundTree;
    public BrickAsset EnemyWaitTree;

    public BrickAsset ConfusionTree;

    public BrickAsset AllyFollowTree;
    public BrickAsset AllyWaitTree;
    public BrickAsset AllyGoToTree;
    public BrickAsset AllyActivateTree;
    public BrickAsset AllyAttackTree;
    //-- end behavior tree variables

    public bool dontWander = false;

    public float threatRange = 10; //the range at which this monster will choose to attack it's enemies
    public float attackRange = 5; //the range at which a monster must be to actually initiate an attack
    public float sightRange = 30; //how far away the monster can see objects
    public float sightRadius = 90F; //the LOS radius 
    public float pickupRange = 3; //the range at which this minion can pick up crystals
    public float autoDetectRange = 9F;

    //public float nearFollowRadius;
    public float farFollowRadius = 10F;

    public float normalAttackSpeed = 1f;
    private float slowAttackSpeed = 0.5f;

    public int normalSpeed = 20;
    public int allySpeedBoost = 10;
    public int surfingSpeedBoost = 25;
    public int slowSpeed = 10;

    protected float patrolTimer = 0F;

    public GameObject spawnedBy;
    public GameObject partolTarget;
    public GameObject workTarget;

    public bool finishedSearchLeft = true;
    public bool finishedSearchForward = true;
    public Vector3 searchForward;
    public Vector3 searchLeft;
    public Vector3 searchRight;
    public bool startSearch = true;
    public float searchTimer = 0F;
    const float SEARCH_TIME = 10F;

    public bool shouldAttack = false;

    public AIC_AllyGroup lastGroup;
    public AIC_AllyGroup commandGroup;
    static GameObject unitContainer;

    public bool shouldRotate = false;
    public Vector3 lookTowardsVec; //also set shouldRotate to true if you change this.

    public bool carrying = false;
    public Vector3 enemyWaitPosition = new Vector3(0, 0, 0);

    private PLY_PlayerDriver playerDriver;
    private GameObject healthBarObj;

    GameObject _confusionEffect = null;
    GameObject _slowEffect;

    //public UIX_CameraFacingBillboard_Minion healthBar;
    protected const int NUMWANDERPOINTS = 4;

    //private Vector3 patrolPos;
    private int patrolIndex = 0;
    public bool startPatrol = false; //should be set to true on entering patrol state

    float timeSinceDamage = 0f;
    const float TIME_UNTIL_HEAL = 25f;
    float healRate = 0.5f; //CL per second
    float _maxHealRatio = 0.5f; //only heals to max health * maxHealRatio
    float maxHealRatio
    {
        get
        {
            return _maxHealRatio;
        }
        set
        {
            maxHealRatio = value;
            maxHealCL = CL_MAX * maxHealRatio;
        }
    }
    float maxHealCL;

    public bool isAbilityActive = false;
    public bool IsAbilityActive
    {
        get { return isAbilityActive; }
        set { isAbilityActive = value;
            //print("setting isAbilityActive to " + value); 
        }
    }

    //private float maxPurityValue;
    private float _purityValue = 0;
    public float purityValue
    {
        get
        {
            return _purityValue;
        }
        set
        {
            _purityValue = value;
            //setPurityBar(_purityValue);
            if (_purityValue >= CL)
            {
                Purity = true;
            }
        }
    }

    public float purityReductionRate = 0.15f;

    public override float CL
    {
        get { return _CL; }
        set
        {
            float newCL = value;
            float difference = _CL - newCL;
            if (difference > 0)
            {
                float scale;
                if (!purity)
                {
                    if (CL != 0)
                    {
                        scale = difference / CL;
                    }
                    else
                    {
                        scale = 0;
                    }
                    if (CL > 0)
                    {
                        purityValue -= purityValue * scale;
                    }
                }
            }
            
            base.CL = newCL;
        }
    }

    public List<ParticleCollisionEvent> collisionEvents;

    public float positionSensitivity;

    public int confusedIndex = 0;
    public bool startConfusion = false;

    public AIC_Zone myZone;

    ParticleSystem[] VFX;

    public List<Pair<CBI_CombatMessenger.status, float>> StatusList = new List<Pair<CBI_CombatMessenger.status, float>>();

    private ENT_MinionAbility ability;

    private List<Pair<CBI_CombatMessenger.status, float>> removeList = new List<Pair<CBI_CombatMessenger.status, float>>();

    private List<Vector3> _PatrolPoints = new List<Vector3>();
    public List<Vector3> PatrolPoints
    {
        get { return _PatrolPoints; }
        set
        {
            _PatrolPoints.Clear();
            if (value == null || value.Count == 0)
            {
                _PatrolPoints = setRandomPatrolPoints();
                return;
            }

            foreach (Vector3 vec in value)
            {
                bool vecIsValid;
                Vector3 new_vec = ValidifyPoint(vec, out vecIsValid);
                if (vecIsValid)
                {
                    _PatrolPoints.Add(new_vec);
                }
            }
        }
    }

    private List<Vector3> _ConfusedPoints = new List<Vector3>();
    public List<Vector3> ConfusedPoints
    {
        get { return _ConfusedPoints; }
        set
        {
            _ConfusedPoints.Clear();
            if (value == null || value.Count == 0)
            {
                _ConfusedPoints = setRandomPatrolPoints();
                return;
            }
            foreach (Vector3 vec in value)
            {
                bool vecIsValid;
                Vector3 new_vec = ValidifyPoint(vec, out vecIsValid);
                if (vecIsValid)
                {
                    _ConfusedPoints.Add(new_vec);
                }
            }
        }
    }

    public enum BehaviorTree
    {
        AttackTree, WorkTree, lookaroundTree, enemyWaitTree , AllyFollowTree, AllyWaitTree, AllyGoToTree, AllyActivateTree, AllyAttackTree, ConfusionTree
    }

    //protected CHA_Motor motor;
    protected NavMeshAgent agent;
    public NavMeshAgent Agent
    {
        get { return agent; }
    }
    //protected PTH_Path path;
    protected BehaviorExecutor AI;
    public BehaviorExecutor ai {
        get { return AI;  }
    }

    public Vector3 endPoint {
        get { return agent.destination; }
        set { goToPosition(value); }
    }

    //Vector3 pastPos;
    //bool move = true;
    public float attackCooldown; // 0 when attack is ready, gets set to attackRechargeTime on attack, goes down by 1/second

    //private bool generatePathThisFrame = true; //whether or not the minion should generate a new path each frame

    protected GameObject _crystal;
    public GameObject crystal
    {
        get { return _crystal; }
        set { setTargetCrystal(value); }
    }

    public Vector3 myGoToPos;

    protected Vector3 waitPos;
    public Vector3 WaitPos
    {
        get { return waitPos; }
        set { waitPos = value; }
    }

    protected GameObject _attackTarget;
    public GameObject AttackTarget
    {
        get { return _attackTarget; }
        set { _attackTarget = value; }
    }
    //private GameObject nearestCrystal;
    BehaviorTree lastTree;
    public BehaviorTree LastTree
    {
        get { return lastTree; }
    }

    protected BehaviorTree _currentTree;
    public BehaviorTree CurrentTree
    {
        get { return _currentTree; }
        set
        {
            _currentTree = value;
            startSearch = false;
            shouldRotate = false;
            switch (_currentTree)
            {
                case BehaviorTree.AttackTree:
                    AI.behavior = AttackTree;
                    if (!isAlly)
                    {
                        if(getMyType() == monsterTypes.treeMinion)
                        {
                            FMODUnity.RuntimeManager.PlayOneShot("event:/enemy_tree_aggro", transform.position);
                        }
                        else if(getMyType() == monsterTypes.flowerMinion)
                        {
                            FMODUnity.RuntimeManager.PlayOneShot("event:/enemy_flower_aggro", transform.position);
                        }
                        else if(getMyType() == monsterTypes.mushroomMinion)
                        {
                            FMODUnity.RuntimeManager.PlayOneShot("event:/enemy_mushroom_aggro", transform.position);
                        }                        
                    }
                    break;
                case BehaviorTree.WorkTree:
                    startPatrol = true;
                    if (PatrolPoints == null || PatrolPoints.Count == 0) PatrolPoints = setRandomPatrolPoints();
                    //Debug.Log("patrol");
                    AI.behavior = WorkTree;
                    break;
                case BehaviorTree.lookaroundTree:
                    startRandomLookaround();
                    AI.behavior = LookaroundTree;
                    break;
                case BehaviorTree.enemyWaitTree:
                    AI.behavior = EnemyWaitTree;
                    break;
                case BehaviorTree.AllyFollowTree:
                    AI.behavior = AllyFollowTree;
                    break;
                case BehaviorTree.AllyWaitTree:
                    AI.behavior = AllyWaitTree;
                    break;
                case BehaviorTree.AllyGoToTree:
                    AI.behavior = AllyGoToTree;
                    break;
                case BehaviorTree.AllyActivateTree:
                    if (lastTree == BehaviorTree.AllyActivateTree)
                    {
                        //AI.paused = true;
                        CurrentTree = lastTree;
                        //ability.endAllyAbility();
                    } else
                    {
                        if (ability.activateAllyAbility()) AI.behavior = AllyActivateTree;
                        else
                        {
                            _currentTree = lastTree;
                            if (commandGroup != null) commandGroup.removeFromList(gameObject);
                            if (lastGroup != null) lastGroup.addToList(gameObject);
                        }
                    }
                    break;
                case BehaviorTree.AllyAttackTree:
                    if (AttackTarget == null)
                    {
                        print("must have attackTarget set to enter attack tree. Try setting AttackTarget before setting CurrentTree");
                    }
                    AI.behavior = AllyAttackTree;
                    break;
                case BehaviorTree.ConfusionTree:
                    startConfusion = true;
                    AI.behavior = ConfusionTree;
                    break;
            }
            if (_currentTree != BehaviorTree.ConfusionTree && _currentTree != BehaviorTree.AllyActivateTree) lastTree = _currentTree;
        }
    }

    private ENV_CrystalSpawner crystalSpawner;
    public ENV_CrystalSpawner CrystalSpawner
    {
        get { return crystalSpawner; }
    }

    protected bool _ghost;
    public bool isGhost
    {
        set {
            if (value == false) return;
            else ghostify();
        }
        get { return _ghost; }
    }

    public float myFollowDistance = 5f;

    Animator anim;
    public bool reappear = false;
    public Vector3 teleportationLocation;

    public bool disableAI = false;

    public Collider col;

    static SelectionManager selectionManager;
    bool showHealthBar = false;

    float enemyShowHealthTimer = 0;

    float maxEnemyShowHealthTimer = 10;
    public float EnemyShowHealthTimer
    {
        get { return enemyShowHealthTimer; }
        set
        {
            enemyShowHealthTimer = value;
            if (enemyShowHealthTimer > 0)
                showHealthBar = true;
            else
                showHealthBar = false;

        }
    }
    

    // Use this for initialization
    public override void Awake()
    {
        base.Awake();
        col = GetComponent<Collider>();
        //motor = gameObject.GetComponent<CHA_Motor>();
        agent = GetComponent<NavMeshAgent>();
        positionSensitivity =agent.stoppingDistance;
        if (agent == null)
        {
            print("error: No NavMeshAgent Component");
        }
        AI = gameObject.GetComponent<BehaviorExecutor>();
        //healthBar = gameObject.GetComponentInChildren<UIX_CameraFacingBillboard_Minion>();

        //New health bars
        healthBarObj = UTL_Resources.cloneAsChild("FloatingUI/newHealthBar", gameObject);
        healthBarObj.GetComponent<UIX_ParentBar>().offset = 5;

        //All minions need to be set to active for cleanse and purify to work properly
        State = "active";
        slowAttackSpeed = normalAttackSpeed * 2f;
        maxHealCL = CL_MAX * maxHealRatio;
        if (unitContainer == null)
        {
            unitContainer = GameObject.Find("UnitContainer");
        }
        if (selectionManager == null)
        {
            selectionManager = GameObject.Find("SelectionManager").GetComponent<SelectionManager>();
        }
        //transform.position = UTL_Dungeon.getTerrPosition(transform.position);
    }
    // Path finding breaks if this is called in Awake()
    public override void Start()
    {
        base.Start();
        anim = GetComponentInChildren<Animator>();
        playerDriver = player.GetComponent<PLY_PlayerDriver>();
        //set start speed 
        //agent.speed = normalSpeed;
        agent.speed = playerDriver.normalSpeed;

        //patrolPos = transform.position;
        attackCooldown = 0f;
        crystalSpawner = GameObject.Find("Scripts").GetComponent<ENV_CrystalSpawner>();
        //THIS IS FOR TESTING ONLY
        //PatrolPoints = new List<Vector3>() { new Vector3(50, 50, 50), new Vector3(60, 60, 60) };
        Vector3 location = UTL_Dungeon.snapPositionToTerrain(transform.position);
        int navmeshMask = 1 << NavMesh.GetAreaFromName("Walkable");
        NavMeshHit hit;
        if (NavMesh.SamplePosition(location, out hit, 10, navmeshMask))
        {
            location = hit.position;
        }
        else
        {
            Debug.Log("stupid Sample Position didn't work");
        }
        transform.position = location;
        agent.enabled = false;
        agent.enabled = true;
        if (PatrolPoints == null || PatrolPoints.Count == 0)
        {
            PatrolPoints = setRandomPatrolPoints();
            //PatrolPoints = new List<Vector3>() { transform.position };
        }

        //sets patrol points else wont start patrolling if STARTING in patrol state
        startPatrol = true;
        enemyWaitPosition = gameObject.transform.position;
        //agent.SetDestination(transform.position);
        // initialize to full health
        CL = CL_MAX;
        ability = gameObject.GetComponent<ENT_MinionAbility>();
        Purity = startsAsAlly;
        VFX = GetComponentsInChildren<ParticleSystem>();
        List<ParticleSystem> tempList = new List<ParticleSystem>();
        foreach (var vf in VFX)
        {
            try
            {
                if (!vf.gameObject.name.Contains("Finish") && !(vf.gameObject.tag == "resurrectEffect"))
                {
                    tempList.Add(vf);
                }
            }
            catch
            {
                print("setting up particle effects didnt work right");
            }
        }
        VFX = tempList.ToArray();
        //SetEnabled(false);
        SetPSEnabled(false);
        Collider col = GetComponent<Collider>();
        col.enabled = false;
        col.enabled = true;
        //gameObject.SetActive(false);
        //ShowMinion(false);
        // intialize the minion based on if they start as an enemy or ally
        collisionEvents = new List<ParticleCollisionEvent>();
        if (!purity) myZone = AIC_ZoneManager.putMinionInZone(gameObject);
        //maxPurityValue = _CL_MAX;
        
    }

    public List<Vector3> setRandomPatrolPoints()
    {
        
        List<Vector3> pointsList = new List<Vector3>();
        if (dontWander)
        {
            Vector3 point = transform.position;
            bool pointIsValid;
            Vector3 adjustedPoint = ValidifyPoint(point, out pointIsValid);
            if (pointIsValid)
            {
                pointsList.Add(adjustedPoint);
            }
            else
            {
                pointsList.Add(point);
            }
            return pointsList;
        }
        for (int i = 0; i < NUMWANDERPOINTS; ++i)
        {
            float randomAngle = Random.Range(0, 355);
            float randomDist = Random.Range(5, 20);
            Vector3 randomPoint = new Vector3(Mathf.Sin(Mathf.Deg2Rad * randomAngle), 0, Mathf.Cos(Mathf.Deg2Rad * randomAngle));
            randomPoint *= randomDist;
            randomPoint += transform.position;
            bool pointIsValid;
            randomPoint = ValidifyPoint(randomPoint, out pointIsValid);
            if (pointIsValid)
            {
                pointsList.Add(randomPoint);
            }
        }
        return pointsList;
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if (patrolTimer > 0 && (startPatrol || Vector3.Distance(transform.position, PatrolPoints[patrolIndex]) <= positionSensitivity + 1)) patrolTimer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
        if (CurrentTree == BehaviorTree.lookaroundTree)
        {
            if (searchTimer <= 0f)
            {
                CurrentTree = BehaviorTree.WorkTree;
            }
            else
            {
                searchTimer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
            }
        }
        if (shouldRotate)
        {
            lookTowards(lookTowardsVec);
        }
        if (_ghost)
        {
            CL -= .7f * TME_Manager.getDeltaTime(TME_Time.type.enemies);
        }
        if (attackCooldown > 0)
        {
            attackCooldown = Mathf.Max(0F, attackCooldown - TME_Manager.getDeltaTime(TME_Time.type.enemies));
        }
        //if (AI.paused) AI.paused = false;
        bool slowed = false;
        foreach (var pair in StatusList)
        {
            if (pair.value1 == CBI_CombatMessenger.status.slow || pair.value1 == CBI_CombatMessenger.status.meleeSlow) slowed = true;
        }
        if (!slowed)
        {
            if (purity)
            {
                
                if (playerDriver.activeState == PLY_MoveState.type.surf)
                {
                    //agent.speed = normalSpeed + allySpeedBoost + surfingSpeedBoost;
                    agent.speed = playerDriver.surfingSpeed;
                    agent.angularSpeed = 300;
                }
                else
                {
                    //agent.speed = normalSpeed + allySpeedBoost;
                    agent.speed = playerDriver.normalSpeed;
                    agent.angularSpeed = 240;
                }
            }
            else
            {
                //agent.speed = normalSpeed;
                agent.speed = playerDriver.normalSpeed;
                agent.angularSpeed = 240;
            }
            attackRechargeTime = normalAttackSpeed;
        }
        //GetComponent<LineRenderer>().SetPosition(0, transform.position + new Vector3(0, 5, 0));
        //GetComponent<LineRenderer>().SetPosition(1, transform.position + new Vector3(0, 5, 0) + (transform.forward * 5));
        applyStatusEffects();
        if (transform.hasChanged && !purity)
        {
            AIC_ZoneManager.updateZone(this);
        }
        timeSinceDamage += TME_Manager.getDeltaTime(TME_Time.type.enemies);
        if (!isGhost && timeSinceDamage >= TIME_UNTIL_HEAL && CL < maxHealCL)
        {
            inflict_healing(healRate * TME_Manager.getDeltaTime(TME_Time.type.enemies));
        }
        if (!purity && purityValue > 0 && isOnCorruption())
        {
            purityValue = Mathf.Max(0, purityValue - (purityReductionRate * TME_Manager.getDeltaTime(TME_Time.type.enemies)));
        }

        //check if selected ally, then show health bar
        if (isAlly)
        {
            List<GameObject> selectedMinions = selectionManager.GetSelectedObjectsAsList();
            showHealthBar = (selectedMinions.Contains(gameObject) && SelectionManager.isInSelectMode);
            toggleHealthBar();
        }else
        {      
            if(CurrentTree == BehaviorTree.AttackTree)
            {
                showHealthBar = SelectionManager.isInSelectMode;
                EnemyShowHealthTimer = maxEnemyShowHealthTimer;
                toggleHealthBar();
            }else if(EnemyShowHealthTimer <= 0 && CurrentTree != BehaviorTree.AttackTree)
            {
                showHealthBar = false;
            }
            EnemyShowHealthTimer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
            toggleHealthBar();
        }
        //DebugPath();
        //if (isAlly) DebugGoToPos();
    }

    void toggleHealthBar()
    {
        healthBarObj.GetComponentInChildren<UIX_HealthBar>().toggleBar(showHealthBar);
    }

    public override void activate()
    {

    }

    public ENT_DungeonEntity.monsterTypes getMyType()
    {
        return entity_type;
    }
    public GameObject getSpawnedBy()
    {
        return spawnedBy;
    }

    #region Behavior Tree Functions

    public void hangout()
    {
        agent.SetDestination(transform.position);
    }

    /*
    public void goToWork()
    {
        //This is temporary for testing purposes
        //This should be done when the minion is spawned, also update minion spawnedby and patroltarget there!!!!!!!!!!!!!!!!!!!!
        //monsterBrain.MyJob = AIC_Brain.work.gather;
        if (Vector2.Distance(endPoint, transform.position) > 3)
        {
            path = PTH_Finder.findPath(transform.position, endPoint);
        }
        else if (Vector2.Distance(workTarget.transform.position, transform.position) < 3)
        {
            //currently needs more logic in doWork() for this to work
            doWork();
            endPoint = spawnedBy.transform.position;
            path = PTH_Finder.findPath(transform.position, endPoint);
        }
        //defaults back to work target
        else
        {
            //currently needs more logic in doWork() for this to work
            doWork();
            endPoint = workTarget.transform.position;
            path = PTH_Finder.findPath(transform.position, endPoint);
        }
    }*/

    public void gatherCrystal(GameObject crystal)
    {
        ENV_GroundCrystal gc = crystal.GetComponent<ENV_GroundCrystal>();
        if (gc == null)
        {
            print(gameObject.name + " tried to gatherCrystal on non-crystal object " + crystal.gameObject.name);
            return;
        }
        if (!crystalInRange(crystal))
        {
            print(gameObject.name + " is not within pickupRange of " + crystal.gameObject.name);
            return;
        }
        gc.takeCrystal(gameObject);
        //if near target pickup crystal and reassign target
        //if no target reassign target
        //nearestCrystal = crystalSpawner.getNearestCrystal(UTL_Math.vec3ToVec2(gameObject.transform.position));
    }

    public bool crystalInRange(GameObject crystal)
    {
        return Vector3.Distance(transform.position, crystal.transform.position) <= pickupRange;
    }

    //Don't use unless you are devon
    public virtual void patrol()
    {
        //if at current patrol point
        //move to next patrol point
        if (PatrolPoints.Count == 0)
        {
            startPatrol = false;
            return;
        }
        if (startPatrol || Vector3.Distance(transform.position, PatrolPoints[patrolIndex]) <= positionSensitivity + 1)
        {
            if (patrolTimer <= 0)
            {
                //print("starting patrol");
                patrolIndex = ++patrolIndex;
                if (patrolIndex >= PatrolPoints.Count) patrolIndex = 0;

                Vector3 position = PatrolPoints[patrolIndex];
                goToPosition(position);
                //print("patrolIndex " + patrolIndex);
                startPatrol = false;
                patrolTimer = 3;
            }
        }
    }

    //Don't use unless you are devon
    public void traverseConfusedPath()
    {
        //if at current patrol point
        //move to next patrol point
        if (startConfusion || Vector3.Distance(transform.position, ConfusedPoints[confusedIndex]) <= positionSensitivity + 1)
        {
            //print("starting patrol");

            confusedIndex = ++confusedIndex;
            if (confusedIndex >= ConfusedPoints.Count) confusedIndex = 0;
            goToPosition(ConfusedPoints[confusedIndex]);
            startConfusion = false;
        }
    }

    //deprecated
    public void goToRandomPosition()
    {

        goToNearbyPosition();
    }

    public void goToPlayer()
    {
        goToPosition(player.transform.position);
    }

    public bool CanSeePlayer()
    {
        //check if player is in sightRange before LOS check
        if (Vector3.Distance(gameObject.transform.position, player.transform.position) > sightRange) return false;
        return UTL_Dungeon.checkLOSWallsThickness(transform.position, player.transform.position, .5f);
    }

    public bool isNearPlayer(float distance = 0)
    {
        if (distance == 0) distance = threatRange;
        return Vector3.Distance(transform.position, player.transform.position) < distance;
    }

    public void spawnProjectileAttack(Vector3 dirToTarget)
    {
        GameObject projectile = null;
        Vector3 inAir = gameObject.transform.position + (Vector3.up * 2);
        if (purity)
        {
            if(entity_type == monsterTypes.flowerMinion || entity_type == monsterTypes.confusionMinion)
            {
                projectile = UTL_Resources.cloneAtLocation("Abilities/MinionAttacks/AllyFlowerAttack", inAir + dirToTarget * 1, false);
                
            }
            else
            {
                projectile = UTL_Resources.cloneAtLocation("Abilities/MinionAttacks/AllyBasicAttack", inAir + dirToTarget * 1, false);
            }
        }
        else
        {
            if(entity_type == monsterTypes.flowerMinion || entity_type == monsterTypes.confusionMinion)
            {
                projectile = UTL_Resources.cloneAtLocation("Abilities/MinionAttacks/EnemyFlowerAttack", inAir + dirToTarget * 1,false);
            }
            else
            {
                projectile = UTL_Resources.cloneAtLocation("Abilities/MinionAttacks/EnemyBasicAttack", inAir + dirToTarget * 1, false);
            }
        }
        ABI_Projectile proj = projectile.GetComponent<ABI_Projectile>();
        proj.setDirection(dirToTarget);
        proj.maxDistance = attackRange + 3; //extra amount so flowers don't stop prematurely
        if (attackRange > proj.Velocity) proj.Velocity = attackRange;
        //attackCooldown = attackRechargeTime;
    }

    public void spawnMeleeAttack()
    {
        Vector3 meleePos = new Vector3();
        if (entity_type == monsterTypes.slowMinion)
        {
            meleePos = UTL_Dungeon.snapPositionToTerrain(transform.position + (transform.forward * 2.2F));
        }
        else
        {
            meleePos = UTL_Dungeon.snapPositionToTerrain(transform.position + (transform.forward * 0.35F));
        }

        GameObject melee = UTL_Resources.cloneAtLocation("Abilities/MinionAttacks/meleeAttack", meleePos);
        

        if (entity_type == monsterTypes.slowMinion)
        {
            melee.GetComponent<ABI_MeleeAttack>().entityType = monsterTypes.slowMinion;
        }
        else
        {
            melee.GetComponent<ABI_MeleeAttack>().entityType = monsterTypes.treeMinion;
        }

        melee.transform.rotation = transform.rotation;
        melee.GetComponent<ABI_MeleeAttack>().purity = Purity;
    }


    public virtual void attackTarget(GameObject target)
    {
        Vector3 vec = target.transform.position - transform.position;
        Vector3 dirToTarget = vec.normalized;
        if (!isFacingForAttack(target.transform.position))
        {
            //this means the target is behind the player
            lookTowards(target.transform.position);

            //maybe should force target to rotate to player?
        }
        if (entity_type == monsterTypes.treeMinion)
        {
            spawnMeleeAttack();
        }
        
        else
        {
            spawnProjectileAttack(dirToTarget);
        }
    }

    public virtual void attackForwards()
    {
        Vector3 dirToTarget = transform.forward;
        if (entity_type == monsterTypes.treeMinion || entity_type == monsterTypes.slowMinion)
        {
            spawnMeleeAttack();
        }
        else
        {
            spawnProjectileAttack(dirToTarget);
        }
    }

    public void attack()
    {
        GameObject target;
        if (purity) target = allyManager.getNearestVisibleEnemy(transform.position, attackRange, false);
        else target = allyManager.getNearestVisibleAlly(transform.position, attackRange);
        if (target != null)
        {
            attackTarget(target);
        }
        else attackForwards();
    }

    public virtual bool isFacingForAttack(Vector3 pos)
    {
        pos.y = transform.position.y;
        float halfRad = sightRadius / 3;
        if (Vector3.Angle(transform.forward, pos - transform.position) < halfRad)
        {
            return true;
        }
        else return false;
    }

    //returns whether the pos is within the 180 degree area in front of the monster
    public virtual bool isFacing(Vector3 pos)
    {
        if (Vector3.Distance(transform.position, pos) < autoDetectRange) return true;
        pos.y = transform.position.y;
        //minions can always detect in a small 360 radius
        float halfRad = sightRadius / 2;
        if (Vector3.Angle(transform.forward, pos - transform.position) < halfRad)
        {
            return true;
        }
        else return false;
    }

    public void goToNearbyPosition()
    {
        //distance of next point
        float distance = 10;
        goToPosition(getNearbyPosition(distance));
    }

    //Get random position whithin dist of gameObject
    public Vector3 getNearbyPosition(float dist)
    {
        Vector3 randDirection = Random.insideUnitSphere * dist;

        randDirection += transform.position;

        randDirection = UTL_Dungeon.snapPositionToTerrain(randDirection);

        return randDirection;
    }

    public bool isAtPosition(Vector3 pos, float reducedSensitivity = 0F)
    {
        //print("positionSensitivity " + positionSensitivity);

        bool result = Vector3.Distance(transform.position, pos) < (positionSensitivity + reducedSensitivity);
        //if (result) dontMove();
        return result;
    }

    public bool isMidpointNearTwig()
    {
        return allyManager.isMidpointNearTwig();
    }

    public bool amINearTwig()
    {
        return allyManager.amINearTwig(gameObject.transform.position);
    }

    public bool amINearSpawner(float distance = 10)
    {
        return Vector3.Distance(gameObject.transform.position, spawnedBy.transform.position) < distance;
    }

    public bool amINearSpawner()
    {
        try
        {
            return Vector3.Distance(gameObject.transform.position, spawnedBy.transform.position) < 10F;
        }
        catch
        {
            Debug.Log("Haro2D needs to fix this!");
            return false;
        }
    }

    public bool amINearWaitPos()
    {
        return Vector3.Distance(gameObject.transform.position, WaitPos) < positionSensitivity;
    }

    public bool isMidpointNearWaitPos()
    {
        return Vector3.Distance(allyManager.getMidpoint(), WaitPos) < allyManager.farFollowDistance;
    }
    #endregion

    public override void purify_self()
    {
        if (purifiable)
        {
            purity = true;
            gameObject.layer = 17;
            //print(gameObject.name + " purified");
            //print("Purifying " + gameObject.name);
            //confusion effect not going away fix
            if (CurrentTree != BehaviorTree.ConfusionTree)
            {
                CurrentTree = BehaviorTree.AllyFollowTree;
                lastTree = BehaviorTree.AllyFollowTree;
            }
            else
            {
                lastTree = BehaviorTree.AllyFollowTree;
            }

            //CurrentTree = BehaviorTree.ConfusionTree;
            allyManager.removeEnemy(gameObject);
            commandGroup = allyManager.addAlly(gameObject);
            healthBarObj.GetComponentInChildren<UIX_Bar>().BarColor = allyHealthBarColor;

            GameObject alarmSphere = null;
            var children = GetComponentsInChildren<Transform>();
            foreach (var child in children)
            {
                //Remove crystals
                if (child.name.StartsWith("Crystal"))
                {
                    child.GetComponent<MeshRenderer>().enabled = false;
                    ParticleSystem.EmissionModule crystalEmitter = child.GetComponentInChildren<ParticleSystem>().emission;
                    crystalEmitter.enabled = false;
                    Destroy(child.gameObject, 3F);
                }

                if(child.tag == "alarmSphere")
                {
                    alarmSphere = child.gameObject;
                }
            }

            if(alarmSphere != null)
            {
                Destroy(alarmSphere);
            }

            //myFollowDistance = allyManager.getNextFollowDist;
            //ability.resetTimers();

            
            if (myZone != null) myZone.removeMinionFromZone(gameObject);

            //Make this minion selectable
            gameObject.transform.parent = unitContainer.transform;

            // Update material
            if (!isGhost)
            {
                SkinnedMeshRenderer render = GetComponentInChildren<SkinnedMeshRenderer>();
                render.material = UTL_Resources.getMinionMaterial(entity_type);
            }

            // Fill up health
            CL = CL_MAX;

            // Add purify hit marker
            if (!startsAsAlly) UIX_PurifyMarker.markPurify(entity_type);

            //play purification sound here
            FMODUnity.RuntimeManager.PlayOneShot("event:/twig_purify_success", transform.position);  
        }
    }

    public override void corrupt_self()
    {
        purity = false;
        //print(gameObject.name + " corrupted");
        allyManager.removeAlly(gameObject);
        allyManager.addEnemy(gameObject);
        CurrentTree = BehaviorTree.WorkTree;
        healthBarObj.GetComponentInChildren<UIX_Bar>().BarColor = enemyHealthBarColor;
        //if we end up using this function, make sure to add the minion to the zone it is in
        //ability.resetTimers();
    }

    public override void cleanse_self()
    {

        base.cleanse_self();

        //play death sound 

        if (purity)
        {
            commandGroup.removeFromList(gameObject);
            //print("removed from list");
            allyManager.removeAlly(gameObject);

        }
        else
        {
            allyManager.removeEnemy(gameObject);
        }

        //Only allies play Cleanse Effect
        if (isAlly)
        {
            UTL_Resources.cloneAtLocation("Particles/CleanseEffect", transform.position);
        }

        //Spawn Remnant at cleanse location
        //NO ONE DROPS REMNANTS IN THIS VERSION
        /*
        if (!isGhost) {
            GameObject remnant;
            Collider remnantCollider;
            switch (entity_type)
            {
                case monsterTypes.flowerMinion:
                    remnant = UTL_Resources.cloneAtLocation("Minions/Remnants/FlowerRemnent", gameObject.transform.position);
                    remnant.GetComponent<ENT_Remnant>().entity_type = entity_type;
                    remnantCollider = remnant.GetComponent<Collider>();
                    remnantCollider.enabled = false;
                    remnantCollider.enabled = true;
                    remnant.transform.rotation = transform.rotation;
                    break;
                case monsterTypes.mushroomMinion:
                    remnant = UTL_Resources.cloneAtLocation("Minions/Remnants/MushroomRemnent", gameObject.transform.position);
                    remnant.GetComponent<ENT_Remnant>().entity_type = entity_type;
                    remnantCollider = remnant.GetComponent<Collider>();
                    remnantCollider.enabled = false;
                    remnantCollider.enabled = true;
                    remnant.transform.rotation = transform.rotation;
                    break;
                case monsterTypes.treeMinion:
                    remnant = UTL_Resources.cloneAtLocation("Minions/Remnants/TreeRemnent", gameObject.transform.position);
                    remnant.GetComponent<ENT_Remnant>().entity_type = entity_type;
                    remnantCollider = remnant.GetComponent<Collider>();
                    remnantCollider.enabled = false;
                    remnantCollider.enabled = true;
                    remnant.transform.rotation = transform.rotation;
                    break;
                case monsterTypes.necroMinion:
                    remnant = UTL_Resources.cloneAtLocation("Minions/Remnants/NecroRemnent", gameObject.transform.position);
                    remnant.GetComponent<ENT_Remnant>().entity_type = entity_type;
                    remnantCollider = remnant.GetComponent<Collider>();
                    remnantCollider.enabled = false;
                    remnantCollider.enabled = true;
                    remnant.transform.rotation = transform.rotation;
                    break;
                case monsterTypes.slowMinion:
                    remnant = UTL_Resources.cloneAtLocation("Minions/Remnants/StickyRemnent", gameObject.transform.position);
                    remnant.GetComponent<ENT_Remnant>().entity_type = entity_type;
                    remnantCollider = remnant.GetComponent<Collider>();
                    remnantCollider.enabled = false;
                    remnantCollider.enabled = true;
                    remnant.transform.rotation = transform.rotation;
                    break;
                case monsterTypes.confusionMinion:
                    remnant = UTL_Resources.cloneAtLocation("Minions/Remnants/BellRemnent", gameObject.transform.position);
                    remnant.GetComponent<ENT_Remnant>().entity_type = entity_type;
                    remnantCollider = remnant.GetComponent<Collider>();
                    remnantCollider.enabled = false;
                    remnantCollider.enabled = true;
                    remnant.transform.rotation = transform.rotation;
                    break;
                    
            }
            

        }
        */


        if (myZone != null) myZone.removeMinionFromZone(gameObject);

        if (isAlly)
        {
            Destroy(gameObject);
        }
        else
        {
            purify_self();
        }
        
    }

    public override void on_delta_clarity()
    {

        //healthBar.updateHealthBar(CL_MAX, CL);
    }

    public override void inflict_damage(float DC)
    {
        if (vulnerable)
        {
            CL -= DC;
            //if (CurrentTree == BehaviorTree.WorkTree) CurrentTree = BehaviorTree.lookaroundTree;
        }
        timeSinceDamage = 0;
    }

    public override void inflict_healing(float heal)
    {
        CL += heal;
    }

    public override void inflict_status(List<CBI_CombatMessenger.status> statuses)
    {
        //print("status inflicted");
        if (statuses == null) return;
        foreach (CBI_CombatMessenger.status stat in statuses)
        {
            float duration = CBI_CombatMessenger.statusCooldownMap[stat];
            bool makeNewStatus = true;
            foreach (Pair<CBI_CombatMessenger.status, float> curStat in StatusList)
            {
                if (stat == curStat.value1)
                {
                    curStat.value2 = duration;
                    makeNewStatus = false;
                    break;
                }
            }
            if (makeNewStatus)
            {
                StatusList.Add(new Pair<CBI_CombatMessenger.status, float>(stat, duration));
            }
        }
    }

    public override void attempt_purify_or_corrupt(float deltaPurity)
    {
        float baseTierOne = .25f;
        float baseTierTwo = .125f;
        if (entity_type == monsterTypes.confusionMinion || entity_type == monsterTypes.slowMinion || entity_type == monsterTypes.necroMinion)
        {
            deltaPurity *= baseTierTwo;
        }
        else if (entity_type == monsterTypes.treeMinion)
        {
            deltaPurity *= baseTierOne * 1.33f;
        }
        else if(entity_type == monsterTypes.flowerMinion)
        {
            deltaPurity *= baseTierOne * 0.66f;
        }
        else
        {
            deltaPurity *= baseTierOne;
        }

        purityValue += deltaPurity;

        // trigger sparkle effect
        //if(purifySparkle != null)
        purifySparkle.Emit(1);

        // Play enemy hit by purify
        FMODUnity.RuntimeManager.PlayOneShot("event:/enemy_hit_sound", transform.position);
        FMODUnity.RuntimeManager.PlayOneShot("event:/twig_purify_hit");

        // trigger hit marker
        UIX_HitMarker.registerHit();
    }

    public override void receive_combat_message(CBI_CombatMessenger combat_message)
    {
        base.receive_combat_message(combat_message);
        if (cleansed) return;
        if (isAlly)
        {
            if (combat_message.Damage_DC > 0)
            {
                //ally taking damage here
            }
        }
        if (!isAlly)
        {
            if (combat_message.Damage_DC >= 0 || combat_message.Status_list.Count > 0 || combat_message.Delta_Purity >= 0)
            {
                if(combat_message.Damage_DC > 0)
                {
                    //enemy taking damage here
                }

                //show health bar
                EnemyShowHealthTimer = maxEnemyShowHealthTimer;
                if (CurrentTree == BehaviorTree.WorkTree || CurrentTree == BehaviorTree.lookaroundTree)
                {
                    CurrentTree = BehaviorTree.lookaroundTree;
                    Vector3 searchDirection = combat_message.position - transform.position;
                    startLookaround(searchDirection);
                }
            }
        }
    }

    public void goToTargetCrystal()
    {
        if (crystal != null)
        {
            print(crystal.name);
            agent.SetDestination(crystal.transform.position);
        }
        else
        {
            print("No Crystal");
            agent.SetDestination(transform.position);
        }
    }

    //sets the crystal this minion is collecting from.
    private void setTargetCrystal(GameObject tc)
    {
        ENV_GroundCrystal gc = tc.GetComponent<ENV_GroundCrystal>();
        if (gc == null)
        {
            print("Cannot set " + tc.name + " as target crystal, its not a groundCrystal");
            return;
        }
        else if (_crystal == tc)
        {
            //wont do anything if the crystal is already targeted
            return;
        }
        //this increases the count of minionsCollecting if it is able as well as returning a value.
        else if (!gc.requestCrystalPickup())
        {
            print("Cannot set " + tc.name + " as target crystal, it already has too many minions collecting from it");
            return;
        }
        else if (_crystal != null)
        {
            _crystal.GetComponent<ENV_GroundCrystal>().releaseCrystalHold();
        }
        _crystal = tc;
    }

    public bool nearCrystal()
    {
        if (crystal == null)
        {
            return false;
            //no pathable crystal
        }
        return Vector3.Distance(crystal.transform.position, transform.position) < pickupRange;
    }

    public void pickupCrystal()
    {
        if (crystal == null) return;
        crystal.GetComponent<ENV_GroundCrystal>().takeCrystal(gameObject);
    }

    public GameObject getNearestAlly()
    {
        return allyManager.getNearestAlly(gameObject.transform.position);
    }

    public GameObject getNearestEnemy()
    {
        return allyManager.getNearestEnemy(gameObject.transform.position);
    }

    public virtual bool goToPosition(Vector3 position)
    {
        //Debug.Log("postion: "+position);
        if (agent.destination == position) return true;
        
        //agent.SamplePathPosition()
        bool result = agent.SetDestination(ValidifyPoint(position));

        return result;
    }


    protected void slowEffect()
    {
        agent.speed = slowSpeed;
        attackRechargeTime = slowAttackSpeed;
        if (_slowEffect == null)
            _slowEffect = UTL_Resources.cloneAsChild("FloatingUI/SlowEffect", gameObject);
        return;
    }

    protected void confusionEffect()
    {
        if (_confusionEffect == null)
        {
            _confusionEffect = UTL_Resources.cloneAsChild("FloatingUI/ConfusionEffect", gameObject);
            ConfusedPoints = setRandomPatrolPoints();
            CurrentTree = BehaviorTree.ConfusionTree;
        }
    }

    protected void endConfusionEffect()
    {
        CurrentTree = lastTree;
        Destroy(_confusionEffect);
        _confusionEffect = null;
    }

    protected void endSlowEffect()
    {
        Destroy(_slowEffect);
    }

    protected void applyStatusEffects()
    {
        removeList.Clear();
        foreach (Pair<CBI_CombatMessenger.status, float> statPair in StatusList)
        {
            if (statPair.value2 <= 0)
            {
                removeList.Add(statPair);
                //StatusList.Remove(statPair);
                continue;
            }
            statPair.value2 -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
            switch (statPair.value1)
            {
                case CBI_CombatMessenger.status.slow:
                    slowEffect();
                    break;
                case CBI_CombatMessenger.status.meleeSlow:
                    slowEffect();
                    break;
                case CBI_CombatMessenger.status.confusion:
                    confusionEffect();
                    break;
            }
        }
        foreach (Pair<CBI_CombatMessenger.status, float> statToRemove in removeList)
        {
            StatusList.Remove(statToRemove);
            if (statToRemove.value1 == CBI_CombatMessenger.status.confusion) endConfusionEffect();
            else if (statToRemove.value1 == CBI_CombatMessenger.status.meleeSlow) endSlowEffect();
            else if (statToRemove.value1 == CBI_CombatMessenger.status.slow) endSlowEffect();
        }
    }

    public void startRandomLookaround()
    {
        float startAngle = Random.Range(0, 355);
        Vector3 forwardVec = new Vector3(Mathf.Sin(Mathf.Deg2Rad * startAngle), 0, Mathf.Cos(Mathf.Deg2Rad * startAngle));
        startLookaround(forwardVec);
    }

    public void startLookaround(Vector3 forwardVec)
    {
        //USED TO ROTATATING MINION
        //lookaroundLeft = transform.right * -1;
        //lookaroundRight = transform.right;
        //print("left: " + lookaroundLeft + ", right: " + lookaroundRight);
        //finishedRotateLeft = false;

        //USED TO MOVE MINION IN SEARCH
        forwardVec = forwardVec.normalized;
        float startAngle = Vector3.Angle(Vector3.forward, forwardVec);
        int DISTTOSEARCH = 10;
        startAngle = startAngle + 120 % 360;
        Vector3 rightvec = new Vector3(Mathf.Sin(Mathf.Deg2Rad * startAngle), 0, Mathf.Cos(Mathf.Deg2Rad * startAngle));
        startAngle = startAngle + 120 % 360;
        Vector3 leftVec = new Vector3(Mathf.Sin(Mathf.Deg2Rad * startAngle), 0, Mathf.Cos(Mathf.Deg2Rad * startAngle));

        searchForward = getValidPath(UTL_Dungeon.snapPositionToTerrain(transform.position + (DISTTOSEARCH * forwardVec)));
        searchLeft = getValidPath(UTL_Dungeon.snapPositionToTerrain(transform.position + (DISTTOSEARCH * leftVec)));
        searchRight = getValidPath(UTL_Dungeon.snapPositionToTerrain(transform.position + (DISTTOSEARCH * rightvec)));
        startSearch = true;
        searchTimer = SEARCH_TIME;
        //print("Search left: " + searchLeft + ", Search right: " + searchRight);
        finishedSearchLeft = false;
        finishedSearchForward = false;
    }

    public void rotate(bool rotateLeft, float speed = 10f)
    {
        //CHANGE SPEED, DOES NOTHING
        if (rotateLeft)
        {
            gameObject.transform.Rotate(Vector3.down * speed, Space.Self);
            //gameObject.transform.Rotate(Vector3.up * speed, Space.Self);
        }
        else
        {
            gameObject.transform.Rotate(Vector3.up * speed, Space.Self);
        }
    }

    public void dontMove()
    {
        endPoint = gameObject.transform.position;
        gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
    }

    public void ghostify()
    {
        _ghost = true;
        //GameObject crystals = transform.GetChild(1).gameObject;
        //if(crystals != null) Destroy(crystals);
        //if (rend == null) rend = GetComponentInChildren<Renderer>();
        SkinnedMeshRenderer rend = GetComponentInChildren<SkinnedMeshRenderer>();
        rend.material = ghostMat;
        //print("set material to ghostmat");
        //GameObject ghostEffect = UTL_Resources.cloneAsChild("Particles/GhostEffect", gameObject);
        //PSMeshRendererUpdater effectScript = ghostEffect.GetComponent<PSMeshRendererUpdater>();
        //effectScript.MeshObject = gameObject;
        //effectScript.UpdateMeshEffect();
        CL = CL_MAX / 2;
    }


    public void searchingLeft()
    {
        searchLeft = UTL_Dungeon.snapPositionToTerrain(getValidPath(searchLeft));
        goToPosition(searchLeft);
    }

    public void searchingRight()
    {
        searchRight = UTL_Dungeon.snapPositionToTerrain(getValidPath(searchRight));
        goToPosition(searchRight);
    }

    public void searchingForward()
    {
        searchForward = UTL_Dungeon.snapPositionToTerrain(getValidPath(searchForward));
        goToPosition(searchForward);
    }

    public void setValidPath(Vector3 pathEnd)
    {
        int navmeshMask = 1 << NavMesh.GetAreaFromName("Walkable");
        NavMeshHit hit;
        Vector3 vec = pathEnd - transform.position;
        float dist = 2 * vec.magnitude;
        do
        {
            dist /= 2;
            vec = vec.normalized * dist;
            endPoint = transform.position + vec;
        }
        while (agent.SamplePathPosition(navmeshMask, dist, out hit));
    }

    public Vector3 getValidPath(Vector3 pathEnd)
    {
        int navmeshMask = 1 << NavMesh.GetAreaFromName("Walkable");
        NavMeshHit hit;
        Vector3 vec = pathEnd - transform.position;
        Vector3 result;
        float dist = 2 * vec.magnitude;
        do
        {
            dist /= 2;
            vec = vec.normalized * dist;
            result = transform.position + vec;
        }
        while (agent.SamplePathPosition(navmeshMask, dist, out hit));
        return result;
    }

    public virtual void lookTowards(Vector3 lookAt)
    {

        if (Vector3.Angle(transform.forward, lookAt - transform.position) <= 5)
        {
            shouldRotate = false;
            return;
        }

        Vector3 direction = (lookAt - transform.position).normalized;
        if (direction != Vector3.zero)
        {
            Quaternion lookRotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, TME_Manager.getDeltaTime(TME_Time.type.enemies) * 2);
        }

        //myFollowDistance = Vector3.Distance(player.transform.position, transform.position + lookDir);
        //transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(lookDir), 0.1F);
    }

    //makes point into one that is pathable.
    public Vector3 ValidifyPoint(Vector3 vec)
    {
        int navMask = 1 << NavMesh.GetAreaFromName("Walkable");
        NavMeshHit hit;
        vec = UTL_Dungeon.snapPositionToTerrain(vec);
        if (NavMesh.SamplePosition(vec, out hit, 5, navMask))
        {
            vec = hit.position;
        }
        return vec;
    }

    //makes point into one that is pathable.
    //pass in an out bool to check that the new point is in fact pathable.
    public Vector3 ValidifyPoint(Vector3 vec, out bool pointIsValid, float distanceToCheck = 2.5F)
    {
        int navMask = 1 << NavMesh.GetAreaFromName("Walkable");
        NavMeshHit hit;
        vec = UTL_Dungeon.snapPositionToTerrain(vec);
        pointIsValid = NavMesh.SamplePosition(vec, out hit, distanceToCheck, navMask);
        if (pointIsValid)
        {
            vec = hit.position;
        }
        return vec;
    }

    public void SetEnabled(bool enable)
    {
        AI.enabled = enable;
        agent.enabled = enable;
        this.enabled = enable;
    }

    public void SetPSEnabled(bool enable)
    {
        if (VFX == null) return;
        foreach (ParticleSystem effect in VFX)
        {
            if (effect == null) continue;
            if (enable)
            {
                effect.Play();
            }
            else
            {
                effect.Stop();
            }
        }
    }

    public void ShowMinion(bool show)
    {
        gameObject.SetActive(show);

    }

    public void setPurityBar(float value)
    {
        //not yet implemented
        return;
    }

    //handles purification
    public void OnParticleCollision(GameObject other)
    {
        //REMOVED TO TAKE OUT PURIFICATION DAMAGE
        /*
        if (isAlly) return;
        ParticleSystem ps = other.GetComponent<ParticleSystem>();
        if (other.tag == "purityEffect")
        {
            PUR_PurityEffect purityEffect = other.GetComponent<PUR_PurityEffect>();
            if (purityEffect != null)
            {
                this.receive_combat_message(new CBI_CombatMessenger(0, 0, purityEffect.purityDamage, new List<CBI_CombatMessenger.status>(),other.transform.position));
            }
        }
        */
    }

    public void teleport(Vector3 newPosition)
    {
        //stop
        if(agent.isActiveAndEnabled)
            agent.ResetPath();

        //agent.enabled = false;
        AI.enabled = false;
        col.enabled = false;
        //GetComponent<Rigidbody>().isKinematic = true;

        //dissapear
        anim.SetBool("isAppear", false);

        reappear = true;
        teleportationLocation = newPosition;
        this.enabled = false;
        /*
        //set new teleportation position
        transform.position = newPosition;

        //appear
        anim.SetBool("isAppear", true);

        //renable movement
        agent.enabled = true;
        AI.enabled = true;
        goToPosition(newPosition);
        */
    }

    public bool isSlowed()
    {
        foreach (Pair<CBI_CombatMessenger.status, float> stat in StatusList)
        {
            if (stat.value1 == CBI_CombatMessenger.status.slow) return true;
            else if (stat.value1 == CBI_CombatMessenger.status.meleeSlow) return true;
        }
        return false;
    }

    public bool isConfused()
    {
        foreach (Pair<CBI_CombatMessenger.status, float> stat in StatusList)
        {
            if (stat.value1 == CBI_CombatMessenger.status.confusion) return true;
        }
        return false;
    }

    public bool isOnCorruption()
    {
        return MAP_NatureMap.natureTypeAtPos(transform.position) == MAP_NatureData.type.corrupt;
    }

    public bool isOnGrass()
    {
        return MAP_NatureMap.natureTypeAtPos(transform.position) == MAP_NatureData.type.grass;
    }

    public void DebugPath()
    {
        for (int i = 0; i < agent.path.corners.Length - 1; i++)
        {
            Debug.DrawLine(agent.path.corners[i], agent.path.corners[i + 1], Color.red);
        }
    }

    GameObject debugSphere; 
    public void DebugGoToPos()
    {
        print(gameObject.name + " " + myGoToPos);
    }
}