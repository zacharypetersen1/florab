﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_Remnant : MonoBehaviour {
    public ENT_DungeonEntity.monsterTypes entity_type;
    float remnantTimer = 500F;
    List<List<GameObject>> ListsImIn = new List<List<GameObject>>();
    List<List<GameObject>> removeList = new List<List<GameObject>>();
    bool watered = false;
    float growthScalar = 1.01f;

    // Use this for initialization
    void Start () {
		
	}

    public void OnParticleCollision(GameObject other)
    {
        if(other.tag == "purityEffect")
        {
            watered = true;
            StartCoroutine(WaitToRevive(2f));
        }
    }

    IEnumerator WaitToRevive(float timer)
    {
        yield return new WaitForSeconds(timer);
        spawnMinion();
        Destroy(gameObject);
    }

    void grow()
    {
        transform.localScale *= (growthScalar);
    }

    void spawnMinion()
    {
        UTL_Dungeon.spawnMinion(entity_type,transform.position,true);
    }

    // Update is called once per frame
    void Update () {
        if (watered)
        {
            grow();
        }
        /*
        remnantTimer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
		if(remnantTimer <= 0)
        {
            Destroy(gameObject);
        }
        */
	}

    public void revive(bool asAlly, GameObject necromancer)
    {
        GameObject resurrectEffect;
        if (asAlly)
        {
            resurrectEffect = UTL_Resources.cloneAtLocation("Particles/ally_Resurrect", transform.position);
        }
        else
        {
            resurrectEffect = UTL_Resources.cloneAtLocation("Particles/enemy_Resurrect", transform.position);
        }
             
        ENT_ResurrectParticleEffect effect = resurrectEffect.GetComponent<ENT_ResurrectParticleEffect>();
        necromancer.GetComponent<ENT_NecroMinion>().myResurectAbilities.Add(effect);
        effect.myNecromancer = necromancer;
        effect.myRemnant = gameObject;

        Quaternion rot = resurrectEffect.transform.rotation;
        rot.eulerAngles = new Vector3(90, 0, 0);
        resurrectEffect.transform.rotation = rot;
        effect.entity_type = entity_type;
        effect.asAlly = asAlly;
    }

    public void addToList(List<GameObject> newList)
    {
        if (!ListsImIn.Contains(newList))
        {
            newList.Add(gameObject);
            ListsImIn.Add(newList);
        }
    }

    public void removeFromList(List<GameObject> newList)
    {
        if (ListsImIn.Contains(newList))
        {
            newList.Remove(gameObject);
            ListsImIn.Remove(newList);
        }
    }

    public void removeFromAllListsExcept(List<GameObject> onlyList)
    {
        foreach (List<GameObject> list in ListsImIn)
        {
            if (list != onlyList)
            {
                removeList.Add(list);
            }
        }
        foreach (List<GameObject> toRemove in removeList)
        {
            toRemove.Remove(gameObject);
            ListsImIn.Remove(toRemove);
        }
    }

    void OnDestroy()
    {
        List<List<GameObject>> removeList = new List<List<GameObject>>(ListsImIn);
        foreach (var toRemove in removeList)
        {
            removeFromList(toRemove);
        }
    }
}
