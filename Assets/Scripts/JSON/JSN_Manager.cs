﻿//Eliezer Miron
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class JSN_Manager : MonoBehaviour {
    static string json_path;
    
    // Use this for initialization
    void Awake ()
    {
        json_path = Application.dataPath + "//Scripts//JSON//";
        // LoadAllJSON(json_path); //may be implemented in the future
    }
	
	// Update is called once per frame
	void Update ()
    {

    }

    /// <summary>
    /// WIP/TBI/maybe not ever
    /// Goes through all JSON files and builds a dictionary for later reference out of 
    /// deserialized JSON objects. 
    /// </summary>
    /// <param name="path"></param>
    void LoadAllJSON(string path)
    {


    }


    //
    // Gets data from dictionary for the requested object and reads that data directly into obj's fields
    //
    static public void getStats(object obj, string objName, string level)
    {
        string json_string = File.ReadAllText(json_path+objName+"//"+level+".JSON"); //Path for json files
        JsonUtility.FromJsonOverwrite(json_string, obj); //Overwrites the stats that are in json_string on the object.
    }
}
