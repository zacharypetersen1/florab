﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Harold
//should be attached to player gameObject

public class COM_AllyCommands : MonoBehaviour {

    protected PLY_AllyManager am;
    protected List<GameObject> removeList = new List<GameObject>();
	// Use this for initialization
	void Awake () {
        am = GetComponent<PLY_AllyManager>();
	}

    List<GameObject> removeIndisposedMinions(List<GameObject> allies)
    {
        removeList.Clear();
        foreach(GameObject ally in allies)
        {
            if (ally.GetComponent<ENT_Body>().CurrentTree == ENT_Body.BehaviorTree.ConfusionTree)
            {
                removeList.Add(ally);
            }
        }
        foreach (GameObject toRemove in removeList)
        {
            allies.Remove(toRemove);
        }
        return allies;
    }

    #region list of minion commands
    public void FollowCommand(List<GameObject> allies)
    {
        allies = removeIndisposedMinions(allies);
        if (allies.Count == 0) return;
        foreach (GameObject ally in allies)
        {
            ENT_Body body = ally.GetComponent<ENT_Body>();
            if (body.commandGroup != null) body.commandGroup.removeFromList(ally);
            FollowCommand(ally);
            am.followGroup.addToList(ally);
            body.commandGroup = am.followGroup;
            //print("Added " + body.name + " to followGroup");
        }
        if (!SelectionManager.isInSelectMode) am.lastCommandIssued = am.followGroup;
    }

    public void WaitCommand(List<GameObject> allies)
    {
        allies = removeIndisposedMinions(allies);
        if (allies.Count == 0) return;
        AIC_AllyGroup waitGroup = new AIC_AllyGroup(PLY_AllyManager.allyState.wait, allies, am, am.transform.position);
        am.commandGroups.Add(waitGroup);
        foreach (GameObject ally in allies)
        {
            ENT_Body body = ally.GetComponent<ENT_Body>();
            if (body.commandGroup != null) body.commandGroup.removeFromList(ally);
            WaitCommand(ally);
            body.commandGroup = waitGroup;
        }
        if (!SelectionManager.isInSelectMode) am.lastCommandIssued = waitGroup;
    }

    public void GoToCommand(List<GameObject> allies, Vector3 goToPos)
    {
        allies = removeIndisposedMinions(allies);
        if (allies.Count == 0) return;
        AIC_AllyGroup goToGroup = new AIC_AllyGroup(PLY_AllyManager.allyState.goTo, allies, am, goToPos);
        am.commandGroups.Add(goToGroup);
        foreach (GameObject ally in allies)
        {
            ENT_Body body = ally.GetComponent<ENT_Body>();
            if (body.commandGroup != null) body.commandGroup.removeFromList(ally);
            GoToCommand(ally, goToPos);
            body.commandGroup = goToGroup;
        }
        goToGroup.assignGoToPoints();
        if (!SelectionManager.isInSelectMode) am.lastCommandIssued = goToGroup;
    }

    public void AttackCommand(List<GameObject> allies, GameObject target)
    {
        if (target == null)
        {
            Debug.LogError("Error: tried to issue attack command with null target");
            return;
        }
        allies = removeIndisposedMinions(allies);
        if (allies.Count == 0) return;
        AIC_AllyGroup attackGroup = new AIC_AllyGroup(PLY_AllyManager.allyState.attack, allies, am, target.transform.position, target);
        am.commandGroups.Add(attackGroup);
        foreach (GameObject ally in allies)
        {
            ENT_Body body = ally.GetComponent<ENT_Body>();
            if (body.commandGroup != null) body.commandGroup.removeFromList(ally);
            AttackCommand(ally, target);
            body.commandGroup = attackGroup;
        }
        if (!SelectionManager.isInSelectMode) am.lastCommandIssued = attackGroup;
    }

    public void ActivateCommand(List<GameObject> allies)
    {
        allies = removeIndisposedMinions(allies);

        if (allies.Count == 0) return;
        AIC_AllyGroup activateGroup = new AIC_AllyGroup(PLY_AllyManager.allyState.activate, allies, am, Vector3.zero);
        am.commandGroups.Add(activateGroup);
        foreach (GameObject ally in allies)
        {
            ENT_Body body = ally.GetComponent<ENT_Body>();
            body.lastGroup = body.commandGroup;
            if (body.commandGroup != null) body.commandGroup.removeFromList(ally);
            ActivateCommand(ally);
            body.commandGroup = activateGroup;
        }
    }

    #endregion
    #region single minion commands
    void ActivateCommand(GameObject ally)
    {
        ENT_Body body = ally.GetComponent<ENT_Body>();
        if (body == null)
        {
            Debug.LogWarning("cannot run Activate command on " + ally.name + " because it does not have a body component");
            return;
        }
        if (!body.Purity)
        {
            Debug.LogWarning("cannot run Activate command on " + ally.name + " because it is not an ally");
            return;
        }
        am.setState(ally, PLY_AllyManager.allyState.activate);
    }

    void FollowCommand(GameObject ally)
    {
        ENT_Body body = ally.GetComponent<ENT_Body>();
        if (body == null)
        {
            Debug.LogWarning("cannot run Follow command on " + ally.name + " because it does not have a body component");
            return;
        }
        if (!body.Purity)
        {
            Debug.LogWarning("cannot run Follow command on " + ally.name + " because it is not an ally");
            return;
        }
        am.setState(ally, PLY_AllyManager.allyState.follow);
    }

    void WaitCommand(GameObject ally)
    {
        ENT_Body body = ally.GetComponent<ENT_Body>();
        if (body == null)
        {
            Debug.LogWarning("cannot run Wait command on " + ally.name + " because it does not have a body component");
            return;
        }
        if (!body.Purity)
        {
            Debug.LogWarning("cannot run Wait command on " + ally.name + " because it is not an ally");
            return;
        }
        am.setState(ally, PLY_AllyManager.allyState.wait);
    }

    void AttackCommand(GameObject ally, GameObject target)
    {
        if (target == null)
        {
            Debug.LogWarning("target to attack cannot be null");
            return;
        }
        ENT_Body body = ally.GetComponent<ENT_Body>();
        if (body == null)
        {
            Debug.LogWarning("cannot run Attack command on " + ally.name + " because it does not have a body component");
            return;
        }
        if (!body.Purity)
        {
            Debug.LogWarning("cannot run Attack command on " + ally.name + " because it is not an ally");
            return;
        }
        am.setState(ally, PLY_AllyManager.allyState.attack, target);
    }

    void GoToCommand(GameObject ally, Vector3 goToPos)
    {
        ENT_Body body = ally.GetComponent<ENT_Body>();
        if (body == null)
        {
            Debug.LogWarning("cannot run Go To command on " + ally.name + " because it does not have a body component");
            return;
        }
        if (!body.Purity)
        {
            Debug.LogWarning("cannot run Go To command on " + ally.name + " because it is not an ally");
            return;
        }
        am.setState(ally, PLY_AllyManager.allyState.goTo, goToPos);
    }
    #endregion
}
