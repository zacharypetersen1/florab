﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TUT_TutorialManager : MonoBehaviour {

    public static Image dialogueBox;
    public static TextMeshProUGUI dialogueNameText;
    public static TextMeshProUGUI text;
    public static Image mouseImage;
    public static float displayTime = 1f;
    public static float timer = 0f;
    private static bool appearing = false;
    private static float alpha = 0f;
    public static float maxAlpha = 0.9f;
    private static Color tempColor = new Color(1f, 1f, 1f, 1f);
    public static float fadeInSpeedupFactor = 1.5F; //how many times faster the text should fade in
    public static float speedScalar = 3F;
    //private static bool readyForMessage = true;
    private static List<TUT_Messages.Messages> messageQueue = new List<TUT_Messages.Messages>();
    private static TUT_Messages.Messages currMessage = TUT_Messages.Messages.None;
    public static bool isMessagePlaying
    {
        get { return currMessage != TUT_Messages.Messages.None; }
    }

      
	// Use this for initialization
	void Start () {
        dialogueBox = GameObject.Find("DialogueBox").GetComponent<Image>();
        dialogueNameText = GameObject.Find("DialogueName").GetComponent<TextMeshProUGUI>();
        text = GameObject.Find("TutorialText").GetComponent<TextMeshProUGUI>();
        mouseImage = GameObject.Find("MouseIndicator").GetComponent<Image>();
        setFade(alpha);
        setDialogueVisible(false);
        queueMessage(TUT_Messages.Messages.Hello);
    }
	
	// Update is called once per frame
	void Update () {
        if (appearing)
        {
            alpha = Mathf.Clamp(alpha + (TME_Manager.getDeltaTime(TME_Time.type.master) * speedScalar * fadeInSpeedupFactor), 0, maxAlpha);
        }
        else
        {
            alpha = Mathf.Clamp(alpha - (TME_Manager.getDeltaTime(TME_Time.type.master) * speedScalar), 0, maxAlpha);
        }
        /*if(alpha <= 0 && ! appearing)
        {
            endMessage();
        }*/
        setFade(alpha);
        timer = Mathf.Clamp(timer - TME_Manager.getDeltaTime(TME_Time.type.master), 0, displayTime);
        if (timer <= 0 && isMessagePlaying)
        {
            //setDialogueVisible(false);
            setMouseButtonVisible(true);
        }
        else
        {
            setMouseButtonVisible(false);
        }
        if (Input.GetMouseButtonDown(0) && isMessagePlaying && timer <= 0)
        {
            //print("should end");
            endMessage();
        }
	}

    private static void endMessage()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/woodAccept");
        currMessage = TUT_Messages.Messages.None;
        setDialogueVisible(false);
        setMouseButtonVisible(false);
        if (messageQueue.Count > 0)
        {
            TUT_Messages.Messages nextMessage = messageQueue[0];
            messageQueue.RemoveAt(0);
            playMessage(nextMessage);
        }
    }

    public static void queueMessage(TUT_Messages.Messages tutorialMessage)
    {
        /*if (currMessage == tutorialMessage)
        {
            timer = displayTime;
        }
        else*/ if (currMessage == TUT_Messages.Messages.None)
        {
            playMessage(tutorialMessage);
        }
        else if (!messageQueue.Contains(tutorialMessage))
        {
            messageQueue.Add(tutorialMessage);
        }
    }

    public static void resetMessageTimer(TUT_Messages.Messages tutMessage)
    {
        if (currMessage == tutMessage)
        {
            timer = displayTime;
        }
    }

    private static void playMessage(TUT_Messages.Messages msg)
    {
        currMessage = msg;
        setDialogueVisible(true);
        string messageText = TUT_Messages.getMessageText(msg);
        //print("Displaying: " + messageText);
        text.SetText(messageText);
        timer = displayTime;
    }

    public static void setDialogueVisible(bool enable)
    {
        appearing = enable;
    }

    private static void setFade(float alpha)
    {
        tempColor = dialogueBox.color;
        tempColor.a = alpha;
        dialogueBox.color = tempColor;
        tempColor = dialogueNameText.color;
        tempColor.a = alpha;
        dialogueNameText.color = tempColor;
        tempColor = text.color;
        tempColor.a = alpha;
        text.color = tempColor;
        tempColor = mouseImage.color;
        tempColor.a = alpha;
        mouseImage.color = tempColor;
    }
    private static void setMouseButtonVisible(bool visible)
    {
        tempColor = mouseImage.color;
        if (visible)
        {
            tempColor.a = 1f;
        }
        else
        {
            tempColor.a = 0f;
        }
        mouseImage.color = tempColor;
    }
}
