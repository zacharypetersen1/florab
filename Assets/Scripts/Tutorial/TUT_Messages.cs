﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TUT_Messages {

	public enum Messages {None, Hello, WASD, Jump, DoubleJump, FirstMinion, Q, GoTo, YellowGrass, Purification,
                          Structures, RedGrass, Shift, ShieldGenerator, CommandMode, CommandSelect, Splicer, Recipe, Abilties,
                          SurfJump, ConfusionMinion, Necrominion, Corruption, StoryPiece, FoundGrandGuardians,
                          SlowMinion};

    static Dictionary<Messages, string> messageDict = new Dictionary<Messages, string>()
    {
        {Messages.None, "" },
        {Messages.Hello, "Hi I'm Red, thanks for picking me up. Can you help me find my parents?..." },
        {Messages.WASD, "Use the mouse and WASD to explore the world." },
        {Messages.Jump, "Press Spacebar to jump across these plateaus." },
        {Messages.DoubleJump, "Press Spacebar twice to double jump." },
        {Messages.FirstMinion, "This flora is friendly. Right click on a crystal to tell your flora to attack it." },
        {Messages.Q, "Press Q to command your flora to follow you." },
        {Messages.GoTo, "Right click the ground to command your flora to travel to that position." },
        {Messages.YellowGrass, "Hey, you found some yellow grass! Standing in this will charge up your purification ability." },
        {Messages.Purification, "Hold left click to purify corrupted flora into allies. We're going to need more friends on our journey!" },
        {Messages.Structures, "Right click on an enemy structure to command your flora to attack it." },
        {Messages.RedGrass, "Woah, that was a lot of enemies! If you get hurt, you can heal yourself by standing in red grass." },
        {Messages.Shift, "Hold Shift to use your Vineweaver powers to move swiftly through the world." },
        {Messages.ShieldGenerator, "This crystal is shielded by some magical force! Follow the arc to find the shield generator." },
        {Messages.CommandMode, "Scroll the mouse out to enter Command Mode. Left click and drag in Command Mode to select allies." },
        {Messages.Splicer, "Splicers can combine two weaker flora into stronger ones with powerful abilites." },
        {Messages.Recipe, "Check nearby signs for the Splicer's recipe." },
        {Messages.Abilties, "Press the number key that matches the number above allied flora to activate their abilities." },
        {Messages.SurfJump, "While holding shift, press space to jump over large areas." },
        {Messages.Necrominion, "The powerful Necromushroom flora's ability temporarily turns nearby remnants into ghostly allies." },
        {Messages.ConfusionMinion, "The Bellflower flora's confusion ability causes enemies to wander aimlessly for some time." },
        {Messages.Corruption, "Watch out for the corrupted ground! It will injure you." },
        {Messages.StoryPiece, "It looks like you found a page out of someone's diary! Press M to read it." },
        {Messages.FoundGrandGuardians, "Hooray, you found my parents! It looks like they're trapped behind another shield though..." },
        {Messages.SlowMinion, "The Sundew flora can use its ability to lay sticky traps that hinder enemies." }
    };

    public static string getMessageText(Messages message)
    {
        return messageDict[message];
    }
}
