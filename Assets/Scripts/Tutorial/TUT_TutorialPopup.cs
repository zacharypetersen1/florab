﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TUT_TutorialPopup : MonoBehaviour {

    public TUT_Messages.Messages tutorialMessage;
    public bool repeat = true;
    private bool popupShown = false;

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (repeat || !popupShown)
            {
                TUT_TutorialManager.queueMessage(tutorialMessage);
                popupShown = true;
            }
            /*else
            {
                TUT_TutorialManager.resetMessageTimer(tutorialMessage);
            }*/
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (!repeat && popupShown)
            {
                Destroy(gameObject);
            }
        }
    }
}
