﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TME_Manager : MonoBehaviour {



    Dictionary<TME_Time.type, TME_Time> times = new Dictionary<TME_Time.type, TME_Time>();
    private static TME_Manager singleton;

    // FOR DEBUGGING
    public bool useDebugTimes = false;
    public float masterTime = 1;
    public float gameTime = 1;
    public float playerTime = 1;
    public float enemiesTime = 1;
    public float environmentTime = 1;
    public Material[] timeEffectedMats;
    //


	// Use this for initialization
	void Awake () {
        singleton = this;
        times.Add(TME_Time.type.master, new TME_MasterTime());
        times.Add(TME_Time.type.game, new TME_ChildTime(times[TME_Time.type.master]));
        times.Add(TME_Time.type.player, new TME_ChildTime(times[TME_Time.type.game]));
        times.Add(TME_Time.type.enemies, new TME_ChildTime(times[TME_Time.type.game]));
        times.Add(TME_Time.type.environment, new TME_ChildTime(times[TME_Time.type.game]));

        foreach(var mat in timeEffectedMats)
        {
            mat.SetFloat("_GameTime", Time.time);
        }
    }



    // Update is called once per frame
    void Update () {
        
        // FOR DEBUGGING
        if (useDebugTimes)
        {
            setTimeScalar(TME_Time.type.master, masterTime);
            setTimeScalar(TME_Time.type.game, gameTime);
            setTimeScalar(TME_Time.type.player, playerTime);
            setTimeScalar(TME_Time.type.enemies, enemiesTime);
            setTimeScalar(TME_Time.type.environment, environmentTime);
        }
        //

        foreach (var mat in timeEffectedMats)
        {
            mat.SetFloat("_GameTime", Time.time);
        }
    }



    //
    // Returns delta time based on type of time
    //
    public static float getDeltaTime(TME_Time.type type)
    {
        return singleton.times[type].getDeltaTime();
    }



    //
    // Returns time scalar based on type of time
    //
    public static float getTimeScalar(TME_Time.type type)
    {
        return singleton.times[type].getTimeScalar();
    }



    //
    // Sets the time scalar for a type of time
    //
    public static void setTimeScalar(TME_Time.type type, float setScalar)
    {
        singleton.times[type].setTimeScalar(setScalar);

        //DEBUGGING PURPOSES
        switch (type)
        {
            case TME_Time.type.master: singleton.masterTime = setScalar; break;
            case TME_Time.type.game: singleton.gameTime = setScalar; break;
            case TME_Time.type.enemies: singleton.enemiesTime = setScalar; break;
            case TME_Time.type.player: singleton.playerTime = setScalar; break;
            case TME_Time.type.environment: singleton.environmentTime = setScalar; break;
        }
        //
    }
}
