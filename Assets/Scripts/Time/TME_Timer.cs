﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TME_Timer : MonoBehaviour {



    public static TME_Timer singleton;
    private Dictionary<string, Timer> timers;



    //
    // contains a single timer
    //
    private class Timer
    {
        TME_Time.type timeType;
        float curTime = 0;

        // constructor
        public Timer(TME_Time.type setTimeType)
        {
            timeType = setTimeType;
        }

        // checks if there is no time left on timer
        public bool timeIsUp()
        {
            return curTime == 0;
        }

        // returns amount of time left on timer
        public float getTime()
        {
            return curTime;
        }

        // sets the time
        public void setTime(float time)
        {
            curTime = time;
        }

        // updates the timer
        public void update()
        {
            // only need to decriment if time is not up
            if(!timeIsUp())
            {
                // decriment and make sure that we don't go below 0
                curTime -= TME_Manager.getDeltaTime(timeType);
                if (curTime < 0)
                {
                    curTime = 0;
                }
            }
        }
    }



    //
    // Runs when object is first created
    //
    void Awake()
    {
        singleton = this;
        timers = new Dictionary<string, Timer>();
    }



    //
    // Runs once per frame
    //
    void Update()
    {
        // loop through each timer and update it
        foreach(var timer in timers.Values)
        {
            timer.update();
        }
    }



    //
    // Adds a new timer
    //
    public static void addTimer(string timerName, TME_Time.type timeType)
    {
        singleton.timers.Add(timerName, new Timer(timeType));
    }



    //
    // Returns amount of time left on timer
    //
    public static float getTime(string timerName)
    {
        return singleton.timers[timerName].getTime();
    }



    //
    // Sets amount of time left on timer
    //
    public static void setTime(string timerName, float time)
    {
        singleton.timers[timerName].setTime(time);
    }



    //
    // Resets the timer to 0
    //
    public static void resetTimer(string timerName)
    {
        setTime(timerName, 0);
    }



    //
    // Checks if time is up for specified timer
    //
    public static bool timeIsUp(string timerName)
    {
        return singleton.timers[timerName].timeIsUp();
    }
}
