﻿// Matt

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFX_Twig : SFX_System {

    public float walkThreshold = 1.2f;      // threshold determining whether Twig is considered walking or running
    public AudioClip footstepGrass;         // Twig's audio clips
    public AudioClip footstepRock;

    //
    // Play footstep audio at walking speed
    //
    void footstepWalk()
    {
        if (velocity < walkThreshold)
        {
            // Play footstep sound depending on terrain type
            if (MAP_NatureMap.natureTypeAtPos(transform.position) == MAP_NatureData.type.grass)
            {
                source.clip = footstepGrass;
                source.Play();
            } else if(MAP_NatureMap.natureTypeAtPos(transform.position) == MAP_NatureData.type.rock)
            {
                source.clip = footstepRock;
                source.Play();
            }
        }
    }

    //
    // Play footstep audio at running speed
    //
    void footstepRun()
    {
        if (velocity >= walkThreshold)
        {
            // Play footstep sound depending on terrain type
            if (MAP_NatureMap.natureTypeAtPos(transform.position) == MAP_NatureData.type.grass)
            {
                source.clip = footstepGrass;
                source.Play();
            }
            else if (MAP_NatureMap.natureTypeAtPos(transform.position) == MAP_NatureData.type.rock)
            {
                source.clip = footstepRock;
                source.Play();
            }
        }
    }

    void footstepTwig()
    {
        // Play footstep sound depending on terrain type
        if (MAP_NatureMap.natureTypeAtPos(transform.position) == MAP_NatureData.type.grass)
        {

        }
        else if (MAP_NatureMap.natureTypeAtPos(transform.position) == MAP_NatureData.type.rock)
        {

        }
        else if (MAP_NatureMap.natureTypeAtPos(transform.position) == MAP_NatureData.type.corrupt)
        {

        }
    }
}
