﻿// Zach

using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

public class INP_PlayerInput : MonoBehaviour
{



    static float deadZone = 0.3f;                       // analog magnitude for axis must be greater than this value to be detected
    static float axisAsButtonTHold = 0.5f;              // threshold past which axis is considered to be "Held Down"
    static InputType curentInput = InputType.keyboard;   // currectly active input device


    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }


    //
    // struct for all types of input devices
    //
    public enum InputType
    {
        xBox360, keyboard
    }



    //
    // struct for containing all maps correlating to one type of input device
    //
    struct InputMap
    {
        public Dictionary<string, string[]> axis_map;
        public Dictionary<string, string[]> button_map;
    }



    //
    // If axis is used like button, must keep track of value on previous frame here
    //
    static Dictionary<string, float> axis_history = new Dictionary<string, float>()
    {
        { "LTRI", 0 },
        { "RTRI", 0 }
    };



    //
    // maps input name to boolean indicating if it's an axis input
    //
    static Dictionary<string, bool> is_axis = new Dictionary<string, bool>()
    {
        { "LTRI", true },
        { "RTRI", true },
        { "LeftBumper", false },
        { "RightBumper", false },
        { "RightClick", false },
        { "Start_Button", false },
        { "A"   , false },
        { "X"   , false },
        { "B"   , false },
        { "Y"   , false },
        { "KEY_Shift" , false },
        { "KEY_Space" , false },
        { "KEY_Escape" , false },
        { "KEY_1" , false },
        { "KEY_2" , false },
        { "KEY_3" , false },
        { "KEY_4" , false },
        { "KEY_H" , false },
        { "KEY_K" , false },
        { "KEY_L" , false },
        { "KEY_J" , false },
        { "KEY_I" , false },
        { "KEY_R" , false },
        { "KEY_B" , false },
        { "KEY_Q" , false },
        { "KEY_Z" , false },
        { "KEY_E" , false },
        { "KEY_U" , false },
        { "KEY_M" , false },
        { "JSLX"  , true },
        { "JSLY"  , true },
        { "JSRX"  , true },
        { "JSRY"  , true },
        { "KEY_AD", true },
        { "KEY_WS", true },
        { "KEY_FH", true },
        { "KEY_TG", true },
        { "MOUSEX", true },
        { "MOUSEY", true },
        { "MOUSE0", false },
        { "MOUSE1", false },
        { "MOUSE2", false },
        { "KEY_Alt", false },
        { "KEY_Ctrl", false },
        { "KEY_F", false },
        { "KEY_Tab", false }
    };



    //
    // maps input type to correspoinding input map
    //
    static private Dictionary<InputType, InputMap> inputMaps = new Dictionary<InputType, InputMap>
    {
        /*{InputType.xBox360,

            // Map of all controls for the xbox 360 controller
            new InputMap
            {
                // map for axis
                axis_map = new Dictionary<string, string[]>()
                {
                    {"Movement",    new string[] { "JSLX", "JSLY" } },
                    {"Cursor",      new string[] { "JSLX", "JSLY" } },
                    {"Abilities",   new string[] { "LTRI", "RTRI" } },
                    {"AbilityAim",  new string[] { "JSRX", "JSRY" } },
                    {"Camera",      new string[] { "JSRX", "JSRY" } }
                },

                // map for buttons
                button_map = new Dictionary<string, string[]>()
                {
                    {"Ability0",     new string[] { "LTRI"} },
                    {"Ability1",     new string[] { "RTRI"} },
                    {"Ability2",     new string[] { "RightBumper" } },
                    {"Ability3",     new string[] { "Y"} },
                    {"Ability4",     new string[] { "X"} },
                    {"Ability5",     new string[] { "B"} },
                    {"Ability6",     new string[] { "LeftBumper"} },
                    {"Interact",     new string[] { "A" } },
                    {"ToggleCamera", new string[] { "RightClick" } },
                    {"Pause",        new string[] { "Start_Button"} }
                }
            }
        },*/

        {InputType.keyboard,

            // map of all controls for the keyboard
            new InputMap
            {
                // map for axis
                axis_map = new Dictionary<string, string[]>()
                {
                    {"Movement",   new string[] { "KEY_AD", "KEY_WS" } },
                    {"Cursor",     new string[] { "KEY_AD", "KEY_WS" } },
                    {"Abilities",  new string[] { "KEY_Shift", "KEY_Space" } },
                    {"AbilityAim", new string[] { "KEY_FH", "KEY_TG" } },
                    {"Camera",     new string[] { "MOUSEX", "MOUSEY" } }
                },

                // map for buttons
                button_map = new Dictionary<string, string[]>()
                {
                    {"Ability0", new string[] { "KEY_Shift" } },
                    {"Ability1", new string[] { "MOUSE0" } },
                    {"Ability2", new string[] { "KEY_L" } },
                    {"FollowCommand", new string[] { "KEY_Q" } },
                    {"FollowCommand2", new string[] { "MOUSE2" } },
                    {"ToggleHealth", new string[] { "KEY_H" } },
                    {"Pause",    new string[] { "KEY_Escape" } },
                    {"Pause2",     new string[] { "KEY_U"} },
                    {"Jump",     new string[] { "KEY_Space" } },
                    //{"Ability3",     new string[] { "KEY_O" } },
                    {"Command",  new string[] { "MOUSE1" } },
                    {"Interact", new string[] { "KEY_F" } },
                    {"Reload", new string[] { "KEY_R" } },
                    {"GroupUp",    new string[] { "KEY_Z"} },
                    {"Story",    new string[] { "KEY_M"} },
                    {"Purify",    new string[] { "MOUSE1"} },
                    {"SelectMode", new string[] { "KEY_E"} },
                    {"SelectMode2", new string[] { "KEY_L" } },
                    {"ActivateAlly1", new string[] { "KEY_1"} },
                    {"ActivateAlly2", new string[] { "KEY_2"} },
                    {"ActivateAlly3", new string[] { "KEY_3"} },
                    {"ActivateAlly4", new string[] { "KEY_4"} },
                    {"SelectAll", new string[] { "KEY_Tab"} }
                }
            }
        }

    };



    //
    // returns 2D vector with input values for 2D axis that is mapped to specified action tag. 
    // If input device uses buttons and not axis, will return 0 or 1
    //
    public static Vector2 get2DAxis(string actionTag, bool normalize)
    {
        // pull out the names for the axis that relates to specified action tag in the currently active control map
        string[] axisNames = inputMaps[curentInput].axis_map[actionTag];

        // pull out the float value for x and y axis using the names listed in current map, use getAxis or getButton depending on type of input
        float x = is_axis[axisNames[0]] ? Input.GetAxis(axisNames[0]) : Input.GetButton(axisNames[0]) ? 1 : 0;
        float y = is_axis[axisNames[1]] ? Input.GetAxis(axisNames[1]) : Input.GetButton(axisNames[1]) ? 1 : 0;
        Vector2 axis = new Vector2(x, y);

        // account for dead zone
        if (axis.magnitude < deadZone)
            axis = Vector2.zero;

        if (axis.magnitude > 1 && normalize)
        {
            axis = axis.normalized;
        }

        return axis;
    }



    //
    // returns true if single button or button combination was pressed this frame
    //
    public static bool getButtonDown(string actionTag)
    {
        try
        {
            if (!inputMaps[curentInput].button_map.ContainsKey(actionTag)) return false;
            string[] keyNames = inputMaps[curentInput].button_map[actionTag];

            // if this is button combo, check if other buttons are held
            for (int i = 0; i < keyNames.Length - 1; i++)
            {
                if (!checkHeld(keyNames[i]))
                {
                    return false;
                }
            }

            // check if last button was pressed this frame
            return checkDown(keyNames[keyNames.Length - 1]);
        }

        catch (KeyNotFoundException e)
        {
            print("actionTag: " + actionTag);
            throw e;
            //throw new UnityException("GetButtonDown Failed");
        }
    }



    //
    // returns true if the last button in this action tag was released this frame
    //
    public static bool getButtonUp(string actionTag)
    {
        string[] keyNames;
        if (!inputMaps[curentInput].button_map.ContainsKey(actionTag)) return false;
        // just in case there is a key exception
        try
        {

            keyNames = inputMaps[curentInput].button_map[actionTag];
        }
        catch (KeyNotFoundException e)
        {
            Debug.LogError("Cannot find button/button sequence for: " + actionTag);
            throw e;
        }

        string buttonName = keyNames[keyNames.Length - 1];

        if (is_axis[buttonName])
        {
            return Input.GetAxis(buttonName) < axisAsButtonTHold && axis_history[buttonName] >= axisAsButtonTHold;
        }
        else
        {
            return Input.GetButtonUp(keyNames[keyNames.Length - 1]);
        }
    }



    //
    // checks if last button in an action string is held down
    //
    public static bool getButton(string actionTag)
    {
        string[] keyNames;
        if (!inputMaps[curentInput].button_map.ContainsKey(actionTag)) return false;

        // just in case there is a key exception
        try
        {
            keyNames = inputMaps[curentInput].button_map[actionTag];
        }
        catch (KeyNotFoundException e)
        {
            Debug.LogError("Cannot find button/button sequence for: " + actionTag);
            throw e;
        }

        // check if last button was released this frame
        return checkHeld(keyNames[keyNames.Length - 1]);
    }



    //
    // checks if physical button is held or not. If axis name is entered, treat axis as a button past specified threshold
    //
    public static bool checkHeld(string buttonName)
    {

        bool isHeld;
        if (is_axis[buttonName])
            isHeld = Input.GetAxis(buttonName) >= axisAsButtonTHold;
        else
            isHeld = Input.GetButton(buttonName);

        return isHeld;
    }



    //
    // checks if a physical button was pressed down on this frame
    //
    public static bool checkDown(string buttonName)
    {
        if (is_axis[buttonName])
        {
            return Input.GetAxis(buttonName) >= axisAsButtonTHold && axis_history[buttonName] < axisAsButtonTHold;
        }
        else
        {
            return Input.GetButtonDown(buttonName);
        }
    }



    //
    // Update is called once per frame
    //
    void LateUpdate()
    {

        // update the values in axis_history
        foreach (var key in axis_history.Keys.ToList())
        {
            axis_history[key] = Input.GetAxis(key);
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            //curentInput = InputType.xBox360;
            Cursor.lockState = CursorLockMode.None;
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            //curentInput = InputType.keyboard;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

}


