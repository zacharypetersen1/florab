﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DBG_PathFinding : MonoBehaviour {


    static DBG_PathFinding singleton;
    public bool pathDebugEnabled = true;
    public bool onlyFollowOne = true;
    public GameObject singlePath;


    void Awake()
    {
        singleton = this;
    }



    public static bool PathDebugIsEnabled()
    {
        return singleton.pathDebugEnabled;
    }
}
