﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SEL_Cursor : MonoBehaviour {

    List<GameObject> hoveredList;
    SelectionManager selectionManager;
    private LayerMask mask;
    public GameObject selectionProjector;
    public Renderer rend;

    // Use this for initialization
    void Start () {
        mask = (1 << 11);
        selectionManager = GameObject.Find("SelectionManager").GetComponent<SelectionManager>();
        hoveredList = new List<GameObject>();
        rend = selectionProjector.GetComponent<Renderer>();
    }
	
	// Update is called once per frame
	void Update () {

        // Check if need to update selection cursor
        if (SelectionManager.isInSelectMode)
        {
            // Raycast from cursor position
            Ray ray = Camera.main.ScreenPointToRay(new Vector3(INP_MouseCursor.position.x, INP_MouseCursor.position.y, Camera.main.nearClipPlane));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask))
            {
                transform.position = hit.point;
            }
            else
            {
                transform.position = Vector3.down * 500;
            }
        }

        else
        {
            transform.position = Vector3.down * 500;
        }
	}

    private void OnTriggerEnter(Collider other)
    {

        foreach (GameObject selectable in selectionManager.getSelectables())
        {
            if (selectable == other.gameObject)
            {
                hoveredList.Add(other.gameObject);
                break;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        hoveredList.Remove(other.gameObject);
    }

    public List<GameObject> getHovered()
    {
        return hoveredList;
    }
}
