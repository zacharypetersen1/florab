﻿// Andrei & Sterling
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class SEL_Manager : MonoBehaviour {



    public SelectionManager selectionManager;
    public List<GameObject> selectedList;
    public GameObject crosshair;
    static SEL_Manager singleton;



    //
    // Use this for initialization
    //
    void Start () {
        selectionManager = GameObject.Find("SelectionManager").GetComponent<SelectionManager>();
        selectedList = new List<GameObject>();
        Cursor.visible = false;
        crosshair = GameObject.Find("Crosshair2");
        singleton = this;
    }



    //
    // Update is called once per frame
    //
    void Update() {

        selectedList = selectionManager.GetSelectedObjectsAsList();

        if (!SelectionManager.isInSelectMode && INP_PlayerInput.getButtonDown("SelectMode"))
        {
            selectionManager.selectAll();
            activateSelectMode();
            CAM_Dolly.snapToDefaultScrollValue(true);
        }
        else if(SelectionManager.isInSelectMode && INP_PlayerInput.getButtonDown("SelectMode"))
        {
            selectionManager.ClearSelection();
            deactivateSelectMode();
            CAM_Dolly.snapToDefaultScrollValue(false);
        }

        if (INP_PlayerInput.getButtonDown("SelectAll"))
        {
            if (SelectionManager.isInSelectMode)
            {
                selectionManager.toggleSelectAll();
            }
        }
    }



    //
    // Activates select mode
    //
    public static void activateSelectMode()
    {
        singleton.selectionManager.selectAll();
        singleton.crosshair.SetActive(false);
        SelectionManager.isInSelectMode = true;
        TME_Manager.setTimeScalar(TME_Time.type.game, 1);
        INP_MouseCursor.position = new Vector2(Screen.width / 2, Screen.height / 2);
    }



    //
    // Deactivates select mode
    //
    public static void deactivateSelectMode()
    {
        singleton.crosshair.SetActive(true);
        SelectionManager.isInSelectMode = false;
        TME_Manager.setTimeScalar(TME_Time.type.game, 1);
    }



    //
    // Returns pair of dictionaries, one containing amount of selected minions stored by type, and one containing the total amount of minions stored by type
    //
    public Pair<Dictionary<ENT_DungeonEntity.monsterTypes, int>, Dictionary<ENT_DungeonEntity.monsterTypes, int>> getSelected()
    {
        // Count total minions
        Dictionary<ENT_DungeonEntity.monsterTypes, int> totalCount = new Dictionary<ENT_DungeonEntity.monsterTypes, int>();
        foreach (Transform child in GameObject.Find("UnitContainer").transform)
        {
            if (child != null)
            {
                var type = child.GetComponent<ENT_Body>().entity_type;
                if (totalCount.ContainsKey(type))
                {
                    totalCount[type] += 1;
                }
                else totalCount.Add(type, 1);
            }
        }

        Dictionary<ENT_DungeonEntity.monsterTypes, int> selectedCount = new Dictionary<ENT_DungeonEntity.monsterTypes, int>();
        foreach (GameObject obj in selectedList)
        {
            if (obj != null)
            {
                var type = obj.GetComponent<ENT_Body>().entity_type;
                if (selectedCount.ContainsKey(type))
                {
                    selectedCount[type] += 1;
                }
                else selectedCount.Add(type, 1);
            }
        }
        return new Pair<Dictionary<ENT_DungeonEntity.monsterTypes, int>, Dictionary<ENT_DungeonEntity.monsterTypes, int>>(selectedCount, totalCount);
    }



    //
    // 
    //
    public Dictionary<ENT_DungeonEntity.monsterTypes, int> getHovered()
    {
        // Count total minions
        Dictionary<ENT_DungeonEntity.monsterTypes, int> totalCount = new Dictionary<ENT_DungeonEntity.monsterTypes, int>();
        foreach (Transform child in GameObject.Find("UnitContainer").transform)
        {
            if (child != null)
            {
                var type = child.GetComponent<ENT_Body>().entity_type;
                if (totalCount.ContainsKey(type))
                {
                    totalCount[type] += 1;
                }
                else totalCount.Add(type, 1);
            }
        }

        // Count selected minions
        Dictionary <ENT_DungeonEntity.monsterTypes, int> selectedCount = new Dictionary<ENT_DungeonEntity.monsterTypes, int>();
        foreach (GameObject obj in GetComponent<SelectionManager>().GetHoveredObjects())
        {
            if (obj != null)
            {
                var type = obj.GetComponent<ENT_Body>().entity_type;
                if (selectedCount.ContainsKey(type))
                {
                    selectedCount[type] += 1;
                }
                else selectedCount.Add(type, 1);
            }
        }

        return selectedCount;
    }

    // My failed / unfinished attempt to use generics

    //Dictionary<ENT_DungeonEntity.monsterTypes, int> countMinions<T>(List<T> minions)
    //{
    //    Dictionary<ENT_DungeonEntity.monsterTypes, int> count = new Dictionary<ENT_DungeonEntity.monsterTypes, int>();
    //    foreach (T obj in minions)
    //    {
    //        var type = obj.GetComponent<ENT_Body>().entity_type;
    //        if (count.ContainsKey(type))
    //        {
    //            count[type] += 1;
    //        }
    //        else count.Add(type, 1);
    //    }
    //    return count;
    //}
}
