﻿// Andrei

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SEL_SelectionModeCamera : MonoBehaviour {

    CAM_Dolly dolly;

    public float minHorizSpeed = 0;
    public float maxHorizSpeed = 4;
    public float minVertSpeed = 0;
    public float maxVertSpeed = 4;
    public int horizontalMargin = 10;  // Percent
    public int verticalMargin = 10;    // Percent
    public float horizBorderSpeed = 1; // Speed of panning when mouse pushes beyond edge of screen
    public float vertBorderSpeed = 1;

    float leftMargin;                  // Also the size of the horizontal margins
    float rightMargin;
    float botMargin;                   // Also the size of the vertical margins
    float topMargin;

    float mouseX;
    float mouseY;

    float savedWidth;
    float savedHeight;

    // Use this for initialization
    void Start () {
        dolly = GameObject.Find("CameraDolly").GetComponent<CAM_Dolly>();
        setMargin();
    }
	
	// Update is called once per frame
	void Update () {
        // If screen has been resized, recaclulate margins
        if (Screen.width != savedWidth || Screen.height != savedHeight)
        {
            setMargin();
        }

        if (SelectionManager.isInSelectMode)
        {
            mouseX = INP_MouseCursor.position.x;
            mouseY = INP_MouseCursor.position.y;
            setHorizRot();
            setVertRot();
            clampRot();
        }
	}

    void setHorizRot()
    {
        float intensity = 0;
        // Left
        if (mouseX < leftMargin)
        {
            intensity = (leftMargin - mouseX) / leftMargin;
            dolly.horizontalRotation += calcHorizSpeed(intensity);
            var mouseDelta = Input.GetAxis("MOUSEX");
            if ((int) mouseX <= 0 && mouseDelta < 0)
            {
                //Code for action on mouse moving left
                dolly.horizontalRotation -= mouseDelta * horizBorderSpeed;
            }
        }
        // Right
        else if (mouseX > rightMargin)
        {
            intensity = (mouseX - rightMargin) / leftMargin;
            dolly.horizontalRotation -= calcHorizSpeed(intensity);
            var mouseDelta = Input.GetAxis("MOUSEX");
            if ((int) mouseX == Screen.width && mouseDelta > 0)
            {
                //Code for action on mouse moving right
                dolly.horizontalRotation -= mouseDelta * horizBorderSpeed;
            }
        }
    }

    void setVertRot()
    {
        float intensity = 0; // Stays between [0, 1]
        // Bottom
        if (mouseY < botMargin)
        {
            intensity = (botMargin - mouseY) / botMargin;
            dolly.verticleRotation -= calcVertSpeed(intensity);
            var mouseDelta = Input.GetAxis("MOUSEY");
            if ((int) mouseY == 0 && mouseDelta < 0)
            {
                //Code for action on mouse moving down
                dolly.verticleRotation += mouseDelta * vertBorderSpeed;
            }
        }
        // Top
        else if (mouseY > topMargin)
        {
            intensity = (mouseY - topMargin) / botMargin;
            dolly.verticleRotation += calcVertSpeed(intensity);
            var mouseDelta = Input.GetAxis("MOUSEY");
            if ((int) mouseY == Screen.height && mouseDelta > 0)
            {
                //Code for action on mouse moving up
                dolly.verticleRotation += mouseDelta * vertBorderSpeed;
            }
        }
    }

    // Calculate how much to vertically rotate the camera, clamped by minVertSpeed and maxVertSpeed
    float calcVertSpeed(float intensity)
    {
        return (minVertSpeed + maxVertSpeed * intensity);
    }



    // Calculate how much to horizontally rotate the camera, clamped by minHorizSpeed and maxHorizSpeed
    float calcHorizSpeed(float intensity)
    {
        return (minHorizSpeed + maxHorizSpeed * intensity);
    }

    // Clamp rotation
    void clampRot()
    {
        dolly.horizontalRotation %= 360;
        dolly.verticleRotation %= 360;
        if (dolly.horizontalRotation < 0) dolly.horizontalRotation += 360;
        if (dolly.verticleRotation < 0) dolly.verticleRotation += 360;
        dolly.clampVertical();
    }

    // Translates the margins from percent to float
    void setMargin()
    {
        savedWidth = Screen.width;
        savedHeight = Screen.height;
        leftMargin = savedWidth * (horizontalMargin / 100f);
        rightMargin = savedWidth * (1 - horizontalMargin / 100f);
        botMargin = savedHeight * (verticalMargin / 100f);
        topMargin = savedHeight * (1 - verticalMargin / 100f);
    }

}
