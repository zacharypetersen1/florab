﻿// Andrei
using UnityEngine;

public class TGT_Targetable : MonoBehaviour
{
    public bool isEnemy;
    GameObject twig;
    public bool isTarget;



    //
    // Returns angle from this targetable relative to current camera angle
    //
    public float getAngleToTwig()
    {
        Vector2 twigPos = UTL_Math.vec3ToVec2(twig.transform.position);
        Vector2 thisPos = new Vector2(transform.position.x, transform.position.y);
        Vector2 twigAngle = UTL_Math.angleRadToUnitVec(twig.GetComponent<CHA_Motor>().getRotation());
        Vector2 targetAngle = UTL_Math.angleRadToUnitVec(Mathf.Atan2(thisPos.y - twigPos.y, thisPos.x - twigPos.x));
        float targetAngleF = Mathf.Rad2Deg * UTL_Math.vecToAngleRad(targetAngle);
        float twigAngleF = Mathf.Rad2Deg * UTL_Math.vecToAngleRad(twigAngle);


        // Taken from http://stackoverflow.com/questions/7570808/how-do-i-calculate-the-difference-of-two-angle-measures
        int resultSign = (twigAngleF - targetAngleF >= 0 && twigAngleF - targetAngleF <= 180) || (twigAngleF - targetAngleF <= -180 && twigAngleF - targetAngleF >= -360) ? 1 : -1;


        float resultAngle = Mathf.Rad2Deg * Mathf.Acos(Vector2.Dot(targetAngle, twigAngle)) * resultSign;
        return resultAngle;
    }



    //
    // Returns distance from this targetable to twig
    //
    public float getDistance()
    {
        Vector2 targetablePosition = new Vector2(transform.position.x, transform.position.y);
        Vector2 twigPosition = new Vector2(twig.GetComponent<Transform>().position.x, twig.GetComponent<Transform>().position.y);
        return Vector2.Distance(targetablePosition, twigPosition);
    }



    //
    // Use this for initialization
    //
    void Start()
    {
        twig = GameObject.Find("Player");
        isTarget = false;
    }



    //
    // Update is called once per frame
    //
    void Update()
    {
        /*
        if (getDistance() <= TGT_Controller.maxDist)
        {
            TGT_Controller.viableTargets.Add(this);
            TGT_Controller.controller.targetQueue.insert(this, TGT_Controller.calcPriority(this));
        }*/
    }
}