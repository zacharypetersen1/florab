﻿// Zach

using UnityEngine;
using System.Collections;

public class EFX_AuraParticles : MonoBehaviour {



    public float radius = 5;            // radius of particle effect
    public float innerRadius = 1.5f;    // radius where there is no size scaling
    public int numParticles = 10;
    private AuraParticle[] particles;



    //
    // Defines single particle
    //
    class AuraParticle
    {

        Transform playerT;  // reference to the player object
        GameObject particleObj;     // reference to gameobject of the plant
        Vector3 scale;
        float genRadius;    // defines radius around twig where particle can generate
        float innerRadius;

        //
        // Constructor
        //
        public AuraParticle(float setRadius, float setInnerRadius, Transform playerTransform)
        {
            playerT = playerTransform;
            genRadius = setRadius;
            innerRadius = setInnerRadius;

            // find random offset values from player
            float r = Random.Range(0, 1f);
            float theta = Random.Range(0, 2 * Mathf.PI);
            float zOffset = (Mathf.Sqrt(r) * Mathf.Sin(theta)) * genRadius;
            float xOffset = (Mathf.Sqrt(r) * Mathf.Cos(theta)) * genRadius;

            // load object at players location + offset values. Also set the scale
            particleObj = UTL_Resources.cloneAtLocation("Plants/GrassClump", playerT.position + new Vector3(xOffset, 0, zOffset));
            scale = particleObj.transform.localScale * Random.Range(0.8f, 1.2f);
            particleObj.transform.Rotate(0, 0, Random.Range(0, 360));
            // TODO make a better system for hiding particles
            particleObj.SetActive(MAP_NatureMap.natureTypeAtPos(particleObj.transform.position) == MAP_NatureData.type.grass);
        }

        //
        // Updates particle every frame
        //
        public void update()
        {
            if(Vector3.Distance(playerT.position, particleObj.transform.position) > genRadius)
            {
                // relocate the particle
                // TODO only generate in front
                float theta = Random.Range(0, 2 * Mathf.PI);
                float yOffset = Mathf.Sin(theta) * genRadius;
                float xOffset = Mathf.Cos(theta) * genRadius;
                particleObj.transform.position = playerT.position + new Vector3(xOffset, yOffset, 0);
                particleObj.transform.position = UTL_Dungeon.snapPositionToTerrain(particleObj.transform.position);
                // TODO make a better system for hiding particles
                particleObj.SetActive(MAP_NatureMap.natureTypeAtPos(particleObj.transform.position) == MAP_NatureData.type.grass);
            }

            // update size of particle
            float dist = Vector3.Distance(playerT.position, particleObj.transform.position);
            if (dist < innerRadius)
            {
                particleObj.transform.localScale = scale;
            }
            else
            {
                float scalar = 1 - ((dist - innerRadius) / (genRadius - innerRadius));
                particleObj.transform.localScale = scale * scalar;
            }
        }
    }



	//
	// Use this for initialization
	//
	void Start () {
        particles = new AuraParticle[numParticles];
        for(int i = 0; i < numParticles; i++)
        {
            particles[i] = new AuraParticle(radius, innerRadius, transform);
        }
	}
	


	//
	// Update is called once per frame
	//
	void Update () {
        for (int i = 0; i < numParticles; i++)
        {
            particles[i].update();
        }
    }
}
