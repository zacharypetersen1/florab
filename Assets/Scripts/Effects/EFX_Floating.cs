﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EFX_Floating : MonoBehaviour
{
    private float counter;
    Transform twig_t;
    Vector3 init_p;
    public float intensity = .3f;   // distance of bobbing
    public float speed = .06f;      // speed of bobbing [god dangit bobby
    public float active_radius = 8; // distance at which object should track player
    public float turn_speed = 6;    // speed at which object tracks player, lower == floatier, higher == snappier
    public float collect_radius = 1.8f;

    // p0 - p3 affect the easing function of the destroy animation
    // use this website to find desired p values http://cubic-bezier.com/#.35,-0.21,1,.46
    public float p0 = 0f;
    public float p1 = -.12f;
    public float p2 = 1f;
    public float p3 = -.31f;

    //private bool in_transition = false;
    //private bool was_in_idle = false;
    private bool dying = false;
    private float dying_counter = 0;

    private static bool displayedMessage = false; //for tutorial

    [FMODUnity.EventRef]
    public string collectStory = "event:/collect_story";
    FMOD.Studio.EventInstance collectEv;



    // Use this for initialization
    void Start()
    {
        counter = 0f;
        twig_t = GameObject.FindGameObjectWithTag("Player").transform;
        init_p = transform.position;

        collectEv = FMODUnity.RuntimeManager.CreateInstance(collectStory);
    }

    // Update is called once per frame
    void Update()
    {
        // Get distance from this to twig
        float dist = Vector2.Distance(new Vector2(twig_t.position.x, twig_t.position.z), new Vector2(transform.position.x, transform.position.z));

        // Begin self destruct animation, trigger collection event
        if (dist <= collect_radius)
        {
            on_collect();
        }
        
        // Floating animations
        else
        {
            upDown();
            facePlayer(0);
        }

        // Do implode animation
        if (dying)
        {
            dying_step();
        }
    }

    // Actual code to run when object gets collected
    void on_collect()
    {
        if (!dying)
        {
            collectEv.start();
            GameObject.Find("Story_Menu").GetComponent<EFX_StoryMenu>().incrementStory();
            dying = true;
            if (!displayedMessage)
            {
                TUT_TutorialManager.queueMessage(TUT_Messages.Messages.StoryPiece);
                displayedMessage = true;
            }
        }
    }

    // Implode animation
    void dying_step()
    {
        float delta = 6 * UTL_Math.cubicBezier(dying_counter, p0, p1, p2, p3);
        transform.localScale = new Vector3(transform.localScale.x * (1 - delta), transform.localScale.y * (1 - delta), transform.localScale.z * (1 - delta));

        // Destroy object once it's small enough
        if (transform.localScale.x <= 0.1 && transform.localScale.y <= 0.1 && transform.localScale.z <= 0.1)
        {
            Object.Destroy(gameObject);
        }

        // Make sure scale of object never goes below 0.1
        if (transform.localScale.x < 0.1f) transform.localScale = new Vector3(0.1f, transform.localScale.y, transform.localScale.z);
        if (transform.localScale.y < 0.1f) transform.localScale = new Vector3(transform.localScale.x, 0.1f, transform.localScale.z);
        if (transform.localScale.z < 0.1f) transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, 0.1f);

        dying_counter += .01f;
    }

    // Floating up and down animation
    void upDown()
    {
        // Up and down motion
        if (counter >= 2 * Mathf.PI) counter = 0f;
        else counter += speed;
        //transform.position = new Vector3(init_p.x, init_p.y + intensity * Mathf.Sin(counter), init_p.z);
        var target_position = new Vector3(init_p.x, init_p.y + intensity * Mathf.Sin(counter), init_p.z);
        transform.position = Vector3.Slerp(transform.position, target_position, Time.deltaTime * 2);
    }

    // Floating look at player animation
    void facePlayer(float speed)
    {
        if (speed == 0) speed = turn_speed;
        // Face player motion
        var rotationAngle = Quaternion.LookRotation(twig_t.position - transform.position); // we get the angle has to be rotated
        var rotationAngle2 = new Quaternion(0, rotationAngle.y, 0, rotationAngle.w);
        var rot = Quaternion.Slerp(transform.rotation, rotationAngle2, Time.deltaTime * speed); // we rotate the rotationAngle 
        transform.rotation = new Quaternion(rot.x, rot.y, rot.z, rot.w);
    }

}