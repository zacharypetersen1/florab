﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Text;
public class EFX_StoryMenu : MonoBehaviour
{
    public bool right_enter = true; // Enter screen from right, otherwise enter from left
    public float scroll_speed = 1; // Speed of scrolling, can use for parallax

    // Final position relative to camera
    public float x_offset = 0;
    public float y_offset = 0;
    public float z_offset = 0; // Use to arrange multiple elements, lower is closer

    public float max_scroll_offset = 1;
    float scroll_offset = 0;
    float counter = 0;
    float offscreen_offset = 6;
    public static bool active = false;
    bool slideOut = false;
    Vector3 init_p;
    public TextMeshPro text;
    int story_counter = 0;
    string timerName = "storyNoteTimer";
    bool timerSet = false;
    GameObject noteIcon;

    List<string> stories = new List<string> {
        "Day 1:\n\nI begin my search into the power of the guardians. For years we have lived in peace and harmony with the forest, yet always secluded. What force have they been protecting us from and what is the source of their power?",
        "\n\nDay 56:\n\nThe more time I spend in the Forest, the more it feels, alive. I do not find the words to explain all of what I am experiencing. I do not lack provisions and am confident that what I have seen is not a trick of my mind. Two days ago I saw what I believe to be a mushroom stand up and start walking around. A Mushroom! When it sensed my presence it hid itself from me.",
        "\n\nDay 72:\n\nThe first thing we learned in the temple was to cherish life. Those who would honor and cherish all life could become Vineweavers. It was a Vineweaver's job to defend the forest and all beings living in it.",
        "\n\nDay 93:\n\nThere is some force alive in the Forest, I know for certain. It must be what the Vineweavers pull their power from. If only we could find the source, we would not need to rely on the guardians to bestow power to the few.",
        "\n\nDay 107:\n\nThe Grand Guardians were the first beings to learn Vineweaving. This knowledge was passed on to humans they found worthy to protect the secrets of the Forest. The origins of this power is a mystery to all but the oldest of Guardians.",
        "\n\nDay 115:\n\nI HAVE FOUND ONE! I stumbled across a sentient flower! A fully functioning flora. Believing this creature to hold insight into the world of the Guardians I attempted to capture it. In a struggle with the creature i was left holding a flower. No sign of sentience! What game the Forest plays with my mind! There is no doubt that the power of the Guardians is at work here.",
        "\n\nDay 131:\n\nIt is tradition in my village to name children after flora. When I was just a young child the other children would tease me about my name. I would feel so small and weak, but my father would tell me that he gave me my name because one day help restore purity to the Forest.",
        "\n\nDay 140:\n\nI hear them. There are more creatures out here. I know I am close to discovering the secrets of the forest. The things we could accomplish with the power that it yields.",
        "\n\nDay 152:\n\nIt is said that the most powerful of Vineweavers could summon the might of the ancient Forest to fight by their side."
    };

    string cur_story = "";

    // Use this for initialization
    void Start()
    {
        foreach (string story in stories)
        {
            story.Replace("\n", "\\u000a");
        }
        if (right_enter) init_p = new Vector3(offscreen_offset - x_offset, y_offset, 1 + z_offset);
        else init_p = new Vector3(x_offset - offscreen_offset, y_offset, 1);
        new Vector3(x_offset - offscreen_offset, y_offset, 1 + z_offset);
        transform.localPosition = init_p;
        max_scroll_offset = max_scroll_offset * scroll_speed;
        text.SetText(cur_story);
        TME_Timer.addTimer(timerName, TME_Time.type.game);
        noteIcon = GameObject.Find("NewStoryIcon");
        noteIcon.SetActive(false);
    }


    // Update is called once per frame
    void Update()
    {
        if (INP_PlayerInput.getButtonDown("Story"))
        {
            if (active)
            {
                active = false;
                slideOut = true;
                //TME_Manager.setTimeScalar(TME_Time.type.game, 1);
                //CAM_Free.frozen = false;
            }
            else
            {
                active = true;
                slideOut = false;
                //TME_Manager.setTimeScalar(TME_Time.type.game, 0);
                //CAM_Free.frozen = true;
            }
        }
        if ((Input.GetKeyDown("escape") || Input.GetMouseButtonDown(0)) && active)
        {
            active = false;
            slideOut = true;
        }
        if (active)
        {
            set_position(counter);
            if (counter < 1) counter += .02f;
        }
        if (slideOut)
        {
            set_position(counter);
            if (counter > 0) counter -= .02f;
        }

        // Notification icon
        if (timerSet)
        {
            if (TME_Timer.timeIsUp(timerName))
            {
                timerSet = false;
                noteIcon.SetActive(false);
            }
        }
    }


    public void set_position(float c)
    {
        scroll_offset -= Input.GetAxis("Mouse ScrollWheel") * scroll_speed;
        scroll_offset = Mathf.Max(0, scroll_offset);
        scroll_offset = Mathf.Min(max_scroll_offset, scroll_offset);
        var cam_p = transform.parent.transform.position;
        var x = easeOut(c);
        Vector3 tar_p;
        if (right_enter) tar_p = new Vector3(offscreen_offset * (1 - x) - x_offset, y_offset + scroll_offset, 1 + z_offset);
        else tar_p = new Vector3(x_offset - offscreen_offset * (1 - x), y_offset + scroll_offset, 1 + z_offset);
        var tar_p_lerped = Vector3.Lerp(transform.localPosition, tar_p, Time.deltaTime * 8);
        transform.localPosition = tar_p_lerped;
    }


    public static float easeOut(float x)
    {
        return -(2.5f) * Mathf.Pow(x, 3) + (3.5f) * Mathf.Pow(x, 2);
    }


    public void incrementStory()
    {
        story_counter = Mathf.Min(story_counter + 1, stories.Count);
        Debug.Log(story_counter);
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < story_counter; i++)
        {
            stringBuilder.Append(stories[i]);
        }
        text.SetText(stringBuilder.ToString());
        enableNotification();
    }

    void enableNotification()
    {
        timerSet = true;
        TME_Timer.setTime(timerName, 5);
        noteIcon.SetActive(true);
    }
}