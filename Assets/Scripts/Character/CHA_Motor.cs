﻿// Zach

using UnityEngine;
using System;
using System.Collections;

public class CHA_Motor : MonoBehaviour {

    public float acceleration = 0.02f;
    public float gravity = -2f;
    public float jumpForce = 1.5f;
    private float speed = 2f;
    private float targetSpeed;
    public float speedAcceleration = .1f;
    private Vector2 targetVelocity = Vector2.zero;
    //public Vector2 velocity = Vector2.zero;
    public float rotation = 0;                     // character's current rotation
    public bool clampVelocityToDirection = false;   // if true, character only moves in the direction they are facing
    public float rotationVelocity = 1f;             // Scalar for rotation constant
    private float rotationConstant = Mathf.PI/20f;  // constant rotation velocity for linear rotation
    public bool sendAnimData = false;
    public bool sendSoundData = false;
    public GameObject animObject;
    private Animator animator;
    private Vector3 pastLocation;
    public TME_Time.type updateTime = TME_Time.type.game;   // motor will update on this time
    private SFX_System soundSystem;
    public GameObject soundTargetObject;
    public bool isJumping = false;
    private float addJumpForce = 0;
    private float jumpResetBufferTime = 0.2f;
    private float jumpResetTimer = 0;
    public float forceAccel = 0.03f;
    private bool wasFalling;
    public bool landed = false;
    public bool isSliding = false;
    Rigidbody rb;

    RaycastHit hit;
    float groundRaycastDistance = 3f;
    LayerMask layermask;
    public enum groundType { air, terrainGround, meshGround };



    //
    // Stores state of motor in a frame
    //
    public struct state
    {
        public Vector2 targetVelocity;
        public Vector3 pastLocation;
        public Vector3 position;
        public float speed;
        public float targetSpeed;
    }


    //
    // Saves current movement state
    //
    public state saveState()
    {
        state s = new state();
        s.targetVelocity = targetVelocity;
        s.pastLocation = pastLocation;
        s.position = transform.position;
        s.speed = speed;
        s.targetSpeed = targetSpeed;
        return s;
    }



    //
    // Loads given state into motor
    //
    public void loadState(state s)
    {
        targetVelocity = s.targetVelocity;
        pastLocation = s.pastLocation;
        transform.position = s.position;
        speed = s.speed;
        targetSpeed = s.targetSpeed;
    }



    //
    // initialize
    //
    void Start()
    {
        rb = GetComponent<Rigidbody>();

        layermask = (1 << 11) | (1 << 14);
        pastLocation = transform.position;
        
        // Make sure that the palyer does not send sound data w/o animation data
        if(sendSoundData && !sendAnimData)
        {
            throw new UnityException("Cannot send sound information without sending animation information");
        }

        // Get references to locations where sound/anim data will be sent
        if (sendAnimData)
        {
            animator = animObject.GetComponent<Animator>();
            pastLocation = transform.position;
        }
        if (sendSoundData)
        {
            soundSystem = soundTargetObject.GetComponent<SFX_System>();
        }

        wasFalling = isInAir();

        // Initialize target speed
        //targetSpeed = speed;
    }



    //
    // returns CH_Motor attatched to specified game object
    //
    public static CHA_Motor get(string gameObjectName)
    {
        try
        {
            CHA_Motor temp = GameObject.Find(gameObjectName).GetComponent<CHA_Motor>();
            return temp;
        }
        catch (NullReferenceException e)
        {
            Debug.LogError("Unable to find CH_Motor for object named: '" + gameObjectName + "'");
            throw e;
        }
    }


    //
    // Sets the target speed for the motor
    //
    public void setSpeed(float newSpeed)
    {
        targetSpeed = newSpeed;
    }



    //
    // sets z rotation of character
    //
    public void setRotation(Vector2 input)
    {
        float targetRotation = UTL_Math.vecToAngleRad(input);
        float scaledRotationVelocity = rotationConstant * rotationVelocity * input.magnitude;

        // check if angle is within one step of target angle, be sure to catch case that wraps past 2 PI
        if(    Mathf.Abs(targetRotation              - rotation) <= scaledRotationVelocity 
            || Mathf.Abs(targetRotation + 2*Mathf.PI - rotation) <= scaledRotationVelocity)
        {
            rotation = targetRotation;
        }

        // determine if rotation is positive or negative
        else
        {
            // apply rotation so that "rotation" variable would be 0 and targetRotation is at same relative angle
            targetRotation = (targetRotation - rotation + 2 * Mathf.PI) % (2 * Mathf.PI);

            // determine correct direction of rotation
            if (targetRotation <= Mathf.PI)
            {
                
                // calculate new rotation accounting for clock delta time
                rotation += scaledRotationVelocity * TME_Manager.getDeltaTime(updateTime) * 60;
                if(rotation >= 2 * Mathf.PI)
                {
                    rotation -= 2 * Mathf.PI;
                }
            }
            else
            {

                // calculate new rotation accounting for clock delta time
                rotation -= scaledRotationVelocity * TME_Manager.getDeltaTime(updateTime) * 60;
                if(rotation < 0)
                {
                    rotation += 2 * Mathf.PI;
                }
            }
        }

        // set rotation of gameobject
        transform.rotation = Quaternion.identity;
        transform.Rotate(0, -rotation * 180 / Mathf.PI, 0, Space.World);
    }



    //
    // wrapper for moving, accounts for acceleration. All velocity calculated <= unit vector, speed multiplied on later
    //
    public void move(Vector2 setTargetVelocity)
    {
        targetVelocity = setTargetVelocity;
    }

    //
    // detects ground type beneath Twig
    //
    public groundType onGround()
    {
        if (Physics.Raycast(gameObject.transform.position + Vector3.up, -Vector3.up, out hit, groundRaycastDistance, layermask))
        {
            if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Ground"))
            {
                return groundType.terrainGround;
            }
            else if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Ruins"))
            {
                return groundType.meshGround;
            }
        }
        return groundType.air;
    }



    //
    // Checks if Twig is "falling" i.e. she is not touching the ground
    //
    public bool isInAir()
    {
        return onGround() == groundType.air || isDuringJumpResetBuffer();
    }



    //
    // Checks if Twig "Landed" on ground this frame
    //
    public bool isLanded()
    {
        return landed;
    }



    //
    // Checks if the player is restricted from jumping because of the jump reset buffer time
    //
    public bool isDuringJumpResetBuffer()
    {
        return jumpResetTimer > 0;
    }



    //
    // Make the character jump with the desired force
    //
    public void jump(float force)
    {
        isJumping = true;
        if (sendAnimData)
        {
            animator.SetBool("isFalling", true);
        }
        addJumpForce += force;
        jumpResetTimer = jumpResetBufferTime;
    }



    //
    // Updates the motor by one tick
    //
    public void FixedUpdate()
    {
        rb.velocity = Vector3.zero;

        // save state in case we need to reload it
        state s = saveState();

        if(isJumping && onGround() != groundType.air && !isDuringJumpResetBuffer())
        {
            isJumping = false;
        }

        // Update target Speed
        if (speed != targetSpeed)
        {
            float speedAccelStep = speedAcceleration * TME_Manager.getDeltaTime(updateTime);

            // Lock speed to target if within one step
            if (Mathf.Abs(targetSpeed - speed) <= speedAccelStep)
            {
                speed = targetSpeed;
            }
            else
            {
                speed += Mathf.Sign(targetSpeed - speed) * speedAccelStep;
            }
        }

        // get Velocity
        Vector2 velocityXZ = getVelocity2D();
        float velocityY = getVelocity().y;

        if(velocityY > 2f)
        {
            velocityY = 0;
        }

        // ensure that Yvelocity doesn't get too big
        if (!isJumping && velocityY > 0)
        {
            velocityY = 0;
        }

        //sliding

        if (isSliding)
        {
            if (!PLY_PlayerDriver.singleton.spiderTwig)
            {
                rotationVelocity = 5;
                if (ABI_Moving.singleton.isSurfing)
                {
                    ABI_Moving.singleton.stopSurfing();
                }
            }

            // do some vector math to get movement input
            Vector3 terNormal = MAP_Terrain.getTerrainNormal(transform.position);
            //Vector3 copy = terNormal; //for printing
            Vector3 temp = terNormal;
            temp.y = 0;
            temp = temp.normalized;
            temp = (temp + Vector3.up).normalized;
            terNormal = Vector3.Reflect(-terNormal, temp);
            //print("origX:" + copy.x + " newX" + terNormal.x);

            move(UTL_Math.vec3ToVec2(terNormal*1.2f));
            velocityY = -terNormal.y*.9f;
            velocityXZ = Vector2.zero;
        }

        // Update past location now that we don't need to get velocity
        pastLocation = transform.position;
        velocityY += gravity * TME_Manager.getDeltaTime(updateTime);

        targetVelocity *= TME_Manager.getDeltaTime(updateTime) * speed;

        if (addJumpForce > 0)
        {
            velocityY = addJumpForce * jumpForce;
            addJumpForce = 0;
        }

        if ((targetVelocity - velocityXZ).magnitude <= acceleration)
        {
            velocityXZ = targetVelocity;
        }
        else
        {
            // add acceleration as a vector with "acceleration" variable multiplied as scalar
            velocityXZ += (targetVelocity - velocityXZ).normalized * acceleration;
        }

        // update character's rotation
        if (targetVelocity.magnitude > 0)
        {
            setRotation(targetVelocity);
        }

        // apply translation to the object
        if (clampVelocityToDirection)
        {
            // only move in direction that character is facing
            Vector2 clampedVel = UTL_Math.angleRadToUnitVec(rotation) * velocityXZ.magnitude;
            physicalMove(new Vector3(clampedVel.x, velocityY, clampedVel.y));
        }
        else
        {
            // move normally
            Vector3 finalVelocity = new Vector3(velocityXZ.x, velocityY, velocityXZ.y);
            physicalMove(finalVelocity); //* TME_Manager.getDeltaTime(updateTime) * 5 * speed);
        }

        // update state information
        // Update the jump reset buffer timer if necessary
        if (isDuringJumpResetBuffer())
        {
            jumpResetTimer -= TME_Manager.getDeltaTime(TME_Time.type.game);
            jumpResetTimer = Mathf.Clamp(jumpResetTimer, 0, jumpResetBufferTime);
        }

        // Update landed
        landed = wasFalling && !isInAir();
        wasFalling = isInAir();

        // Calculate info needed to check sliding
        Vector2 playerMoveVec = velocityXZ.normalized;
        Vector2 terrainVec = UTL_Math.vec3ToVec2(MAP_Terrain.getTerrainNormal(transform.position)).normalized;
        float slopeConflictDot = Vector2.Dot(playerMoveVec, terrainVec);
        float slopeConflictAngle = Mathf.Acos(slopeConflictDot);

        // Check if player should be sliding
        if (!isSliding && willSlideHere(transform.position))
        {
            if (slopeConflictAngle > 1.57f && !landed)
            {
                loadState(s);
                pastLocation = transform.position;
            }
            else
            {
                isSliding = true;
            }
        }

        // Check if player should stop sliding
        if (isSliding && (MAP_Terrain.getTerrainSlopeAngle(transform.position) <= .9f || onGround() == groundType.meshGround))
        {
            isSliding = false;
        }

        // update animation velocity based on physical distance moved
        if (sendAnimData)
        {
            // get the offset since last frame
            Vector3 delta = transform.position - pastLocation;

            // get velocity and animation scalar accounting for the scalar of the clock
            float velScalar = (delta.magnitude * 11);
            float animScalar = (0.75f + velScalar * .16f);
            float yVel = delta.y;

            // set the velocity and anim scalar varibles within the animator
            if (animator != null)
            {
                animator.SetFloat("velocity", getVelocity2D().magnitude);
                animator.SetFloat("velocityAnimScalar", animScalar);
                animator.SetFloat("yVelocity", yVel * 3);
                if (isLanded())
                {
                    animator.SetBool("isFalling", false);
                }
            }

            // only send sound data if needed
            if (sendSoundData)
            {
                soundSystem.velocity = velScalar;
            }
        }

        // reset target velocity
        targetVelocity = Vector2.zero;
    }



    //
    // Checks if the entity will slide at the given position
    //
    public bool willSlideHere(Vector3 position)
    {
        groundType g = onGround();
        MAP_NatureData.type t = MAP_NatureMap.natureTypeAtPos(transform.position);
        if (g != groundType.terrainGround) return false;
        if (PLY_PlayerDriver.singleton.spiderTwig && ABI_Moving.singleton.isSurfing) return false;
        else if (!PLY_PlayerDriver.singleton.spiderTwig && ABI_Moving.singleton.isSurfing && t == MAP_NatureData.type.grass) return false;
        float angle = MAP_Terrain.getTerrainSlopeAngle(transform.position);
        return angle > .9f;
    }



    //
    // moves object exactly x and y units
    //
    private void physicalMove(Vector3 vec)
    {
        float floorLevel = UTL_Dungeon.snapPositionToTerrain(transform.position + Vector3.up * 0.2f).y;
        gameObject.transform.Translate(vec, Space.World);
        if(transform.position.y < floorLevel)
        {
            transform.position = new Vector3(transform.position.x, floorLevel, transform.position.z);
        }
    }



    //
    // getter for rotation field
    //
    public float getRotation()
    {
        return rotation;
    }



    //
    // returns the physical velocity for this frame
    //
    public float getVelocityMagnitude()
    {
        return (transform.position - pastLocation).magnitude / TME_Manager.getTimeScalar(updateTime);
    }



    //
    // Get 2D velocity (x/z plane)
    //
    public Vector2 getVelocity2D()
    {
        return UTL_Math.vec3ToVec2((transform.position - pastLocation) / TME_Manager.getTimeScalar(updateTime));
    }



    //
    // Get the velocity
    //
    public Vector3 getVelocity()
    {
        return (transform.position - pastLocation);
    }

    public void resetPastLocation()
    {
        pastLocation = transform.position;
    }



    //
    // Checks to see if this entity is touching corruption
    //
    public bool isTouchingCorruption()
    {
        return onGround() == groundType.terrainGround && MAP_NatureMap.natureTypeAtPos(transform.position) == MAP_NatureData.type.corrupt;
    }



    //
    // Checks to see if this entity should be hindered by surfing
    //
    public bool shouldHinderSurf()
    {
        // collect info
        groundType g = onGround();
        MAP_NatureData.type t = MAP_NatureMap.natureTypeAtPos(transform.position);

        // return value
        return !(g == groundType.air || (g == groundType.terrainGround && t == MAP_NatureData.type.grass));
    }
}
