﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CBI_CombatMessenger {

    public float Damage_DC = 0;
    public float Healing_DC = 0;
    public float Delta_Purity = 0;
    public List<status> Status_list = null;
    public Vector3 position;
    //public List<string> Valid_target_tags = null;
    //public GameObject Sender = null;

    public CBI_CombatMessenger(float dc, float healing, float dP, List<status> statList, Vector3 positionOfMessage)
    {
        Damage_DC = dc;
        Healing_DC = healing;
        Delta_Purity = dP;
        Status_list = statList;
        position = positionOfMessage;
    }

    
    public enum status
    {
        slow, confusion, meleeSlow
    }

    public static Dictionary<status, float> statusCooldownMap = new Dictionary<status, float>()
    {
        {status.slow, 5F },
        {status.meleeSlow, 2.4F},
        {status.confusion, 1F }
    };

    public static void sendCombatMessage (ENT_DungeonEntity target, CBI_CombatMessenger message)
    {
        target.receive_combat_message(message);
    }
}
