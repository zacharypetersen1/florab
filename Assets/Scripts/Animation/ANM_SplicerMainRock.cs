﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ANM_SplicerMainRock : MonoBehaviour
{

    Animator anim;
    GameObject player;
    public GameObject twigSpawnPoint;

    public float preDist = 30;
    public float dist = 15;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        float curDist = Vector3.Distance(player.transform.position, transform.position);
        //Debug.Log(curDist);
        if (curDist <= preDist)
        {
            //Debug.Log("pre range");
            anim.SetTrigger("playerInPreRange");
            if (curDist <= dist)
            {
                //Debug.Log("range");
                anim.SetTrigger("playerInRange");
                if (twigSpawnPoint != null)
                {
                    AIR_Respawn.SpawnPoint = twigSpawnPoint.transform.position;
                }
            }
        }
    }
}
