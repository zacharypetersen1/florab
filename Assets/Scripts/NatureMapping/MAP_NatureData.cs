﻿// Zach

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class MAP_NatureData {

    //
    // enum of all nature types
    //
    public enum type
    {
        rock, grass, corrupt
    }



    //
    // maps nature types to colors
    //
    public static Dictionary<type, Color> colorMap = new Dictionary<type, Color>
    {
        {type.grass, new Color(.2f, .7f, .2f) },
        {type.rock, new Color(.5f, .5f, .5f) }
    };



    //
    // gets color that is mapped to given nature type
    //
    public static Color colorFromNatureType(type t)
    {
        return colorMap[t];
    }
}
