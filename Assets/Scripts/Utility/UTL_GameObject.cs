﻿// Zach

using UnityEngine;
using System.Collections;


public static class UTL_Resources {
    
    //
    // clones object at "pathName" from the resources folder at specified location
    //
    /*public static void cloneAtLocation(string pathName, Vector3 location, float z)
    {
        GameObject temp = (GameObject)MonoBehaviour.Instantiate(Resources.Load(pathName));
        temp.transform.position = new Vector3(location.x, location.y, z);
    }*/



    //
    // clones object at "pathName" from the resources folder at specified location
    //
    public static GameObject cloneAtLocation(string pathName, Vector3 location, bool adjustForTerrain = true)
    {
        GameObject temp = (GameObject)MonoBehaviour.Instantiate(Resources.Load(pathName), location, Quaternion.identity);
        if (adjustForTerrain)
        {
            temp.transform.position = UTL_Dungeon.snapPositionToTerrain(location);
        }
        Collider col = temp.GetComponentInChildren<Collider>();
        if (col != null)
        {
            col.enabled = false;
            col.enabled = true;
        }
        return temp;
    }



    //
    // clones object at "pathName" from the resources folder at specified location
    //
    public static GameObject cloneAtLocation(string pathName, Vector2 location)
    {
        return cloneAtLocation(pathName, new Vector3(location.x, location.y, 0));
    }

    //Harold
    //
    // clones object at specified location
    //
    public static GameObject cloneAtLocation(GameObject toClone, Vector3 location, bool adjustForTerrain = true)
    {
        GameObject temp = (GameObject)MonoBehaviour.Instantiate(toClone, location, Quaternion.identity);
        if (adjustForTerrain)
        {
            temp.transform.position = UTL_Dungeon.snapPositionToTerrain(location);
        }
        Collider col = temp.GetComponentInChildren<Collider>();
        if (col != null)
        {
            col.enabled = false;
            col.enabled = true;
        }
        return temp;
    }



    //
    // clones object at specified location
    //
    public static GameObject cloneAtLocation(GameObject toClone, Vector2 location)
    {
        return cloneAtLocation(toClone, new Vector3(location.x, location.y, 0));
    }

    public static GameObject cloneAsChild(GameObject toClone, GameObject Parent)
    {
        GameObject newObject = cloneAtLocation(toClone, Parent.transform.position);
        newObject.transform.SetParent(Parent.transform);
        return newObject;
    }

    public static GameObject cloneAsChild(string pathname, GameObject Parent)
    {
        return cloneAsChild((GameObject)Resources.Load(pathname), Parent);
    }


    //
    // clones object at "pathName" from the resources folder at specified location
    //
    /*public static void cloneAtLocation(string pathName, Vector2 location, float z)
    {
        cloneAtLocation(pathName, new Vector3(location.x, location.y, 0), z);
    }*/



    //
    // Gets material for non-corrupted minions based on minon type
    // 
    public static Material getMinionMaterial(ENT_Body.monsterTypes type)
    {
        switch (type)
        {
            case ENT_DungeonEntity.monsterTypes.confusionMinion:
                return (Material)Resources.Load("Minions/Materials/Bell");
            case ENT_DungeonEntity.monsterTypes.flowerMinion:
                return (Material)Resources.Load("Minions/Materials/Flower");
            case ENT_DungeonEntity.monsterTypes.mushroomMinion:
                return (Material)Resources.Load("Minions/Materials/Mushroom");
            case ENT_DungeonEntity.monsterTypes.necroMinion:
                return (Material)Resources.Load("Minions/Materials/Necro");
            case ENT_DungeonEntity.monsterTypes.slowMinion:
                return (Material)Resources.Load("Minions/Materials/Slow");
            case ENT_DungeonEntity.monsterTypes.treeMinion:
                return (Material)Resources.Load("Minions/Materials/Tree");
        }
        return null;
    }



    //
    //
    //
    public static Sprite getShotUISprite(ABI_Purify.purifyTypes type)
    {
        switch (type)
        {
            case ABI_Purify.purifyTypes.launchShot:
                return Resources.Load<Sprite>("ShotSprites/LaunchShot");
            case ABI_Purify.purifyTypes.quickShot:
                return Resources.Load<Sprite>("ShotSprites/QuickShot");
            case ABI_Purify.purifyTypes.multiShot:
                return Resources.Load<Sprite>("ShotSprites/MultiShot");
        }
        return null;
    }
}
