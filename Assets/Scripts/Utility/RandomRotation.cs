﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotation : MonoBehaviour {

    public float speed = 0.1f;
    private int rotate;
    //float rotation = 0.0f;
    float timer = 0.0f;
    /*
    Transform beg;
    Quaternion end;   
    */
    void Start()
    {
        /*
        beg = gameObject.transform;
        end = gameObject.transform.rotation;
        end *= Random.rotation;
        */
        gameObject.transform.Rotate(new Vector3(Random.Range(180, 360),0,0),Space.Self);
        speed = Random.Range(-2, 2);
        rotate = Random.Range(-1, 1);
        if(rotate < 0)
        {
            rotate = -1;
        }
        else
        {
            rotate = 1;
        }
    }

    void FixedUpdate()
    {
        timer += 1;
        if(timer > 60)
        {
            rotateRandom();
        }
       
    }

    //
    //rotates gameobject randomly
    //
    public void rotateRandom()
    {
        //gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.rotation, end, Time.deltaTime * speed);
        gameObject.transform.Rotate(new Vector3(speed * rotate, 0, 0), Space.Self);
    }
}
