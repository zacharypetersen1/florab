using UnityEngine;
using System.Collections;

public class TrigWave {



	private trigType waveType;
	private float curTime = 0;
	private float period;
	private float amplitude;
	private float value;



	//
	//Records the different types of trig functions that can be used to generat waves
	//
	public enum trigType {
		SINE, COSINE, TANGENT
	}



	//
	//Constructor
	//Use setType to determine what trig function will be used to calculate values of wave
	//Use offsetTime to offset the starting position along the wave
	//
	public TrigWave(trigType setType, float setPeriod, float setAmplitude, float offsetTime) {

		//Error checking
		if(setPeriod <= 0)
			throw new UnityException("Period of trig wave must be set to value that is greater than 0");

		waveType = setType;
		period = setPeriod;
		amplitude = setAmplitude;
		curTime = offsetTime;

		//Call update with no time passing to ensure that value is not null
		update(0);
	}



	//
	//Incriments the time (x-axis) of the sin wave and calculate the output (y-axis) of the wave on current time
	//
	public float update(float deltaTime) {

		//Update current time
		curTime += deltaTime;

		//Reduce curentTime to within first period (keeps current time from growing infinately)
		while(curTime > period)
			curTime -= period;

		//Execute correct math function to determine the value after this time
		switch(waveType) {
		case trigType.SINE : value = amplitude * Mathf.Sin( ( (1/period)*2*Mathf.PI) * curTime); break;
		case trigType.COSINE : value = amplitude * Mathf.Cos( ( (1/period)*2*Mathf.PI) * curTime); break;
		case trigType.TANGENT : value = amplitude * Mathf.Tan( ( (1/period)*2*Mathf.PI) * curTime); break;
		}
		return value;
	}



	//
	//Returns the value f(currentTime) of the sin wave, must call update first to calculat this value
	//
	public float getValue() {
		return value;
	}



    //
    //Sets amplitude
    //
    public void setAmplitude(float value)
    {
        amplitude = value;
    }
}

