﻿// Ben & Matt

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIX_Crosshair : MonoBehaviour {
    // Camera
    Camera worldCamera = new Camera();
    Vector3 middleScreen;
    CAM_ScreenResize camResize;

    // Ray
    Ray ray;
    RaycastHit[] hits;
    public static float maxDistance = 750;
    public static LayerMask layermask;

    // Target
    GameObject crosshair;
    Renderer crosshairRend;
    Texture[] textures;
    string _target = "invalid";
    Vector3 _targetPos;
    GameObject _targetObject;

    public string target
    {
        get { return _target; }
    }

    public Vector3 targetPos
    {
        get { return _targetPos; }
    }

    public GameObject targetObject
    {
        get { return _targetObject; }
    }

    // Use this for initialization
    void Start () {
        layermask = (1 << 11) | (1 << 12) | (1 << 13) | (1 << 15);
        worldCamera = GameObject.Find("Camera").GetComponent<Camera>();
        middleScreen = new Vector3(worldCamera.pixelWidth / 2, worldCamera.pixelHeight / 2, worldCamera.nearClipPlane);
        camResize = GameObject.Find("Scripts").GetComponent<CAM_ScreenResize>();

        textures = new Texture[15];
        textures[0] = Resources.Load("UI/Textures/crosshair_dot1") as Texture;
        textures[1] = Resources.Load("UI/Textures/crosshair_dot2") as Texture;
        textures[2] = Resources.Load("UI/Textures/crosshair_X1") as Texture;
        textures[3] = Resources.Load("UI/Textures/crosshair_X2") as Texture;
        textures[4] = Resources.Load("UI/Textures/X") as Texture;
        textures[5] = Resources.Load("UI/Textures/crosshair_dot3") as Texture;
        textures[6] = Resources.Load("UI/Textures/crosshair_dot4") as Texture;
        textures[7] = Resources.Load("UI/Textures/crosshair_dot5") as Texture;
        textures[8] = Resources.Load("UI/Textures/crosshair_dot6") as Texture;
        textures[9] = Resources.Load("UI/Textures/crosshair_dot7") as Texture;
        textures[10] = Resources.Load("UI/Textures/crosshair_dot8") as Texture;
        textures[11] = Resources.Load("UI/Textures/crosshair_dot9") as Texture;
        textures[12] = Resources.Load("UI/Textures/crosshair_dot10") as Texture;
        textures[13] = Resources.Load("UI/Textures/Attack_Cursors_Revised_Green") as Texture;
        textures[14] = Resources.Load("UI/Textures/Attack_Cursors_Revised_Pink") as Texture;


        crosshair = GameObject.Find("Crosshair");
        crosshairRend = crosshair.GetComponent<Renderer>();
        _target = "invalid";
        _targetPos = new Vector3();
        _targetObject = new GameObject();
    }
	
	// Update is called once per frame
	void Update () {
        middleScreen = camResize.midScreen;
        if (SelectionManager.isInSelectMode)
        {
            checkTarget(INP_MouseCursor.position);
        }
        else
        {
            checkTarget(middleScreen);
        }
    }

    void checkTarget(Vector3 position)
    {
        ray = worldCamera.ScreenPointToRay(position);
        //if (Physics.Raycast(ray, out hit, maxDistance, layermask))
        hits = Physics.SphereCastAll(ray, 1, maxDistance, layermask);
        if (hits.Length > 0)
        {
            foreach (var hit in hits)
            {
                if (hit.transform.tag == "Minion")
                {
                    ENT_Body body = hit.transform.gameObject.GetComponent<ENT_Body>();
                    /*
                    if (body.isAlly && (body.entity_type == ENT_DungeonEntity.monsterTypes.slowMinion)) // SLOW MINION
                    {
                        _target = "special";
                        _targetObject = hit.transform.gameObject;
                        if (_targetObject.GetComponent<ENT_MinionAbility>().AllyCoolDownTimer <= 0 && !body.isConfused())
                        {
                            crosshairRend.material.mainTexture = textures[11];
                        }
                        else
                        {
                            crosshairRend.material.mainTexture = textures[4];
                        }
                        crosshair.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
                        break;
                    }
                    else if (body.isAlly && (body.entity_type == ENT_DungeonEntity.monsterTypes.turretMinion)) // TURRET MINION
                    {
                        _target = "special";
                        _targetObject = hit.transform.gameObject;
                        if (_targetObject.GetComponent<ENT_MinionAbility>().AllyCoolDownTimer <= 0 && !body.isConfused())
                        {
                            crosshairRend.material.mainTexture = textures[10];
                        }
                        else
                        {
                            crosshairRend.material.mainTexture = textures[4];
                        }
                        crosshair.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
                        break;
                    }
                    else if (body.isAlly && (body.entity_type == ENT_DungeonEntity.monsterTypes.necroMinion)) // NECRO MINION
                    {
                        _target = "special";
                        _targetObject = hit.transform.gameObject;
                        if (_targetObject.GetComponent<ENT_MinionAbility>().AllyCoolDownTimer <= 0 && !body.isConfused())
                        {
                            crosshairRend.material.mainTexture = textures[8];
                        }
                        else
                        {
                            crosshairRend.material.mainTexture = textures[4];
                        }
                        crosshair.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
                        break;
                    }
                    else if (body.isAlly && (body.entity_type == ENT_DungeonEntity.monsterTypes.confusionMinion)) // CONFUSION MINION
                    {
                        _target = "special";
                        _targetObject = hit.transform.gameObject;
                        if (_targetObject.GetComponent<ENT_MinionAbility>().AllyCoolDownTimer <= 0 && !body.isConfused())
                        {
                            crosshairRend.material.mainTexture = textures[12];
                        }
                        else
                        {
                            crosshairRend.material.mainTexture = textures[4];
                        }
                        crosshair.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
                        break;
                    }
                    */
                    /* if (body.isAlly && (body.entity_type == ENT_DungeonEntity.monsterTypes.slowMinion
                        || body.entity_type == ENT_DungeonEntity.monsterTypes.turretMinion
                        || body.entity_type == ENT_DungeonEntity.monsterTypes.necroMinion
                        || body.entity_type == ENT_DungeonEntity.monsterTypes.confusionMinion))
                    {
                        _target = "special";
                        _targetObject = hit.transform.gameObject;
                        if (_targetObject.GetComponent<ENT_MinionAbility>().AllyCoolDownTimer <= 0)
                        {
                            crosshairRend.material.mainTexture = textures[3];
                        }
                        else
                        {
                            crosshairRend.material.mainTexture = textures[4];
                        }
                        crosshair.transform.localScale = new Vector3(0.003f, 0.003f, 0.003f);
                        break;
                    } */
                    /*else*/ if (!body.isAlly)
                    {
                        _target = "enemy";
                        _targetObject = hit.transform.gameObject;
                        crosshairRend.material.mainTexture = textures[14];
                        crosshair.transform.localScale = new Vector3(0.003f, 0.003f, 0.003f);
                    }
                }
                else if (hit.transform.tag == "enemyStructure")
                {
                    _target = "enemy";
                    _targetObject = hit.transform.gameObject;
                    crosshairRend.material.mainTexture = textures[14];
                    crosshair.transform.localScale = new Vector3(0.003f, 0.003f, 0.003f);
                    //print("hovering over structure");
                }
                else if (hit.transform.tag == "splicer")
                {
                    _target = "splicer";
                    //_targetPos = hit.point;
                    crosshairRend.material.mainTexture = textures[5];
                    crosshair.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
                    //Debug.Log("UI Splicer Working");
                }
                else if (hit.transform.tag == "ground")
                {
                    _target = "neutral";
                    _targetPos = hit.point;
                    crosshairRend.material.mainTexture = textures[13];
                    crosshair.transform.localScale = new Vector3(0.003f, 0.003f, 0.003f);
                }
            }
            
        }
        else
        {
            _target = "";
            crosshairRend.material.mainTexture = textures[0];
            crosshair.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
        }
    }
}
