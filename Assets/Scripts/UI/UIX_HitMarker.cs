﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIX_HitMarker : MonoBehaviour {

    float value = 0;
    Image outLineImage;
    Image innerImage;
    static UIX_HitMarker hitMarker;
    float scaleMin = .8f;
    float scaleMax = 2.2f;

	// Use this for initialization
	void Start () {
        outLineImage = GetComponent<Image>();
        innerImage = GameObject.Find("HitMarker").GetComponent<Image>();
        hitMarker = this;
	}
	
	// Update is called once per frame
	void Update () {
        value = Mathf.Clamp(hitMarker.value - TME_Manager.getDeltaTime(TME_Time.type.game), 0, 1);

        // set color
        Color c = outLineImage.color;
        c.a = value;
        outLineImage.color = c;
        c = innerImage.color;
        c.a = value;
        innerImage.color = c;

        // set scale
        outLineImage.rectTransform.localScale = Vector3.one * (scaleMin + (scaleMax-scaleMin)*value);
	}



    //
    // Registers hit to hit marker
    //
    public static void registerHit()
    {
        float addVal = .5f - 0.45f * hitMarker.value;
        hitMarker.value = Mathf.Clamp(hitMarker.value + addVal, 0, 1);
    }
}
