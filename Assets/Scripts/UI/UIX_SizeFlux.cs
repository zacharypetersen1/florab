﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIX_SizeFlux : MonoBehaviour {

    public float size = .1f;
    public float speed = 1f;
    TrigWave wave;
    RectTransform t;
    Vector3 startScale;

	// Use this for initialization
	void Start () {
        wave = new TrigWave(TrigWave.trigType.SINE, speed, size, 0);
        t = GetComponent<RectTransform>();
        startScale = t.localScale;
    }
	
	// Update is called once per frame
	void Update () {
        wave.update(Time.deltaTime);
        t.localScale = startScale * (1 + wave.getValue());
	}
}
