﻿// Zach

using UnityEngine;
using System.Collections;



public class UIX_AbilityTrigger : MonoBehaviour {



    public bool isLeft;
    Vector3 baseScale;



    // runs once to initialize
    void Start()
    {
        baseScale = transform.localScale;
    }



    //
    // runs once per frame
    //
    void Update()
    {
        float scalar = INP_PlayerInput.get2DAxis("Abilities", false)[isLeft ? 0 : 1];
        transform.localScale = baseScale * (1 + (scalar / 2));
    }
}
