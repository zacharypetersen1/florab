﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIX_LowHealth : MonoBehaviour {


    ENT_PlayerBody playerBody;
    Image img;

    // Use this for initialization
    void Start () {
        playerBody = GameObject.Find("Player").GetComponent<ENT_PlayerBody>();
        img = GetComponent<Image>();
    }
	
	// Update is called once per frame
	void Update () {
        float ratio = playerBody.CL / playerBody.CL_MAX;
        Color col = img.color;
        col.a = ratio <= 0.5f ? 1 - UTL_Math.mapToNewRange(ratio, 0f, .5f, 0, 1) : 0;
        img.color = col;
	}
}
