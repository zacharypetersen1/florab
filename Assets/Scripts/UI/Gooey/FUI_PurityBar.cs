﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Based on FUI_FloatingUi, but adapted to use sprites
public class FUI_PurityBar : MonoBehaviour {

    //This script is tailored toward purifying dungeon entities.
    public Camera cam;
    public float offset = 4.0f;
    public GameObject background_bar; //This should be set in the component to the bar with the script
    //The order in layer should be 1 for the background bar
    public GameObject cl_bar; //This should be set in the component to a child bar
    //The order in layer for the child bar should be 2
    public GameObject purity_bar; //This should be set in the component to a child bar
    //The order in layer for the child bar should be 3

    void Awake()
    {
        if (cam == null)
        {
            cam = GameObject.Find("Camera").GetComponent<Camera>();
        }
        background_bar.transform.localScale = new Vector3(2.7f, 0.4f, 1);
    }

	// Use this for initialization
	protected virtual void Start () {
        //offset = 4.0f;
        //background_bar.transform.localScale = new Vector3(2.7f, 0.4f, 1);
        purity_bar.transform.localScale = new Vector3(0.0f, 0.35f, 1); //The child also inherits the scale of the background bar, if set as its child
        cl_bar.transform.localScale = new Vector3(0.8f, 0.8f, 1);
    }

    // Update is called once per frame
    protected virtual void Update () {
        purity_bar.transform.LookAt(purity_bar.transform.position + cam.transform.rotation * Vector3.forward, cam.transform.rotation * Vector3.down);
        cl_bar.transform.LookAt(cl_bar.transform.position + cam.transform.rotation * Vector3.forward, cam.transform.rotation * Vector3.down);
        background_bar.transform.LookAt(background_bar.transform.position + cam.transform.rotation * Vector3.forward, cam.transform.rotation * Vector3.down);
        setOffsets();

    }

    public virtual void hide()
    {
        purity_bar.SetActive(false);
        cl_bar.SetActive(false);
        background_bar.SetActive(false);
    }

    public void hidePurity()
    {
        purity_bar.SetActive(false);
    }

    public void showPurity()
    {
        purity_bar.SetActive(true);
    }

    public virtual void show()
    {
        purity_bar.SetActive(true);
        cl_bar.SetActive(true);
        background_bar.SetActive(true);
    }

    public virtual void setOffsets()
    {
        //Get the parent of the backgroundbar object
        var target = transform.parent;
        purity_bar.transform.position = new Vector3(target.transform.position.x, target.transform.position.y + offset, target.transform.position.z);
        cl_bar.transform.position = new Vector3(target.transform.position.x, target.transform.position.y + offset, target.transform.position.z);
        background_bar.transform.position = new Vector3(target.transform.position.x, target.transform.position.y + offset, target.transform.position.z);
        scaleCLToValue(transform.parent.GetComponent<ENT_DungeonEntity>().CL, transform.parent.GetComponent<ENT_DungeonEntity>().CL_MAX);
    }

    public virtual void subUpdateNoCLNoOffset()
    {
        //Get the parent of the backgroundbar object
        var target = transform.parent;
        purity_bar.transform.position = new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z);
        cl_bar.transform.position = new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z);
        background_bar.transform.position = new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z);
        //scaleCLToValue(transform.parent.GetComponent<ENT_DungeonEntity>().CL, transform.parent.GetComponent<ENT_DungeonEntity>().CL_MAX);
        purity_bar.transform.LookAt(purity_bar.transform.position + cam.transform.rotation * Vector3.forward, cam.transform.rotation * Vector3.down);
        cl_bar.transform.LookAt(cl_bar.transform.position + cam.transform.rotation * Vector3.forward, cam.transform.rotation * Vector3.down);
        background_bar.transform.LookAt(background_bar.transform.position + cam.transform.rotation * Vector3.forward, cam.transform.rotation * Vector3.down);
    }

    //This function shrinks the bar's x scale based on the variable
    public void scalePurityToValue(float value, float max)
    {
        float new_value = value/max;
        //Left justify the bar. Must scale percentage to pixels
        float missing_val = (max - value)/max;
        float cl_scale = cl_bar.transform.localScale.x;
        purity_bar.transform.localScale = new Vector3(new_value*cl_scale, 0.35f, 1);
        purity_bar.transform.localPosition = new Vector3((cl_bar.transform.localPosition.x)+(missing_val*0.5f * cl_scale), 0, 0);
        
    }

    //This function shrinks the bar's x scale based on the variable
    public void scaleCLToValue(float value, float max, bool vertical = false)
    {
        float new_value = value / max;
        //Left justify the bar. Must scale percentage to pixels
        float missing_val = (max - value) / max;
        if (vertical)
        {
            cl_bar.transform.localPosition = new Vector3(0, missing_val * 0.5f * 0.8f, 0);
            cl_bar.transform.localScale = new Vector3(0.8f, new_value * 0.8f, 1);
        }
        else
        {
            cl_bar.transform.localPosition = new Vector3(missing_val * 0.5f * 0.8f, 0, 0);
            cl_bar.transform.localScale = new Vector3(new_value * 0.8f, 0.8f, 1);
        }
        
        
    }
}
