﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FUI_FloatingUI : MonoBehaviour {

    public Material material;
    public GameObject target;
    public Camera cam;
    public int offset = 4;
    public GameObject fui;

    public virtual void Awake()
    {
        if (cam == null)
        {
            cam = GameObject.Find("Camera").GetComponent<Camera>();
        }
        if (target == null)
        {
            target = gameObject;
        }
    }

    // Use this for initialization
    public virtual void Start() {
        fui = (GameObject)MonoBehaviour.Instantiate(Resources.Load("FloatingUI/Instructions"));
        setOffset(fui);
        fui.GetComponent<Renderer>().material = material;
        hide();
	}

    public virtual void hide()
    {
        fui.SetActive(false);
    }

    public virtual void show()
    {
        fui.SetActive(true);
    }
	
	// Update is called once per frame
	public void update () {
        fui.transform.LookAt(fui.transform.position + cam.transform.rotation * Vector3.forward, cam.transform.rotation * Vector3.down);
        fui.transform.Rotate(new Vector3(0, 0, 180));
        setOffset(fui);
    }

    public virtual void setOffset(GameObject obj)
    {
        obj.transform.position = new Vector3(target.transform.position.x, target.transform.position.y + offset, target.transform.position.z);
    }
}
