﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FUI_ConfusionEffect : FUI_Default {

    // Update is called once per frame

    Quaternion rotate;
    public float rotationSpeed = 75;

    protected override void Start()
    {
        base.Start();
        rotate = transform.rotation;
    }
    protected override void Update()
    {
        rotate *= Quaternion.Euler(Vector3.forward * TME_Manager.getDeltaTime(TME_Time.type.enemies) * rotationSpeed);
        transform.rotation = Quaternion.LookRotation(cam.transform.position - transform.position) * rotate; 
    }

}
