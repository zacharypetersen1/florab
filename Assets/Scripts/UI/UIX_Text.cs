﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIX_Text : MonoBehaviour {

    public Camera cam;


    // Use this for initialization
    void Start () {
        cam = GameObject.Find("Camera").GetComponent<Camera>();
    }
	
	// Update is called once per frame
	void Update () {
        //gameObject.transform.rotation = cam.transform.rotation;
        gameObject.transform.LookAt(cam.transform);
	}
}
