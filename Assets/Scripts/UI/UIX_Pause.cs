﻿// Ben
// https://www.sitepoint.com/adding-pause-main-menu-and-game-over-screens-in-unity/
// http://answers.unity3d.com/questions/1173303/how-to-check-which-scene-is-loaded-and-write-if-co.html
// http://answers.unity3d.com/questions/1011523/first-selected-gameobject-not-highlighted.html

// Had to manually include a few so editor wouldn't complain
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class UIX_Pause : MonoBehaviour
{

    EventSystem es;
    GameObject[] pauseObjects;
    GameObject[] controls;
    GameObject pauseHolder;
    GameObject controlHolder;

    GameObject controlButton;

    //bool controlsClicked = false;
    bool paused = false;
    string sceneName;
    bool firstFrame = false;

    // Use this for initialization
    void Start()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.name;

        pauseObjects = GameObject.FindGameObjectsWithTag("showOnPause");
        controls = GameObject.FindGameObjectsWithTag("showOnControls");

        pauseHolder = GameObject.Find("Pause_Layer");
        controlHolder = GameObject.Find("Controller_Layer");

        controlButton = GameObject.Find("Back_Button");

        es = GameObject.Find("EventSystem").GetComponent<EventSystem>();
        //storyMenu = GameObject.Find("Story_Menu").GetComponent<EFX_StoryMenu>();

        pauseHolder.transform.localScale = new Vector3(1, 1, 1);
        controlHolder.transform.localScale = new Vector3(1, 1, 1);

        hidePaused();
        initialHideControls();
        showPaused(); // hiding the UI initially doesn't work so I had to do this. Sorry
        initialShowControls();
        hidePaused();
        initialHideControls();
        //print("Start");
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (firstFrame) Time.timeScale = 1;
        if ((INP_PlayerInput.getButtonDown("Pause") || INP_PlayerInput.getButtonDown("Pause2")) && !EFX_StoryMenu.active) // ESC / U
        {
            if (!paused) // STOP TIME
            {
                //print("Hello world");
                //TME_Manager.setTimeScalar(TME_Time.type.game, 0);
                Time.timeScale = 0.00001f;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                showPaused();
            }
            else if (paused) // RESUME TIME
            {
                //TME_Manager.setTimeScalar(TME_Time.type.game, 1);
                Time.timeScale = 1;
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                hidePaused();
                initialHideControls();
            }
            
            paused = !paused;
        }
        firstFrame = false;
    }

    public void Resume()
    {
        //TME_Manager.setTimeScalar(TME_Time.type.game, 1);
        //print("resumed");
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        hidePaused();
        paused = false;
    }

    public void Reload()
    {
        Cursor.visible = false;
        SceneManager.LoadScene(sceneName);
        Time.timeScale = 1;
    }

    public void Quit()
    {
        //Application.Quit();
        SceneManager.LoadScene("Main_Menu_Scene");
        Time.timeScale = 1;
    }

    //shows objects with ShowOnPause tag
    public void showPaused()
    {
        // shows initial menu
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(true);
        }
        //es.SetSelectedGameObject(null);
        //es.SetSelectedGameObject(es.firstSelectedGameObject);
    }

    //hides objects with ShowOnPause tag
    public void hidePaused()
    {
        // hides initial menu
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(false);
        }
    }

    public void showControls()
    {
        hidePaused();
        // shows initial menu
        foreach (GameObject h in controls)
        {
            h.SetActive(true);
        }
        //es.SetSelectedGameObject(null);
        //es.SetSelectedGameObject(controlButton);
    }

    public void hideControls()
    {
        // shows initial menu
        foreach (GameObject h in controls)
        {
            h.SetActive(false);
        }
        showPaused();
    }



    public void initialShowControls()
    {
        // shows initial menu
        foreach (GameObject h in controls)
        {
            h.SetActive(true);
        }
    }

    public void initialHideControls()
    {
        // shows initial menu
        foreach (GameObject h in controls)
        {
            h.SetActive(false);
        }
    }

}

