﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIX_HealthBar : MonoBehaviour {
    UIX_Bar bar;
    UIX_Bar purityBar;
    ENT_Body body;
    GameObject minion;

    bool showHealth = true;

    PLY_AllyManager allyMan;
    int allyCount;
    int enemyCount;

    bool currentPurity;

    // Use this for initialization
    void Start () {
        allyMan = GameObject.Find("Player").GetComponent<PLY_AllyManager>();
        allyCount = allyMan.getAllAllies.Count;
        enemyCount = allyMan.getAllEnemies.Count;
        bar = GetComponent<UIX_Bar>();
        purityBar = transform.parent.transform.GetChild(1).GetComponent<UIX_Bar>();
        body = GetComponentInParent<ENT_Body>();
        currentPurity = body.isAlly;
        minion = transform.parent.parent.gameObject;
        if (body == null)
        {
            Debug.LogError("health bar not set up right on " + gameObject.transform.parent.transform.parent.name);
        }
        else
        {
            bar.MaxValue = body.CL_MAX;
            bar.CurrentValue = body.CL;
            purityBar.MaxValue = body.CL_MAX;
        }
	}
	
    public void toggleBar(bool isEnable)
    {
        showHealth = isEnable;
        toggleHealthVisible();
    }

	// Update is called once per frame
	void Update () {
        bar.CurrentValue = body.CL;
        purityBar.CurrentValue = body.purityValue;
        /*
        if (INP_PlayerInput.getButtonDown("ToggleHealth"))
        {
            showHealth = !showHealth;
            toggleHealthVisible();
        }*/
        if(currentPurity != body.isAlly)
        {
            toggleHealthVisible();
            currentPurity = body.isAlly;
        }
        if (body.Purity)
        {
            purityBar.gameObject.SetActive(false);
        }
        
    }

    void toggleHealthVisible()
    {
        GameObject healthBar = transform.parent.gameObject;
        GameObject purityBar = transform.parent.FindChild("Purity_Bar").gameObject;
        SpriteRenderer healthRend = healthBar.GetComponent<SpriteRenderer>();
        SpriteRenderer CLRend = gameObject.GetComponent<SpriteRenderer>();
        SpriteRenderer purityRend = purityBar.GetComponent<SpriteRenderer>();
        Color healthColor = healthRend.color;
        Color CLColor = CLRend.color;
        Color purityColor = purityRend.color;
        if (showHealth)
        {
            healthColor.a = 100f;
            CLColor.a = 100f;
            purityColor.a = 100f;
        }
        else
        {
            healthColor.a = 0;
            CLColor.a = 0;
            purityColor.a = 0;
        }
        healthRend.color = healthColor;
        CLRend.color = CLColor;
        purityRend.color = purityColor;
    }
}
