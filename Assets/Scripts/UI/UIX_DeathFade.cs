﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIX_DeathFade : MonoBehaviour
{
    public Image img;
    GameObject imageHolder;
    
    public static bool triggerFade = false;
    private ENT_PlayerBody playerBody;
    public float startDelay = 3;
    public float holdDelay = 3;
    public float endDelay = 3;

    // INSTRUCTIONS
    //
    // TO START, SET startFade = true
    //

    void Start()
    {
        imageHolder = GameObject.Find("Image_Black");
        imageHolder.transform.localScale = new Vector3(20, 20, 1);
        img.color = new Color(1, 1, 1, 0);
        playerBody = GameObject.Find("Player").GetComponent<ENT_PlayerBody>();
    }

    void Update()
    {
        if (triggerFade)
        {
            StartCoroutine(FadeImage());
            triggerFade = false;
        }
    }

    IEnumerator FadeImage()
    {

        startFadeIn();

        for (float i = 0; i <= startDelay; i += Time.deltaTime)
        {
            // set color with [i/startDelay] as alpha
            img.color = new Color(1, 1, 1, i /startDelay);
            yield return null;
        }

        endFadeIn();

        // hold on black screen for HOLDDELAY
        yield return new WaitForSeconds(holdDelay);

        startFadeOut();

        // fade from opaque to transparent

        // loop over ENDDELAY second(s) backwards
        for (float j = endDelay; j >= 0; j -= Time.deltaTime)
        {
            // set color with [i/endDelay] as alpha
            img.color = new Color(1, 1, 1, j / endDelay);
            yield return null;
        }

        endFadeOut();
        StopCoroutine("FadeImage");

    }

    void startFadeIn()
    {
        UIX_DamageIndicator.resetFlashes();
        UIX_DamageIndicator.setEnabled(false);
    }

    void endFadeIn()
    {
        AIR_Respawn.Respawn();
    }

    void startFadeOut()
    {

    }

    void endFadeOut()
    {
        UIX_DamageIndicator.setEnabled(true);
        playerBody.invulnerable = false;
    }

}
