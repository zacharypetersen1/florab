﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

//attached to main camera

public class UIX_MainMenu : MonoBehaviour
{

    /* public Texture backgroundTexture;
    
    public Texture startButton;
    public Texture creditsButton;

    public float guiPlacementX1;
    public float guiPlacementY1;

    public float guiPlacementX2;
    public float guiPlacementY2;

    public bool customButtons = true; */

    public bool fadeTime1 = false;
    public bool fadeTime2 = false;
    public bool fadeTime3 = false;

    MUS_Menu music;

    //private IEnumerator coroutine;

    //KNOWN BUG: Plays button sound effect at start in continuous_music.cs

    void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        music = GameObject.Find("MusicMenu").GetComponent<MUS_Menu>();
        music.playMusic = true;
        //music.resetMusic();
    }

    void Update()
    {
        //coroutine = fadeStart();
        StartCoroutine(fadeStart());
        /* if (INP_PlayerInput.getButton("Pause"))
        {
            fadeTime1 = true;
        } */
    }


    /* void OnGUI()
    {
        if (customButtons)
        {
            // NEW WAY
            //display background texture
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backgroundTexture);
            //display menu buttons
            //Start Campaign was clicked
            if (GUI.Button(new Rect(Screen.width * guiPlacementX1, Screen.height * guiPlacementY1, Screen.width * .25f, Screen.height * .1f), startButton))
            {
                fadeTime1 = true;
                //GetComponent<AudioSource>().Play();
            }
            //Credits/Cited Works clicked
            if (GUI.Button(new Rect(Screen.width * guiPlacementX2, Screen.height * guiPlacementY2, Screen.width * .25f, Screen.height * .1f), creditsButton))
            {
                fadeTime2 = true;
                //GetComponent<AudioSource>().Play();
            }
        }
        else
        {
            // OLD WAY
            //display background texture
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backgroundTexture);
            //display menu buttons
            //Start Campaign was clicked
            if (GUI.Button(new Rect(Screen.width * guiPlacementX1, Screen.height * guiPlacementY1, Screen.width * .25f, Screen.height * .1f), "Start Demo"))
            {
                fadeTime1 = true;
                //GetComponent<AudioSource>().Play();
            }
            //Credits/Cited Works clicked
            if (GUI.Button(new Rect(Screen.width * guiPlacementX2, Screen.height * guiPlacementY2, Screen.width * .25f, Screen.height * .1f), "Zach's Chefs"))
            {
                fadeTime2 = true;
                //GetComponent<AudioSource>().Play();
            }
        }
    } */

	public void fadeTimeGame()
    {
        fadeTime1 = true;
	}

	public void fadeTimeCredits(){
		fadeTime2 = true;
	}

    public void fadeTimeExit()
    {
        fadeTime3 = true;
    }

    private IEnumerator fadeStart()
    {
        if (fadeTime1)
        {
            //GetComponent<AudioSource>().Play();
            //music.stopMusic();
            float fadeTime = GameObject.Find("_fadeObject").GetComponent<UIX_Screen_Fade>().BeginFade(1);
            yield return new WaitForSeconds(fadeTime);
            SceneManager.LoadScene("Alpha");
        }
        if (fadeTime2)
        {
            //GetComponent<AudioSource>().Play();
            float fadeTime = GameObject.Find("_fadeObject").GetComponent<UIX_Screen_Fade>().BeginFade(1);
            yield return new WaitForSeconds(fadeTime);
            SceneManager.LoadScene("Credits_Scene");
        }
        if (fadeTime3)
        {
            float fadeTime = GameObject.Find("_fadeObject").GetComponent<UIX_Screen_Fade>().BeginFade(1);
            yield return new WaitForSeconds(fadeTime);
            Application.Quit();
        }
    }
}
//float fadeTime = GameObject.Find("_fadeObject").GetComponent<Fading>().BeginFade(1);
// yield return new WaitForSeconds(fadeTime);