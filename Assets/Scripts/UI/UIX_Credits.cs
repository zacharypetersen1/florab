﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIX_Credits : MonoBehaviour {

    GameObject creditsScroller;
    GameObject thanksForPlaying;
    public Vector3 speed;
    public Vector3 speed2;

	void Start () {
        creditsScroller = GameObject.Find("Credits_Text");
        thanksForPlaying = GameObject.Find("ThanksForPlaying_Text");


        speed = new Vector3(0, 1f);

        StartCoroutine(scroll());
    }

    void Update()
    {
        // Increase speed on every left click
        /* if (Input.GetMouseButtonDown(0))
        {
            speed += speed;
        }*/
        if (Input.GetMouseButton(0))
        {
            speed = new Vector3(0, 10f);
        }
        else
        {
            speed = new Vector3(0, 1f);
        }
    }

    IEnumerator scroll()
    {
        while(creditsScroller.transform.position.y < 4700)
        {
            creditsScroller.transform.position += speed;
            yield return null;
        }
        if (creditsScroller.transform.position.y >= 4700)
        {
            thanksForPlaying.transform.localPosition = new Vector3(0f, 0f, 0f);
            yield return new WaitForSeconds(5f);
            SceneManager.LoadScene("Main_Menu_Scene");
        }
    }

}
