﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class intPair
{

    public int value1;
    public int value2;

    public intPair(int o, int p)
    {
        this.value1 = o;
        this.value2 = p;
    }

    public int CompareTo(intPair other)
    {
        if (this.value1 < other.value1) return -1;
        else if (this.value1 > other.value1) return 1;
        else if (this.value2 < other.value1) return -1;
        else if (this.value2 > other.value2) return 1;
        else return 0;
    }

    public override bool Equals(System.Object obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }
        else
        {
            intPair other = (intPair)obj;
            return (this.value1 == other.value1) && (this.value2 == other.value2);
        }
    }

    public override int GetHashCode()
    {
        //return (value1 ^ value2) ^ value1;
        return (value1 << 4) + (value2);
    }

}
