﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pair<T, Y> {

    public T value1;
    public Y value2;

    public Pair(T o, Y p)
    {
        this.value1 = o;
        this.value2 = p;
    }

}
