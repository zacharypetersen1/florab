﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIC_StructureParticleSystemDisabler : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "enemyStructure")
        {
            ENT_Structure structure = other.gameObject.GetComponent<ENT_Structure>();
            if (structure != null) structure.SetPSEnabled(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "enemyStructure")
        {
            ENT_Structure structure = other.gameObject.GetComponent<ENT_Structure>();
            if (structure != null) structure.SetPSEnabled(false);
        }
    }
}
