﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIC_Zone {

    ArrayList myMinions;
    public AIC_Zone[] myNeighbors;
    bool _enabled = true;
    GameObject indicator;
    public int x;
    public int y;
    bool debug = false;
    public bool enabled
    {
        get { return _enabled; }
        set
        {
            setEnabled(value);
        }
    }

    public AIC_Zone(int x, int y)
    {
        myMinions = new ArrayList();
        myNeighbors = new AIC_Zone[8]; //number of possible neighbors
        this.x = x;
        this.y = y;
    }

    public void addMinionToZone(GameObject minion)
    {
        myMinions.Add(minion);
    }

    public void removeMinionFromZone(GameObject minion)
    {
        myMinions.Remove(minion);
    }

    public void disableFirstFrame()
    {
        _enabled = false;
        foreach (GameObject minion in myMinions)
        {
            minion.SetActive(false);
        }
    }

    public void setEnabled(bool enable)
    {
        if (debug)
        {
            if (enable == true)
            {
                if (indicator == null)
                {
                    indicator = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                    indicator.transform.position = new Vector3(x * 128, 100, y * 128);
                    indicator.transform.localScale = new Vector3(100, 100, 100);
                    indicator.GetComponent<Collider>().enabled = false;
                }
            }
            else
            {
                if (indicator != null) GameObject.Destroy(indicator);
            }
        }
        if (_enabled == enable) return;
        _enabled = enable;
        
        foreach (GameObject minion in myMinions)
        {

            setMinionEnabled(minion);
        }
    }

    public void setMinionEnabled (GameObject minion)
    {
        //Debug.Log("setting status of " + minion.name + " to " + _enabled);
        //Debug.Log(_enabled + " " + minion.name);
        if (_enabled)
        {
            minion.SetActive(true);
            ENT_Body body = minion.GetComponent<ENT_Body>();
            body.SetPSEnabled(false);
            Collider col = minion.GetComponent<Collider>();
            col.enabled = false;
            col.enabled = true;
            Animator anim = minion.GetComponentInChildren<Animator>();
            anim.SetBool("isAppear", true);

        }
        else
        {
            Animator anim = minion.GetComponentInChildren<Animator>();
            anim.SetBool("isAppear", false);
            ENT_Body body = minion.GetComponent<ENT_Body>();
            body.disableAI = true;
            body.reappear = false;
        }
    }

    public bool isNeighbor(AIC_Zone zone)
    {
        foreach (AIC_Zone z in myNeighbors)
        {
            if (z == zone) return true;
        }
        return false;
    }
}
