﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class AIC_ZoneManager {

    private static Dictionary<intPair, AIC_Zone> Zone_Manager;
    public const int minX = -32;
    public const int maxX = 32;
    public const int minY = -32;
    public const int maxY = 32;
    public const int cellSize = 128;

    private static AIC_Zone ActiveZone = null;

    private static List<AIC_Zone> activeZones = new List<AIC_Zone>();

    public static void createZones()
    {

        Zone_Manager = new Dictionary<intPair, AIC_Zone>();
        for (int i = minX; i < maxX; ++i)
        {
            for (int j = minY; j < maxY; ++j)
            {
                AIC_Zone zone = new AIC_Zone(i, j);
                intPair pair = new intPair(i, j);
                Zone_Manager.Add(pair, zone);
            }
        }
        for (int i = minX; i < maxX; ++i)
        {
            for (int j = minY; j < maxY; ++j)
            {
                intPair pair = new intPair(i, j);
                if (Zone_Manager.ContainsKey(pair))
                {
                    AIC_Zone zone = Zone_Manager[pair];
                    setNeighbors(zone, pair.value1, pair.value2);
                }
            }
        }
    }

    public static void deactivateZonesAtSceneStart()
    {
        foreach (AIC_Zone zone in Zone_Manager.Values)
        {
            zone.disableFirstFrame();
        }
    }

    private static void setNeighbors(AIC_Zone zone, int x, int y)
    {
        int count = 0;
        for (int i = -1; i < 2; ++i)
        {
            for (int j = -1; j < 2; ++j)
            {
                if (i == 0 && j == 0) continue;
                intPair pair = new intPair(x + i, y + j);
                if (Zone_Manager.ContainsKey(pair))
                {
                    AIC_Zone neighbor = Zone_Manager[pair];
                    zone.myNeighbors[count] = neighbor;
                }
                ++count;
            }
        }
    }

    public static AIC_Zone getZoneFromPosition(Vector3 position)
    {
        try
        {
            intPair index = getIndexFromPosition(position);
            return Zone_Manager[index];
        }
        catch (Exception e)
        {
            Debug.LogError(position + " does not correspond to a zone");
            throw e;
        }
    }

    private static intPair getIndexFromPosition(Vector3 position)
    {
        bool PosI = position.x >= 0;
        bool PosJ = position.z >= 0;
        int i, j;
        if (PosI)
        {
            i = Mathf.FloorToInt(position.x / cellSize);
        }
        else
        {
            i = Mathf.CeilToInt(position.x / cellSize);
        }
        if (PosJ)
        {
            j = Mathf.FloorToInt(position.z / cellSize);
        }
        else
        {
            j = Mathf.CeilToInt(position.z / cellSize);
        }

        return new intPair(i, j);
    }

    public static AIC_Zone putMinionInZone(GameObject minion)
    {
        //check if minion in Zone
        //remove minion from Zone
        ENT_Body body;
        try
        {
            body = minion.GetComponent<ENT_Body>();
            if (body.myZone != null)
            {
                body.myZone.removeMinionFromZone(minion);
            }
        }
        catch
        {
            Debug.LogError(minion + " is (probably) not a minion, cannot be put in a zone");
            return null;
        }
        AIC_Zone myZone = getZoneFromPosition(minion.transform.position);
        if (myZone == null)
        {
            return null;
        }
        myZone.addMinionToZone(minion);
        if(body.myZone != null)
        {
            if (body.myZone.enabled != myZone.enabled)
            {
                myZone.setMinionEnabled(minion);
            }
        }
        body.myZone = myZone;
        return myZone;
    }

    private static void disableAllZonesExceptThis(AIC_Zone z)
    {
        foreach (var pair in Zone_Manager)
        {
            AIC_Zone zone = pair.Value;
            if (zone == z) continue;
            if (z.isNeighbor(zone)) continue;
            zone.setEnabled(false);
        }
    }

    private static void disableAllZones()
    {
        foreach (var pair in Zone_Manager)
        {
            AIC_Zone zone = pair.Value;
            zone.setEnabled(false);
        }
    }

    private static void setActiveZones(AIC_Zone zone)
    {
        List<AIC_Zone> ac_zone = new List<AIC_Zone>();
        foreach (AIC_Zone neighbor in zone.myNeighbors)
        {
            if (activeZones.Contains(neighbor))
            {
                ac_zone.Add(neighbor);
                activeZones.Remove(neighbor);
                continue;
            }
            else
            {
                neighbor.setEnabled(true);
                ac_zone.Add(neighbor);
            }
        }

        foreach(AIC_Zone z in activeZones)
        {
            z.setEnabled(false);
        }

        activeZones = ac_zone;

        return; 
    }

    public static void setZonesActive(Vector3 playerPos)
    {
        AIC_Zone zone = getZoneFromPosition(playerPos);
        //STARTER CASE
        /*if (ActiveZone != null)
        {
            disableAllZones();
            zone.setEnabled(true);
            foreach (AIC_Zone neighbor in zone.myNeighbors)
            {
                neighbor.setEnabled(true);
                activeZones.Add(neighbor);
            }
            ActiveZone = zone;
        }*/
        if (ActiveZone != zone)
        {
            disableAllZonesExceptThis(zone);
            //disableAllZones();
            zone.setEnabled(true);
            foreach (AIC_Zone neighbor in zone.myNeighbors)
            {
                neighbor.setEnabled(true);
                //activeZones.Add(neighbor);
                //need to also put zone in activeZones
            }

            ActiveZone = zone;
        }
        //WHILE GAME IS RUNNING
        /*else if (ActiveZone != zone)
        {
            setActiveZones(zone);
            ActiveZone = zone;
        }*/
        
    }

    public static void updateZone(GameObject minion)
    {
        AIC_Zone zone = getZoneFromPosition(minion.transform.position);
        ENT_Body body = minion.GetComponent<ENT_Body>();
        if (body.myZone != zone)
        {
            //Debug.Log(minion.name + " is switching to zone " + zone.x + ", " + zone.y);
            AIC_Zone newZone = putMinionInZone(minion);
            if (newZone == null) Debug.LogError("failed to put: " + minion.name + " in a zone");
        }
    }

    public static void updateZone(ENT_Body body)
    {
        AIC_Zone zone = getZoneFromPosition(body.transform.position);
        if (body.myZone != zone)
        {
            //Debug.Log(body.gameObject.name + " is switching to zone " + zone.x + ", " + zone.y);
            AIC_Zone newZone = putMinionInZone(body.gameObject);
            if (newZone == null) Debug.LogError("failed to put: " + body.gameObject.name + " in a zone");
        }
    }
}
