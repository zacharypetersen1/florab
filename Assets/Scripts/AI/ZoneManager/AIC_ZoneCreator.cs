﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIC_ZoneCreator : MonoBehaviour {

    public GameObject player;
    bool ranOnce = false;

	// Use this for initialization
	void Awake () {
        AIC_ZoneManager.createZones();
        if (player == null) player = GameObject.Find("Player");

    }
	
	// Update is called once per frame
	void Update () {
        if (!ranOnce)
        {
            AIC_ZoneManager.deactivateZonesAtSceneStart();
            ranOnce = true;
        }
        AIC_ZoneManager.setZonesActive(player.transform.position);
	}
}
