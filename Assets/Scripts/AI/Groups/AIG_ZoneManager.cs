﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIG_ZoneManager : MonoBehaviour {

    public List<string> ZoneRecipes; //set this to the names of recipes this zone will produce. Repeat entries to have multiple warbands of the same type

    private List<AIG_Recipes.WarbandRecipes> warbandRecipesToMake = new List<AIG_Recipes.WarbandRecipes>();
    //private List<AIG_Recipes.WarbandRecipes> warbandRecipesCreated;

    public List<GameObject> Spawners; //spawners in zone, these will receive warband recipes
    //private List<AIG_Group> activeGroups;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void LoadRecipes()
    {
        foreach (string s in ZoneRecipes)
        {
            warbandRecipesToMake.Add(AIG_Recipes.getWBRecipeFromString(s));
        }
    }

    private void AssignRecipeToSpawner()
    {
        //AIG_Recipes.WarbandRecipes recipe;
        if (warbandRecipesToMake.Count <= 0)
        {
            print("Error: No warband recipes to assign");
            return;
        }
        //else 
        //choose a spawner
        //set their WBRecipe to make to WarbandRecipesToMake[0]
    }
}
