﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
//Harold

public static class AIG_Recipes {

	public enum Roles { Undef, Collector, Transport, Defender, Guard, Engineer, Warrior}

    public enum ChainGangRecipes { Undef, LargeCollector, SmallCollector, LoneCollector, SecureCollector,
                                   LargeTransport, SmallTransport, LoneTransport, SecureTransport,
                                   LargeDefender, SmallDefender, LoneDefender, EngineeringTeam}

    //new recipes go here and in the WBRecipeMap
    public enum WarbandRecipes { Undef}

    private static Dictionary<ChainGangRecipes, List<Roles>> CGRecipeMap = new Dictionary<ChainGangRecipes, List<Roles>>()
    {
        { ChainGangRecipes.LargeCollector,    new List<Roles>() { Roles.Collector, Roles.Collector, Roles.Collector, Roles.Guard, Roles.Guard } },
        { ChainGangRecipes.SmallCollector,    new List<Roles>() { Roles.Collector, Roles.Collector, Roles.Guard } },
        { ChainGangRecipes.LoneCollector,     new List<Roles>() { Roles.Collector } },
        { ChainGangRecipes.SecureCollector,   new List<Roles>() { Roles.Collector, Roles.Collector, Roles.Guard, Roles.Guard, Roles.Guard } },
        { ChainGangRecipes.LargeTransport,    new List<Roles>() { Roles.Transport, Roles.Transport, Roles.Transport, Roles.Guard, Roles.Guard } },
        { ChainGangRecipes.SmallTransport,    new List<Roles>() { Roles.Transport, Roles.Transport, Roles.Guard } },
        { ChainGangRecipes.LoneTransport,     new List<Roles>() { Roles.Transport } },
        { ChainGangRecipes.SecureTransport,   new List<Roles>() { Roles.Transport, Roles.Transport, Roles.Guard, Roles.Guard, Roles.Guard } },
        { ChainGangRecipes.LargeDefender,     new List<Roles>() { Roles.Defender, Roles.Defender, Roles.Defender, Roles.Defender, Roles.Defender } },
        { ChainGangRecipes.SmallDefender,     new List<Roles>() { Roles.Defender, Roles.Defender, Roles.Defender } },
        { ChainGangRecipes.LoneDefender,      new List<Roles>() { Roles.Defender } },
        { ChainGangRecipes.EngineeringTeam,   new List<Roles>() { Roles.Engineer, Roles.Engineer } },

    };

    //add new recipes here too
    private static Dictionary<string, ChainGangRecipes> CGRecipeStringMap = new Dictionary<string, ChainGangRecipes>()
    {
        {"LargeCollector",     ChainGangRecipes.LargeCollector},
        {"SmallCollector",     ChainGangRecipes.SmallCollector},
        {"LoneCollector",      ChainGangRecipes.LoneCollector},
        {"SecureCollector",    ChainGangRecipes.SecureCollector},
        {"LargeTransport",     ChainGangRecipes.LargeTransport},
        {"SmallTransport",     ChainGangRecipes.SmallTransport},
        {"LoneTransport",      ChainGangRecipes.LoneTransport},
        {"SecureTransport",    ChainGangRecipes.SecureTransport},
        {"LargeDefender",      ChainGangRecipes.LargeDefender},
        {"SmallDefender",      ChainGangRecipes.SmallDefender},
        {"LoneTransport",      ChainGangRecipes.LoneDefender},
        {"EngineeringTeam",    ChainGangRecipes.EngineeringTeam},
    };

    //new recipes go here and in the WarbandRecipes enum
    private static Dictionary<WarbandRecipes, List<Roles>> WBRecipeMap = new Dictionary<WarbandRecipes, List<Roles>>()
    {

    };

    private static Dictionary<string, WarbandRecipes> WBRecipeStringMap = new Dictionary<string, WarbandRecipes>()
    {

    };

    //set this to the Resources folder filepath for the minion you want assigned to each role.
    private static Dictionary<Roles, string> RoleMap = new Dictionary<Roles, string>()
    {
        { Roles.Collector, "Minions/WeakEnemy" },
        { Roles.Transport, "Minions/WeakEnemy" },
        { Roles.Defender,  "Minions/MushroomEnemy" },
        { Roles.Guard,     "Minions/BasicEnemy" },
        { Roles.Engineer,  "Minions/BasicEnemy" },
        { Roles.Warrior,   "Minions/BasicEnemy" },
    };

    //pass in a ChainGang recipe, get a list of all roles in the recipe
    public static List<Roles> getRolesFromCGRecipe(ChainGangRecipes recipe)
    {
        List<Roles> outList;
        if (CGRecipeMap.TryGetValue(recipe, out outList))
        {
            return outList;
        }
        else
        {
            Debug.Log("Recipe missing from CGRecipeMap " + recipe);
            return null;
        }
    }

    public static ChainGangRecipes getCGRecipeFromString(string str)
    {
        ChainGangRecipes outRecipe;
        if (CGRecipeStringMap.TryGetValue(str, out outRecipe))
        {
            return outRecipe;
        }
        else
        {
            Debug.Log("Recipe missing from CGRecipeStringMap " + str);
            return ChainGangRecipes.Undef;
        }
    }

    public static WarbandRecipes getWBRecipeFromString(string str)
    {
        WarbandRecipes outRecipe;
        if (WBRecipeStringMap.TryGetValue(str, out outRecipe))
        {
            return outRecipe;
        }
        else
        {
            Debug.Log("Recipe missing from WBRecipeStringMap " + str);
            return WarbandRecipes.Undef;
        }
    }

    //pass in a warband recipe, get a list of all roles in the recipe
    public static List<Roles> getRolesFromWBRecipe(WarbandRecipes recipe)
    {
        List<Roles> outList;
        if (WBRecipeMap.TryGetValue(recipe, out outList))
        {
            return outList;
        }
        else
        {
            Debug.Log("Recipe missing from WBRecipeMap " + recipe);
            return null;
        }
    }

    //pass in a role, get the path to the monster associated with the role
    public static string getFilePathFromRole(Roles role)
    {
        string outString;
        if (RoleMap.TryGetValue(role, out outString))
        {
            return outString;
        }
        else
        {
            Debug.Log("Role missing from RoleMap " + role);
            return null;
        }
    }

}
