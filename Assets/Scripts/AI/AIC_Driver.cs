﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIC_Driver : MonoBehaviour {


    public float sightAngle = 45;
    public float sightDistance = 5;
    private CHA_Motor motor;
    Transform playerTransform;


    //
    // Use this for initialization
    //
    void Start () {
        motor = gameObject.GetComponent<CHA_Motor>();
        playerTransform = GameObject.Find("Player").transform;
    }



    //
    // Update is called once per frame
    //
    void FixedUpdate () {
        Vector2 TwigPos = playerTransform.position;
        Vector2 MyPos = transform.position;
        Vector2 DirMonsterToTwig = TwigPos - MyPos;
        float MyRotation = gameObject.GetComponent<CHA_Motor>().getRotation();
        Vector2 DirFacing = UTL_Math.angleRadToUnitVec(MyRotation);
        float AngleToTwig = Mathf.Acos(Vector2.Dot(DirFacing, DirMonsterToTwig.normalized)); //gives in Radians

        if (UTL_Dungeon.checkLOSWalls(UTL_Math.vec3ToVec2(transform.position), UTL_Math.vec3ToVec2(playerTransform.position)))
        {
            if (DirMonsterToTwig.magnitude < sightDistance && AngleToTwig < Mathf.Deg2Rad * sightAngle)
            {
                //motor.moveTowardsPoint(TwigPos);
            }
        }
           

		//UTL_Dungeon.checkLOSWalls()
        //motor.moveTowardsPoint()

        // Use UTL_Math

        // Get Direction from Motor
        // Optional: Normalize our Vector
        // Take Vector2.dot product of 2 lines
	}
}
