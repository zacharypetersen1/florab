﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIC_AllyGroup
{
    PLY_AllyManager.allyState state;
    public PLY_AllyManager.allyState State
    {
        get { return state; }
    }
    List<GameObject> allyList;
    public List<GameObject> AllyList
    {
        get { return new List<GameObject>(allyList); }
    }
    Vector3 destination;
    public Vector3 Destination
    {
        get { return destination; }
    }
    PLY_AllyManager am;
    GameObject target;
    public GameObject Target
    {
        get { return target; }
    }
    float farFollowDistance;
    Vector3 midpoint;
    List<Vector3> goToPoints = null;
    static int[] dithervalues = new int[5] { -10, 5, 0, 5, 10 };
    Vector3 goToVec;
    List<GameObject> removeList = new List<GameObject>();
    string pathToGoToIndicator = "UI/Commands/GoToProjector";
    string pathToAttackIndicator = "UI/Commands/AttackProjector";
    public GameObject indicator = null;

    public AIC_AllyGroup(PLY_AllyManager.allyState newState, List<GameObject> myList, PLY_AllyManager allGroups, Vector3 pos, GameObject attackTarget = null)
    {
        state = newState;
        allyList = myList;
        am = allGroups;
        destination = pos;
        target = attackTarget;
        if (state == PLY_AllyManager.allyState.goTo)
        {
            indicator = UTL_Resources.cloneAtLocation(pathToGoToIndicator, pos + new Vector3(0, 3, 0), false);
        }
        if (state == PLY_AllyManager.allyState.attack)
        {
            indicator = UTL_Resources.cloneAsChild(pathToAttackIndicator, target);
            if (attackTarget.tag == "enemyStructure")
            {
                indicator.GetComponentInChildren<Projector>().orthographicSize = 8F;
            }
        }
    }

    public void sortList()
    {
        if (allyList.Count <= 0) return;
        switch (state)
        {
            case PLY_AllyManager.allyState.follow:
                destination = am.transform.position;
                allyList.Sort(compareByDist);
                break;
            case PLY_AllyManager.allyState.wait:
                //midpoint = getMidpoint(); //comment this
                allyList.Sort(compareByDist); //change to compareByDist
                allyList.Reverse();
                break;
            case PLY_AllyManager.allyState.goTo:
                //midpoint = getMidpoint(); //comment this
                allyList.Sort(compareByDist); //change to compareByDist
                allyList.Reverse();
                break;
            case PLY_AllyManager.allyState.activate:
                //am.compareToPos = position;
                //allyList.Sort(am.compareByDist);
                break;
            case PLY_AllyManager.allyState.attack:
                //something has to update pos here
                destination = target.transform.position;
                allyList.Sort(compareByDist);
                break;

        }
    }

    public GameObject this[int key]
    {
        get
        {
            return allyList[key];
        }
    }

    public int getCount()
    {
        return allyList.Count;
    }

    public void addToList(GameObject ally)
    {
        if (!AllyList.Contains(ally))
        {
            allyList.Add(ally);
            if (State == PLY_AllyManager.allyState.goTo || State == PLY_AllyManager.allyState.wait)
            {
                goToPoints = getGoToPoints();
                assignGoToPoints();
            }
        }
        
    }

    public void removeFromList(GameObject ally)
    {
        //Debug.LogError("removing " + ally.name + " from group");
        if (allyList.Contains(ally))
        {
            bool res = allyList.Remove(ally);
            if (!res) Debug.Log("Couldn't remove " + ally.name + " from list");
        }
        if (state != PLY_AllyManager.allyState.follow && allyList.Count <= 0)
        {
            if (indicator != null) GameObject.Destroy(indicator);
            bool res = am.commandGroups.Remove(this);
            if (!res) Debug.Log("Couldn't remove " + allyList + " from command Groups");
        }
    }


    public int compareByDist(GameObject a, GameObject b)
    {
        if (a == null || b == null) return 0;
        float distA = Vector3.Distance(a.transform.position, destination);
        float distB = Vector3.Distance(b.transform.position, destination);
        if (distA < distB) return -1;
        if (distA > distB) return 1;
        return 0;
    }

    public int compareByDistToMidpoint(GameObject a, GameObject b)
    {
        if (a == null || b == null) return 0;
        float distA = Vector3.Distance(a.transform.position, midpoint);
        float distB = Vector3.Distance(b.transform.position, midpoint);
        if (distA < distB) return -1;
        if (distA > distB) return 1;
        return 0;
    }

    public int compareGoToPoints(Vector3 a, Vector3 b)
    {
        if (a == null || b == null) return 0;
        //float distA = Vector3.Distance(a, midpoint);
        //float distB = Vector3.Distance(b, midpoint);
        float distA = Vector3.Distance(a, destination);  //change to midpoint
        float distB = Vector3.Distance(b, destination);  //change to midpoint
        if (distA < distB) return -1;
        if (distA > distB) return 1;
        return 0;
    }


    public float getFollowDistance(int index)
    {
        if (index < 0)
        {
            Debug.LogWarning("Negative index" + index);
        }
        if (state == PLY_AllyManager.allyState.follow)
        {
            if (index == 0) return (am.nearFollowDistance / 3) + (PLY_AllyManager.FOLLOWDISTANCESCALE * index);
            else return (am.nearFollowDistance / 3) + (PLY_AllyManager.FOLLOWDISTANCESCALE * index) + (PLY_AllyManager.FOLLOWDISTANCESCALE * (1 / (index * 4)));
        }
        /*else if (state == PLY_AllyManager.allyState.goTo || state == PLY_AllyManager.allyState.wait)
        {
            float dist = (-1 * getAverageFollowDistance()) + (PLY_AllyManager.FOLLOWDISTANCESCALE * index);
            if (Mathf.Abs(dist) < 3) dist *= 3;
            return dist;
            //return (-1 * getAverageFollowDistance()) + (PLY_AllyManager.FOLLOWDISTANCESCALE * index) + (2 * (1 / Mathf.Log(index)));
        }*/
        else return PLY_AllyManager.FOLLOWDISTANCESCALE * index;
    }

    public float getAverageFollowDistance()
    {
        return PLY_AllyManager.FOLLOWDISTANCESCALE * (getCount() / 2);
    }

    public void setFollowDistances()
    {
        sortList();
        removeList.Clear();
        //create circle size
        //generate points in circle
        //assign minions based on index to indexed points in circle
        setFarFollow();
        for (int i = 0; i < getCount(); ++i)
        {
            GameObject ally = allyList[i];
            if (ally == null)
            {
                removeList.Add(ally);
            }
            else
            {
                ENT_Body body = ally.GetComponent<ENT_Body>();
                if (body != null)
                {
                    body.myFollowDistance = getFollowDistance(i);
                    body.farFollowRadius = farFollowDistance;
                }
                else
                {
                    removeList.Add(ally);
                }
            }
            
        }
        if (removeList.Count > 0)
        {
            foreach (GameObject toRemove in removeList)
            {
                allyList.Remove(toRemove);
            }
        }
        //printFollowDistances();
    }

    public void printFollowDistances()
    {
        string output = "AllyGroup contains: ";
        foreach (GameObject ally in allyList)
        {
            output += ally.name + " : " + ally.GetComponent<ENT_Body>().myFollowDistance + ", ";
        }
        Debug.Log(output);
    }

    public void assignGoToPoints()
    {
        if (goToPoints == null)
        {
            goToPoints = getGoToPoints();
        }
        sortList();
        //sort goToPoints
        //midpoint = getMidpoint(); //uncomment this
        foreach(GameObject ally in AllyList)
        {
            ENT_Body body = ally.GetComponent<ENT_Body>();
            body.myGoToPos = chooseGoToPoint(ally);
            if(Vector3.Distance(body.myGoToPos,body.transform.position) > 150f || body.Agent.pathStatus != NavMeshPathStatus.PathComplete)
            {
                body.teleport(body.myGoToPos);
            }
            //if too far or I can't get to it
            //teleport there

            //.goToPosition(chooseGoToPoint(ally));
        }
    }

    Vector3 chooseGoToPoint(GameObject ally)
    {
        Vector3 nearest = new Vector3(Mathf.Infinity, Mathf.Infinity, Mathf.Infinity);
        float distance = Mathf.Infinity;
        foreach (Vector3 goToPoint in goToPoints)
        {
            float newDist = Vector3.Distance(goToPoint, ally.transform.position);
            if(newDist <= distance)
            {
                distance = newDist;
                nearest = goToPoint;
            }
        }
        goToPoints.Remove(nearest);
        return nearest;
    }

    //assumes position is set
    public Vector3 makeGoToPoint(float angle, float dist)
    {
        Vector3 goToPoint = destination + UTL_Math.vectorfromAngleAndMagnitude(angle, dist);
        goToPoint = UTL_Dungeon.snapPositionToTerrain(goToPoint);
        //check if point is valid
        NavMeshHit hit;
        int navmeshMask = 1 << NavMesh.GetAreaFromName("Walkable");
        if (NavMesh.SamplePosition(goToPoint, out hit, 5.0F, navmeshMask))
        {
            return hit.position;
        }
        else return new Vector3(Mathf.Infinity, Mathf.Infinity, Mathf.Infinity);
    }

    //assumes position and allyList.Count is set
    public List<Vector3> getGoToPoints()
    {
        float startAngle = Random.Range(0F, 360F);
        List<Vector3> goToPoints = new List<Vector3>();
        Vector3 infinty = new Vector3(Mathf.Infinity, Mathf.Infinity, Mathf.Infinity);
        goToPoints.Add(destination);
        if (allyList.Count == 1)
        {
            return goToPoints;
        }
        float dist = 4F;
        bool offset = false;
        for (int pointsAssigned = 1; pointsAssigned < allyList.Count; offset = !offset)
        {
            float dither = dithervalues[Random.Range(0, 5)];
            for (float angle = 0F; angle < 360F; angle += 60F)
            {
                //float tempAngle = angle + (offset ? 30 : 0) + startAngle + dither;
                float tempAngle = angle + (offset ? 30 : 0) + startAngle ;
                //while (tempAngle >= 360) tempAngle -= 360;
                Vector3 goToPoint = makeGoToPoint(tempAngle, dist);
                if (goToPoint != infinty)
                {
                    goToPoints.Add(goToPoint);
                    pointsAssigned++;
                    if (pointsAssigned >= allyList.Count) break;
                }
            }
            dist += 2.5f;
        }
        return goToPoints;
    }

    /*//goto point based on vector to midpoint
    public void assignGoToPoints()
    {
        sortList();
        for (int i = 0; i < allyList.Count; ++i)
        {
            GameObject ally = allyList[i];
            Vector3 normalVec = (ally.transform.position - midpoint).normalized;
            ally.GetComponent<ENT_Body>().goToPosition(createGoToPoint(normalVec, i));
        }
    }

    public float getGoToDistance(int index)
    {
        return PLY_AllyManager.FOLLOWDISTANCESCALE* index;
    }

    Vector3 createGoToPoint(Vector3 normalVecFromMidpoint, int index)
    {
        float dist = getGoToDistance(index);
        return destination + (normalVecFromMidpoint * dist);

    }

    public void assignGoToPoints()
    {
        allyList.Sort(compareByDotProdToMidpoint);
    }

    public int compareByDotProdToMidpoint(Vector3 a, Vector3 b)
    {
        if (a == null || b == null) return 0;
        //float distA = Vector3.Distance(a, midpoint);
        //float distB = Vector3.Distance(b, midpoint);
        Vector3 vecA = (a - midpoint);
        Vector3 vecB = (b - midpoint);
        float dotA = Vector3.Dot(goToVec, vecA);
        float dotb = Vector3.Dot(goToVec, vecB);

        //float distA = Vector3.Distance(a, goToVec);  //change to midpoint
        //float distB = Vector3.Distance(b, goToVec);  //change to midpoint
        if (dotA < dotb) return -1;
        if (dotA > dotb) return 1;
        return 0;
    }

    public int compareByDotProdToMidpoint(Vector3 a, Vector3 b)
    {


        if (a == null || b == null) return 0;
        //float distA = Vector3.Distance(a, midpoint);
        //float distB = Vector3.Distance(b, midpoint);
        Vector3 vecA = (a - midpoint);
        Vector3 vecB = (b - midpoint);
        float dotA = Vector3.Dot(goToVec, vecA);
        float dotb = Vector3.Dot(goToVec, vecB);

        //float distA = Vector3.Distance(a, goToVec);  //change to midpoint
        //float distB = Vector3.Distance(b, goToVec);  //change to midpoint
        if (dotA < dotb) return -1;
        if (dotA > dotb) return 1;
        return 0;
    }

    public int compareByDotProdToDestination(Vector3 a, Vector3 b)
    {
        if (a == null || b == null) return 0;
        //float distA = Vector3.Distance(a, midpoint);
        //float distB = Vector3.Distance(b, midpoint);
        Vector3 vecA = (a - destination);
        Vector3 vecB = (b - destination);
        float dotA = Vector3.Dot(goToVec, vecA);
        float dotb = Vector3.Dot(goToVec, vecB);

        //float distA = Vector3.Distance(a, goToVec);  //change to midpoint
        //float distB = Vector3.Distance(b, goToVec);  //change to midpoint
        if (dotA < dotb) return -1;
        if (dotA > dotb) return 1;
        return 0;
    }*/

    public Vector3 getMidpoint()
    {
        Vector3 tempVec = Vector3.zero;
        int ally_count = 0;
        foreach (GameObject ally in allyList)
        {
            ++ally_count;
            tempVec += ally.transform.position;
        }
        tempVec /= ally_count;
        return tempVec;
    }

    


    public void setFarFollow()
    {
        if (allyList.Count <= 0) return;
        //farFollowDistance = Mathf.Max(25, Mathf.Min(getFollowDistance(allyList.Count - 1) * 2, 100));
        if (state == PLY_AllyManager.allyState.follow) farFollowDistance = Mathf.Max(50, Mathf.Min(getFollowDistance(allyList.Count - 1) * 2, 100));
        else farFollowDistance = Mathf.Max(25, Mathf.Min(getFollowDistance(allyList.Count - 1) * 2, 100));
    }

    
}