﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIE_MinionEngagementManager : MonoBehaviour {

    //leave TIMER alone, is managed by script
    public float timer = 0;
    public float maxTimer = 0;

    public bool repeat = true;
    //leave REPEATTIMER alone, is managed by script
    public float repeatTimer = 0;
    public float maxRepeatTimer = 60f;

    public enum State
    {
        primed, active, inactive
    }
    State myState;
    public State MyState
    {
        set
        {
            if (value == State.active)
            {
                timer = maxTimer;
                myState = value;
            }
            else if (value == State.inactive && repeat != false)
            {
                repeatTimer = maxRepeatTimer;
                myState = value;
            }
            else
            {
                myState = value;
            }
        }

        get { return myState; }
    }

    // Use this for initialization
    void Start () {
        MyState = State.primed;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
