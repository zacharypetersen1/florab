﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIE_DirectionalMinionSpawner : MonoBehaviour
{

    public int FlowerT1 = 0;
    public int TreeT1 = 0;
    public int MushroomT1 = 0;
    public int ConfusionT2 = 0;
    public int SlowT2 = 0;
    public int NecromancerT2 = 0;

    public int RandomT1 = 0;
    public int RandomT2 = 0;

    public bool isAlly = false;

    public dirRotation minionRotation;
    public enum dirRotation
    {
        atPlayer, Forward
    }

    AIE_MinionEngagementManager manager;

    SphereCollider col;
    //float diameter;
    Vector3 center;
    float radius;

    Pair<int, ENT_DungeonEntity.monsterTypes>[] minoionsToUse = new Pair<int, ENT_DungeonEntity.monsterTypes>[6];

    GameObject player;

    private void Awake()
    {
        //Set enemies to spawn
        minoionsToUse[0] = new Pair<int, ENT_DungeonEntity.monsterTypes>(FlowerT1, ENT_DungeonEntity.monsterTypes.flowerMinion);
        minoionsToUse[1] = new Pair<int, ENT_DungeonEntity.monsterTypes>(TreeT1, ENT_DungeonEntity.monsterTypes.treeMinion);
        minoionsToUse[2] = new Pair<int, ENT_DungeonEntity.monsterTypes>(MushroomT1, ENT_DungeonEntity.monsterTypes.mushroomMinion);
        minoionsToUse[3] = new Pair<int, ENT_DungeonEntity.monsterTypes>(SlowT2, ENT_DungeonEntity.monsterTypes.slowMinion);
        minoionsToUse[4] = new Pair<int, ENT_DungeonEntity.monsterTypes>(NecromancerT2, ENT_DungeonEntity.monsterTypes.necroMinion);
        minoionsToUse[5] = new Pair<int, ENT_DungeonEntity.monsterTypes>(ConfusionT2, ENT_DungeonEntity.monsterTypes.confusionMinion);
    }

    // Use this for initialization
    void Start()
    {
        col = gameObject.GetComponent<SphereCollider>();
        //diameter = col.radius;
        gameObject.transform.position = UTL_Dungeon.snapPositionToTerrain(gameObject.transform.position);

        center = transform.position;
        radius = col.radius/2;

        manager = GetComponentInParent<AIE_MinionEngagementManager>();

        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (manager.MyState == AIE_MinionEngagementManager.State.active)
        {
            manager.timer -= TME_Manager.getDeltaTime(TME_Time.type.environment);
            if (manager.timer <= 0)
            {
                spawnTrap();
                if (manager.repeat == false)
                {
                    Destroy(gameObject.transform.parent.gameObject);
                }
                manager.MyState = AIE_MinionEngagementManager.State.inactive;
            }
        }
        else if (manager.MyState == AIE_MinionEngagementManager.State.inactive)
        {
            manager.repeatTimer -= TME_Manager.getDeltaTime(TME_Time.type.environment);
            if (manager.repeatTimer <= 0)
            {
                manager.MyState = AIE_MinionEngagementManager.State.primed;
            }
        }

    }

    void setRotation(GameObject minion)
    {
        if (minionRotation == dirRotation.atPlayer)
        {
            minion.transform.LookAt(player.transform);
        }
        else if (minionRotation == dirRotation.Forward)
        {
            minion.transform.LookAt(transform.forward);
        }
    }

    void spawnTrap()
    {
        for (int i = 0; i < minoionsToUse.Length; i++)
        {
            for (int j = 0; j < minoionsToUse[i].value1; j++)
            {
                Vector3 spawnPosition = UTL_Dungeon.getRandomPointInCircle(center, radius);
                GameObject newMinion = null;
                if (isAlly)
                {
                    newMinion = UTL_Dungeon.spawnMinion(minoionsToUse[i].value2, spawnPosition, isAlly);
                    setRotation(newMinion);
                }
                else
                {
                    newMinion = UTL_Dungeon.spawnMinion(minoionsToUse[i].value2, spawnPosition, isAlly, new List<Vector3> { spawnPosition });
                    setRotation(newMinion);
                }
                
            }
        }

        for (int i = 0; i < RandomT1; i++)
        {
            GameObject newMinion = null;
            Vector3 spawnPosition = UTL_Dungeon.getRandomPointInCircle(center, radius);
            if (isAlly)
            {
                
                newMinion = UTL_Dungeon.spawnMinion((ENT_DungeonEntity.monsterTypes)Random.Range(0, 3), spawnPosition, isAlly);
                setRotation(newMinion);
            }
            else
            {
                newMinion = UTL_Dungeon.spawnMinion((ENT_DungeonEntity.monsterTypes)Random.Range(0, 3), spawnPosition, isAlly, new List<Vector3> { spawnPosition });
                setRotation(newMinion);
            }
        }

        for (int i = 0; i < RandomT2; i++)
        {
            GameObject newMinion = null;
            Vector3 spawnPosition = UTL_Dungeon.getRandomPointInCircle(center, radius);
            if (isAlly)
            {
                newMinion = UTL_Dungeon.spawnMinion((ENT_DungeonEntity.monsterTypes)Random.Range(3, 6), spawnPosition, isAlly);
                setRotation(newMinion);
            }
            else
            {
                newMinion = UTL_Dungeon.spawnMinion((ENT_DungeonEntity.monsterTypes)Random.Range(3, 6), spawnPosition, isAlly, new List<Vector3> { spawnPosition });
                setRotation(newMinion);
            }
        }
    }

    void destroyTrap()
    {
        Destroy(gameObject);
    }
}
