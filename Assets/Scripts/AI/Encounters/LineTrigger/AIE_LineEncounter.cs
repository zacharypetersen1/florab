﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIE_LineEncounter : MonoBehaviour {

    //DELETE THE PREFAB YOU USE TO MEASURE TRANSFORM BEFORE RUNNING GAME

    public List<ENT_DungeonEntity.monsterTypes> minionTypes = new List<ENT_DungeonEntity.monsterTypes>();
    public List<Vector3> minionPosition = new List<Vector3>();

    Vector3 trapTriggerLoc;

    float timer = 0;
    public float maxTimer = 0;

    public bool repeat = true;
    float repeatTimer = 0;
    public float maxRepeatTimer = 60f;

    enum State
    {
        primed, active, inactive
    }
    State myState;
    State MyState
    {
        set {
            if (value == State.active)
            {
                timer = maxTimer;
                myState = value;
            }
            else if (value == State.inactive && repeat != false)
            {
                repeatTimer = maxRepeatTimer;
                myState = value;
            }else
            {
                myState = value;
            }
        }
    }
    

    BoxCollider col;
    float length = 0;
    // Use this for initialization

    


    void Start()
    {
        MyState = State.primed;
        timer = maxTimer;

        col = gameObject.GetComponent<BoxCollider>();
        if(col.size.x > length)
        {
            length = col.size.x;
        }else if (col.size.y > length)
        {
            length = col.size.y;
        }
        else if (col.size.z > length)
        {
            length = col.size.z;
        }

        col.transform.position = UTL_Dungeon.snapPositionToTerrain(col.transform.position);

        
    }

    // Update is called once per frame
    void Update () {
        
        if(myState == State.active)
        {
            timer -= TME_Manager.getDeltaTime(TME_Time.type.environment);
            if (timer <= 0)
            {
                spawnTrap();
                if(repeat == false)
                {
                    destroyTrap();
                }
                MyState = State.inactive;
            }
        }
        else if(myState == State.inactive)
        {
            repeatTimer -= TME_Manager.getDeltaTime(TME_Time.type.environment);
            if(repeatTimer <= 0)
            {
                MyState = State.primed;
            }
        }
        
    }

    void spawnTrap()
    {
        for(int i = 0; i < minionTypes.Count; ++i)
        {
            Vector3 position = minionPosition[i];
            Quaternion rotation = Quaternion.Euler(trapTriggerLoc - position);
            if(position != null || rotation != null)
            {
                GameObject newMinion = UTL_Dungeon.spawnMinionWithRotation(minionTypes[i], position, rotation, false, new List<Vector3> { position });
            }
        }
    }

    void destroyTrap()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider allyCollider)
    {
        if (myState == State.primed)
        {
            if (allyCollider.tag == "Player")
            {
                trapTriggerLoc = allyCollider.transform.position;
                MyState = State.active;
            }
            else if (allyCollider.tag == "Minion")
            {
                if (allyCollider.gameObject.GetComponent<ENT_Body>().isAlly)
                {
                    trapTriggerLoc = allyCollider.transform.position;
                    MyState = State.active;
                }
            }
        }
    }
}
