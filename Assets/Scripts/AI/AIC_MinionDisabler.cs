﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIC_MinionDisabler : MonoBehaviour {

	void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Minion")
        {
            ENT_Body body = other.gameObject.GetComponent<ENT_Body>();
            body.SetEnabled(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Minion")
        {
            ENT_Body body = other.gameObject.GetComponent<ENT_Body>();
            if (!body.Purity) body.SetEnabled(false);
        }
    }
}
