﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StumpAnim : MonoBehaviour {

    Animator anim;
    static StumpAnim stumpAnim;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        stumpAnim = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void activateStumpAnim(Vector3 pos)
    {
        stumpAnim.transform.position = pos;
        stumpAnim.anim.SetTrigger("StartAnim");
    }

    void EndAnim()
    {
        transform.position = new Vector3(0, -500, 0);
    }
}
