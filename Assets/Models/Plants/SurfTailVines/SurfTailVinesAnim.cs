﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfTailVinesAnim : MonoBehaviour {

    Animator playerAnim, logAnim;

	// Use this for initialization
	void Start () {
        logAnim = gameObject.GetComponent<Animator>();
        playerAnim = GameObject.Find("TwigMesh").GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

        // Get VelocityAnimScalar from player animator
        float val = playerAnim.GetFloat("velocityAnimScalar");
        val *= .7f;
        logAnim.SetFloat("velocityAnimScalar", val * val);
	}
}
