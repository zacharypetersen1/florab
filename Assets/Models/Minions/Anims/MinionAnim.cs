﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionAnim : MonoBehaviour
{

    Animator anim;
    private Vector3 pastPos;
    private float clampLower = 0.3f, clampUpper = 3;
    private float mapLower = 1, mapUpper = 2.5f;
    float velScalar;
    ENT_Body body;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        pastPos = transform.position;
        body = GetComponentInParent<ENT_Body>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.K))
        {
            anim.SetBool("isAppear", true);
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            anim.SetBool("isAppear", false);
        }


        if (body.shouldAttack)
        {
            if (body.entity_type == ENT_DungeonEntity.monsterTypes.treeMinion)
            {
                anim.SetTrigger("doMelee");
            }
            else
            {
                anim.SetTrigger("doRanged");
            }
            body.shouldAttack = false;
            body.attackCooldown = body.attackRechargeTime;
        }

        velScalar = (pastPos - transform.position).magnitude * TME_Manager.getDeltaTime(TME_Time.type.game) * 400;
        velScalar = Mathf.Clamp(velScalar, 0, clampUpper);
        if (velScalar > clampLower)
        {
            velScalar = UTL_Math.mapToNewRange(velScalar, clampLower, clampUpper, mapLower, mapUpper);
        }
        else
        {
            velScalar = 0;
        }
        anim.SetFloat("velocity", velScalar);
        pastPos = transform.position;
    }

    void disableAI()
    {
        ENT_Body body = GetComponentInParent<ENT_Body>();
        body.SetEnabled(false);
    }

    void enableAI()
    {
        ENT_Body body = GetComponentInParent<ENT_Body>();
        body.SetEnabled(true);
    }

    void OnDisappearDone ()
    {
        ENT_Body body = GetComponentInParent<ENT_Body>();
        //tell minion to start moving again
        if(body.reappear)
        {
            transform.parent.GetComponent<Rigidbody>().position = body.teleportationLocation;
            body.gameObject.transform.position = body.teleportationLocation;
            enableAI();
            //appear
            anim.SetBool("isAppear", true);

            //renable movement
            body.Agent.ResetPath();
            body.goToPosition(body.teleportationLocation);
            body.reappear = false;
            body.col.enabled = true;
        }
        if (body.disableAI)
        {
            body.SetPSEnabled(false);
            body.disableAI = false;
            body.gameObject.SetActive(false); 
        }
    }


    void TestFunction()
    {
        if (body.AttackTarget != null) body.attackTarget(body.AttackTarget);
        else body.attack();
    }

    void footstepMinion()
    {
        switch (MAP_NatureMap.natureTypeAtPos(transform.position))
        {
            case MAP_NatureData.type.grass:
                FMODUnity.RuntimeManager.PlayOneShot("event:/minion_footstep_grass", transform.position);
                break;
            case MAP_NatureData.type.rock:
                FMODUnity.RuntimeManager.PlayOneShot("event:/minion_footstep_stone", transform.position);
                break;
            case MAP_NatureData.type.corrupt:
                FMODUnity.RuntimeManager.PlayOneShot("event:/minion_footstep_corrupt", transform.position);
                break;
            default:
                break;
        }
    }

    void attackMinion()
    {
        if (body.isAlly)
        {
            switch (body.getMyType())
            {
                case ENT_DungeonEntity.monsterTypes.flowerMinion:
                    FMODUnity.RuntimeManager.PlayOneShot("event:/ally_flower_fight", transform.position);
                    break;
                case ENT_DungeonEntity.monsterTypes.mushroomMinion:
                    FMODUnity.RuntimeManager.PlayOneShot("event:/ally_mushroom_fight", transform.position);
                    break;
                case ENT_DungeonEntity.monsterTypes.treeMinion:
                    FMODUnity.RuntimeManager.PlayOneShot("event:/ally_tree_fight", transform.position);
                    break;
                case ENT_DungeonEntity.monsterTypes.confusionMinion:
                    FMODUnity.RuntimeManager.PlayOneShot("event:/ally_confusion_fight", transform.position);
                    break;
                case ENT_DungeonEntity.monsterTypes.necroMinion:
                    break;
                default:
                    break;
            }
        }
        else
        {
            switch (body.getMyType())
            {
                case ENT_DungeonEntity.monsterTypes.flowerMinion:
                    FMODUnity.RuntimeManager.PlayOneShot("event:/enemy_flower_fight", transform.position);
                    break;
                case ENT_DungeonEntity.monsterTypes.mushroomMinion:
                    FMODUnity.RuntimeManager.PlayOneShot("event:/enemy_mushroom_fight", transform.position);
                    break;
                case ENT_DungeonEntity.monsterTypes.treeMinion:
                    FMODUnity.RuntimeManager.PlayOneShot("event:/enemy_tree_fight", transform.position);
                    break;
                case ENT_DungeonEntity.monsterTypes.confusionMinion:
                    break;
                case ENT_DungeonEntity.monsterTypes.necroMinion:
                    break;
                default:
                    break;
            }
        }
    }



    //
    // Triggered from animation, spawns appear effect
    //
    void spawnEntryEffect()
    {
        EFX_Effects.createGroundCrackAppear(transform.position);
    }



    //
    // Triggered from animation, spawns disappear effect
    //
    void spawnExitEffect()
    {
        EFX_Effects.createGroundCrackDisappear(transform.position);
    }
}
