﻿Shader "Hidden/ImageShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Value ("Value", range(0, 1)) = 1
		_BrushMask("Texture", 2D) = "white" {}
		_LastFrame("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			float _Value;
			sampler2D _BrushMask;
			sampler2D _LastFrame;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 thisFrame = tex2D(_MainTex, i.uv);
				fixed4 lastFrame = tex2D(_LastFrame, float2(i.uv.x, 1 - i.uv.y));
				fixed4 bMaskCol = tex2D(_BrushMask, i.uv);

				fixed4 col = (bMaskCol * thisFrame) + ((1-bMaskCol) * lastFrame);
				

				return col;
			}
			ENDCG
		}
	}
}
