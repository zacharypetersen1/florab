﻿
Shader "Custom/ShieldEffect" {
	Properties {
		//_MainTex("Texture", 2D) = "white" {}
		//_MainTex("Albedo (RGB)", 2D) = "white" {}
		_MainTex("Base (RGB)", 2D) = "white" {}
		_BumpMap("Bumpmap", 2D) = "bump" {}
		_Color("Color", Color) = (1,1,1,1)
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_Scale("Scale", float) = 1
		_Speed("Speed", float) = 1
		_Frequency("Frequency", float) = 1

		[HideInInspector]_WaveAmplitude1("WaveAmplitude1", float) = 0	
		[HideInInspector]_WaveAmplitude2("WaveAmplitude2", float) = 0
		[HideInInspector]_WaveAmplitude3("WaveAmplitude3", float) = 0
		[HideInInspector]_WaveAmplitude4("WaveAmplitude4", float) = 0

		[HideInInspector]_xImpact1("x Impact 1", float) = 0
		[HideInInspector]_zImpact1("z Impact 1", float) = 0
		[HideInInspector]_yImpact1("y Impact 1", float) = 0

		[HideInInspector]_xImpact2("x Impact 2", float) = 0
		[HideInInspector]_zImpact2("z Impact 2", float) = 0
		[HideInInspector]_yImpact2("y Impact 2", float) = 0

		[HideInInspector]_xImpact3("x Impact 3", float) = 0
		[HideInInspector]_zImpact3("z Impact 3", float) = 0
		[HideInInspector]_yImpact3("y Impact 3", float) = 0

		[HideInInspector]_xImpact4("x Impact 4", float) = 0
		[HideInInspector]_zImpact4("z Impact 4", float) = 0
		[HideInInspector]_yImpact4("y Impact 4", float) = 0
			

		[HideInInspector]_Distance1("Distance1", float) = 0
		[HideInInspector]_Distance2("Distance2", float) = 0
		[HideInInspector]_Distance3("Distance3", float) = 0
		[HideInInspector]_Distance4("Distance4", float) = 0
			
	}
	SubShader {
		//Tags{ "RenderType" = "Opaque" "Queue" = "Geometry+1" "ForceNoShadowCasting" = "True" }
		Tags { "RenderType"="Opaque" }
		LOD 200
		
			/*
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows vertex:vert
		#pragma target 4.0
		*/
		CGPROGRAM
		#pragma surface surf Lambert vertex:vert 
		#pragma target 3.0
		
		//ADDED ALL Y COMPONENTS
		sampler2D _MainTex;
		sampler2D _BumpMap;
		half _Glossiness;
		half _Metallic;
		float _Scale, _Speed, _Frequency;
		half4 _Color;
		float _WaveAmplitude1, _WaveAmplitude2, _WaveAmplitude3, _WaveAmplitude4; //number of hits allowed on mesh
		float _OffsetX1, _OffsetZ1, _OffsetY1, _OffsetX2, _OffsetZ2, _OffsetY2, _OffsetX3, _OffsetZ3, _OffsetY3, _OffsetX4, _OffsetZ4, _OffsetY4;
		float _Distance1, _Distance2, _Distance3, _Distance4; 
		float _xImpact1, _zImpact1, _yImpact1, _xImpact2, _zImpact2, _yImpact2, _xImpact3, _zImpact3, _yImpact3, _xImpact4, _zImpact4, _yImpact4;


		struct appdata
		{
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
		};

		struct Input{
			float2 uv_MainTex;
			float2 uv_BumpMap;
			float3 customValue;
		};

		void vert(inout appdata_full v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input, o);

			half offsetvertX = ((v.vertex.x * v.vertex.x) + (v.vertex.z * v.vertex.z));
			//half offsetvertY = ((v.vertex.y * v.vertex.y) + (v.vertex.z * v.vertex.z));
			
			half value1 = _Scale * sin(_Time.w * _Speed  *_Frequency + offsetvertX + (v.vertex.x * _OffsetX1) + (v.vertex.z * _OffsetZ1)); // + offsetvertY + (v.vertex.y * _OffsetY1) + (v.vertex.y * _OffsetY1));
			half value2 = _Scale * sin(_Time.w * _Speed  *_Frequency + offsetvertX + (v.vertex.x * _OffsetX2) + (v.vertex.z * _OffsetZ2));  //+ offsetvertY + (v.vertex.y * _OffsetY2) + (v.vertex.y * _OffsetY2));
			half value3 = _Scale * sin(_Time.w * _Speed  *_Frequency + offsetvertX + (v.vertex.x * _OffsetX3) + (v.vertex.z * _OffsetZ3));  //+ offsetvertY + (v.vertex.y * _OffsetY3) + (v.vertex.y * _OffsetY3));
			half value4 = _Scale * sin(_Time.w * _Speed  *_Frequency + offsetvertX + (v.vertex.x * _OffsetX4) + (v.vertex.z * _OffsetZ4));  //+ offsetvertY + (v.vertex.y * _OffsetY4) + (v.vertex.y * _OffsetY4));

			float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;

			if (sqrt(pow(worldPos.x - _xImpact1, 2) + pow(worldPos.z - _zImpact1, 2)) < _Distance1) {
				v.vertex.y += value1 * _WaveAmplitude1;
				v.normal.y += value1 * _WaveAmplitude1;
				o.customValue += value1 * _WaveAmplitude1;
			}
			
			if (sqrt(pow(worldPos.x - _xImpact2, 2) + pow(worldPos.z - _zImpact2, 2)) <  _Distance2) {
				v.vertex.y += value2 * _WaveAmplitude2;
				v.normal.y += value2 * _WaveAmplitude2;
				o.customValue += value2 * _WaveAmplitude2;
			}

			if (sqrt(pow(worldPos.x - _xImpact3, 2) + pow(worldPos.z - _zImpact3, 2)) <  _Distance3) {
				v.vertex.y += value3 * _WaveAmplitude3;
				v.normal.y += value3 * _WaveAmplitude3;
				o.customValue += value3 * _WaveAmplitude3; 
			}

			if (sqrt(pow(worldPos.x - _xImpact4, 2) + pow(worldPos.z - _zImpact4, 2)) <  _Distance4) {
				v.vertex.y += value4 * _WaveAmplitude4;
				v.normal.y += value4 * _WaveAmplitude4;
				o.customValue += value4 * _WaveAmplitude4; 
			}
		}

		/*
		void surf(Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap) * 0.2);
			o.Normal.y += IN.customValue;
		}
		*/
		
		
		//GOOD
		void surf(Input IN, inout SurfaceOutput o) {
		half4 c = tex2D(_MainTex, IN.uv_MainTex);
		o.Albedo = _Color.rgb;
		o.Normal.y += IN.customValue;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
